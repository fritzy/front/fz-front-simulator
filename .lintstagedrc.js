'use strict';

module.exports = {
  'package.json': 'sort-package-json',
  'frontend/translations/**/*.json': (filenames) => filenames.map((filename) => `sort-json '${filename}'`),
};
