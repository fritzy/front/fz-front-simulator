'use strict';

const browsers = ['> 0.6%', 'not IE 11', 'not op_mini all', 'not dead'];

module.exports = {
  browsers,
};
