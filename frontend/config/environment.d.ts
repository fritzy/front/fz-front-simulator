export default config;

/**
 * Type declarations for
 *    import config from 'fz-front-simulator/config/environment'
 *
 * For now these need to be managed by the developer
 * since different ember addons can materialize new entries.
 */
declare const config: {
  environment: 'development' | 'production' | 'test';
  modulePrefix: string;
  podModulePrefix: string;
  locationType: string;
  rootURL: string;
  APP: { version: string; [key: string]: unknown };
};
