'use strict';

function addonConfigs() {
  return {
    'asset-cache': {
      include: ['assets/**/*', 'translations/**/*'],
    },
    'ember-bootstrap': require('./ember-boostrap'),
    'ember-composable-helpers': {
      only: ['repeat', 'range'],
    },
    'ember-fetch': {
      preferNative: true,
    },
    'ember-math-helpers': {
      only: ['add', 'div'],
    },
    postcssOptions: require('./postcss'),
  };
}

module.exports = {
  addonConfigs,
};
