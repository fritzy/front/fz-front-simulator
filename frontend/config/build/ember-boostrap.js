'use strict';

module.exports = {
  importBootstrapFont: false,
  bootstrapVersion: 4,
  importBootstrapCSS: false,
  whitelist: [
    'bs-alert',
    'bs-button-group',
    'bs-collapse',
    'bs-form',
    'bs-modal',
    'bs-navbar',
    'bs-progress',
    'bs-tooltip',
  ],
};
