'use strict';

module.exports = {
  allowedVersions: {
    '@ember/render-modifiers': '1.0.2',
    '@embroider/util': '*',
    'ember-modifier': '*',
    'ember-raf-scheduler': '*',
  },
};
