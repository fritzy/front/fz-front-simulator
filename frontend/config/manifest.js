'use strict';

module.exports = function (environment /*, appConfig */) {
  // See https://zonkyio.github.io/ember-web-app for a list of
  // supported properties

  const rootURL = environment.rootURL || '/';

  return {
    name: 'fritzy-simulator',
    short_name: 'simulator',
    description: 'La simulation par votre ami financier',
    start_url: `${rootURL}`,
    display: 'standalone',
    background_color: '#fff',
    theme_color: '#fff',
    icons: [
      {
        src: `${rootURL}assets/images/icons/android-chrome-192x192.png`,
        sizes: '192x192',
        type: 'image/png',
      },
      {
        src: `${rootURL}assets/images/icons/android-chrome-512x512.png`,
        sizes: '512x512',
        type: 'image/png',
      },
      {
        src: `${rootURL}assets/images/icons/apple-touch-icon.png`,
        sizes: '180x180',
        targets: ['apple'],
      },
      {
        src: `${rootURL}assets/images/icons/mstile-70x70.png`,
        element: 'square70x70logo',
        targets: ['ms'],
      },
      {
        src: `${rootURL}assets/images/icons/mstile-150x150.png`,
        element: 'square150x150logo',
        targets: ['ms'],
      },
      {
        src: `${rootURL}assets/images/icons/mstile-310x150.png`,
        element: 'wide310x150logo',
        targets: ['ms'],
      },
      {
        src: `${rootURL}assets/images/icons/mstile-310x310.png`,
        element: 'square310x310logo',
        targets: ['ms'],
      },
      {
        src: `${rootURL}assets/images/icons//safari-pinned-tab.svg`,
        safariPinnedTabColor: '#5bbad5',
        targets: ['safari-pinned-tab'],
      },
    ],
    ms: {
      tileColor: '#da532c',
    },
  };
};
