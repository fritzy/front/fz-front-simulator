'use strict';

const { configureAddons } = require('./addons');

module.exports = function (environment) {
  const ENV = {
    modulePrefix: 'fz-front-simulator',
    podModulePrefix: 'fz-front-simulator/pods',
    environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. EMBER_NATIVE_DECORATOR_SUPPORT: true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false,
      },
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    },
  };

  configureAddons(ENV);

  if (environment === 'development') {
    ENV.SW_DISABLED = process.env.SW_DISABLED;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
    ENV.SW_DISABLED = process.env.SW_DISABLED;
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
  }

  return ENV;
};
