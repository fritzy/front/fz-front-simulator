'use strict';

function configureAddons(ENV) {
  ENV['@sentry/ember'] = {
    disableInstrumentComponents: true,
    disablePerformance: true,
    disableRunloopPerformance: true,
    sentry: {
      attachStacktrace: true,
      autoSessionTracking: false,
      dsn: 'https://f156100c0d60472c831a751a8d9c7ac6@o342082.ingest.sentry.io/5982099',
      enabled: ENV.environment === 'production',
      environment: ENV.environment,
      tracesSampleRate: 1.0,
    },
  };

  ENV['fontawesome'] = {
    defaultPrefix: 'fal', // light
  };

  ENV['sitemap-autogenerator'] = {
    changeFrequency: 'weekly', // default value is 'daily'
    defaultPriorityValue: '0.5', // default value is '0.5'
    showLog: true,
    ignoreTheseRoutes: {
      // Optional,all routes will be included in sitemap.xml except those with path "*"
      privacy: true,
    },
    customPriority: {
      // Optional (if not included in ENV, all values will be the default value '0.5')
    },
  };
}

module.exports = { configureAddons };
