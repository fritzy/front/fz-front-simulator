import { module } from 'qunit';

import { savingsSavingTests } from '../../savings/saving-test';

module('Application | fr/epargne/taux-fixe', savingsSavingTests('/fr/epargne/taux-fixe'));
