import { currentURL, visit } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupApplicationTest } from 'ember-qunit';

import { setupIntl } from 'ember-intl/test-support';

module('Application | en', function (hooks) {
  setupApplicationTest(hooks);
  setupIntl(hooks);

  test('visiting /en', async function (assert) {
    await visit('/en');

    assert.strictEqual(currentURL(), '/en');
  });
});
