import { currentURL, visit } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupApplicationTest } from 'ember-qunit';

import { setupIntl } from 'ember-intl/test-support';

import type ApplicationService from 'fz-front-simulator/services/application';

module('Application | index', function (hooks) {
  setupApplicationTest(hooks);
  setupIntl(hooks);

  test('visiting /', async function (assert) {
    // Given
    const application: ApplicationService = this.owner.lookup('service:application');

    // When
    await visit('/');

    // Then
    assert.true(['/en', '/fr'].includes(currentURL()));

    assert.dom('[data-test-simulators-loan]').exists();
    assert.dom('[data-test-simulators-saving]').exists();
    assert.dom('[data-test-simulators-taxation]').exists();
    assert.dom('[data-test-simulators-portfolio]').exists();

    assert.false(application.embedded);
  });

  test('visiting /?embedded=true', async function (assert) {
    // When
    await visit('/?embedded=true');

    // Then
    const applicationService: ApplicationService = this.owner.lookup('service:application');

    assert.true(applicationService.embedded);
  });
});
