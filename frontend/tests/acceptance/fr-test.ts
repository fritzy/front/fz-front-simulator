import { currentURL, visit } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupApplicationTest } from 'ember-qunit';

import { setupIntl } from 'ember-intl/test-support';

module('Application | fr', function (hooks) {
  setupApplicationTest(hooks);
  setupIntl(hooks);

  test('visiting /fr', async function (assert) {
    await visit('/fr');

    assert.strictEqual(currentURL(), '/fr');
  });
});
