import { currentURL, visit } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupApplicationTest } from 'ember-qunit';

import { setupIntl } from 'ember-intl/test-support';

module('Application | savings/crypto', (hooks) => {
  setupApplicationTest(hooks);
  setupIntl(hooks);

  test('visiting /savings/crypto', async function (assert) {
    await visit('/savings/crypto');

    assert.strictEqual(currentURL(), '/savings/crypto');
    assert.dom('.card').exists();
  });
});
