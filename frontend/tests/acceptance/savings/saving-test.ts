import { currentURL, visit } from '@ember/test-helpers';
import fillIn from '@ember/test-helpers/dom/fill-in';
import { module, test } from 'qunit';
import { setupApplicationTest } from 'ember-qunit';

import { setupIntl } from 'ember-intl/test-support';
import { selectChoose } from 'ember-power-select/test-support';
import { TransferType } from 'fz-front-simulator/enums/transfer-type';

import type { TestContext } from 'ember-intl/test-support';

export function savingsSavingTests(route: string) {
  return (hooks: NestedHooks) => {
    setupApplicationTest(hooks);
    setupIntl(hooks);

    test(`visiting ${route}`, async function (assert) {
      await visit('/savings/saving');

      assert.strictEqual(currentURL(), '/savings/saving');
      assert.dom('.card').exists();
    });

    test('it updates key figures on parameters update', async function (this: TestContext, assert) {
      await visit(route);

      await fillIn('[data-test-parameters-contribution]', '100000');
      await fillIn('[data-test-parameters-saving]', '0');

      assert
        .dom('[data-test-key-figures-deposits-value]')
        .containsText(this.intl.formatNumber(100000, { currency: 'EUR', style: 'currency', maximumFractionDigits: 0 }));
    });

    test('it shows withdrawal on key figures', async function (this: TestContext, assert) {
      await visit(route);

      await selectChoose('[data-test-transfer-type]', this.intl.t(`enums.transfer-type.${TransferType.WITHDRAWAL}`));

      assert.dom('[data-test-key-figures-transfer-type]').isVisible();
    });
  };
}

module('Application | savings/saving', savingsSavingTests('/savings/saving'));
