import { module } from 'qunit';

import { savingsSavingTests } from '../../savings/saving-test';

module('Application | en/saving/fixed-rate', savingsSavingTests('/en/saving/fixed-rate'));
