import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

import Dates from 'fz-front-simulator/utils/dates';

module('Unit | Utils | Dates', function (hooks) {
  setupTest(hooks);

  test('isSameDayOfMonth', function (assert) {
    assert.true(Dates.isSameDayOfMonth(new Date('2017-02-10'), new Date('2017-03-10')));
    assert.true(Dates.isSameDayOfMonth(new Date('2017-02-28'), new Date('2017-03-28')));

    assert.false(Dates.isSameDayOfMonth(new Date('2017-03-31'), new Date('2017-04-29')));
    assert.true(Dates.isSameDayOfMonth(new Date('2017-03-31'), new Date('2017-04-30')));
    assert.true(Dates.isSameDayOfMonth(new Date('2017-01-30'), new Date('2017-02-28')));
  });
});
