import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

import Rates from 'fz-front-simulator/utils/rates';

module('Unit | Utils | Rates', function (hooks) {
  setupTest(hooks);

  test('isSameDayOfMonth', function (assert) {
    assert.strictEqual(Rates.periodicRate(2, 1), 2);
    assert.strictEqual(Rates.periodicRate(2, 12), 0.16515813019202);
  });
});
