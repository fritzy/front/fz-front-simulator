import { destroy } from '@ember/destroyable';
import settled from '@ember/test-helpers/settled';
import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

import SimulationRunner from 'fz-front-simulator/objects/simulation-runner';
import SavingParameters from 'fz-front-simulator/pods/savings/saving/saving-parameters';
import SavingSimulation from 'fz-front-simulator/pods/savings/saving/simulation';

module('Unit | Object | SimulationRunner', function (hooks) {
  setupTest(hooks);

  test('it runs a simulation on results access', async function (assert) {
    let runner = new SimulationRunner(new SavingSimulation(), new SavingParameters());

    assert.true(runner.isRunning);
    await settled();
    assert.ok(runner.result);

    destroy(runner);
  });
});
