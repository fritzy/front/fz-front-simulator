import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | fr/fiscalite/parts-fiscales', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    let route = this.owner.lookup('route:fr/fiscalite/parts-fiscales');

    assert.ok(route);
  });
});
