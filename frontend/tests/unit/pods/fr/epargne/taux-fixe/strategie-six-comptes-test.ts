import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | fr/epargne/strategie-six-comptes', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    let route = this.owner.lookup('route:fr/epargne/strategie-six-comptes');

    assert.ok(route);
  });
});
