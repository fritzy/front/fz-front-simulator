import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | en/loan/credit-score', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    let route = this.owner.lookup('route:en/loan/credit-score');

    assert.ok(route);
  });
});
