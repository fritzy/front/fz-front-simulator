import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | en/saving/financial-freedom', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    let route = this.owner.lookup('route:en/saving/financial-freedom');

    assert.ok(route);
  });
});
