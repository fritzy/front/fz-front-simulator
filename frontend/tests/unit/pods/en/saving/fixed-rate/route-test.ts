import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | en/saving/fixed-rate', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    let route = this.owner.lookup('route:en/saving/fixed-rate');

    assert.ok(route);
  });
});
