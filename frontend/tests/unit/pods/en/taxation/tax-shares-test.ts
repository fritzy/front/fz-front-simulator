import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | en/taxation/tax-shares', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    let route = this.owner.lookup('route:en/taxation/tax-shares');

    assert.ok(route);
  });
});
