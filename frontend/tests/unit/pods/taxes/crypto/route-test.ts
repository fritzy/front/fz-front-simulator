import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | taxes/crypto', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    let route = this.owner.lookup('route:taxes/crypto');

    assert.ok(route);
  });
});
