import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

import { TransferType } from 'fz-front-simulator/enums/transfer-type';
import SavingParametersBuilder from 'fz-front-simulator/pods/savings/saving/saving-parameters-builder';
import SavingSimulation from 'fz-front-simulator/pods/savings/saving/simulation';

module('Unit | Simulation | savings/saving', function (hooks) {
  setupTest(hooks);

  test('it compute simulation on contribution: 1000, duration: 1, saving: 100, yield: 10', async function (assert) {
    // Given
    const simulation = new SavingSimulation();

    const parameters = new SavingParametersBuilder()
      .contribution(1000)
      .duration(1)
      .from(new Date(2021, 0, 1))
      .saving(100)
      .yield(10)
      .fees(0)
      .build();

    // When
    const result = await simulation.simulate(parameters);

    // Then
    assert.strictEqual(result.events.length, 13);
    assert.strictEqual(result.events[0].balance.toFixed(2), '1000.00');
    assert.strictEqual(result.events[1].balance.toFixed(2), '1108.77');
    assert.strictEqual(result.events[11].balance.toFixed(2), '2245.37');

    assert.deepEqual(result.events[12].date, new Date(2022, 0, 1));
    assert.strictEqual(result.events[12].balance.toFixed(2), '2364.07');
    assert.strictEqual(result.events[12].interests.toFixed(2), '164.07');
  });

  test('it compute simulation on contribution: 1000, duration: 1, saving: -100, yield: 10', async function (assert) {
    // Given
    const simulation = new SavingSimulation();

    const parameters = new SavingParametersBuilder()
      .contribution(1000)
      .duration(1)
      .from(new Date(2021, 0, 1))
      .savingType(TransferType.WITHDRAWAL)
      .saving(100)
      .yield(10)
      .fees(0)
      .build();

    // When
    const result = await simulation.simulate(parameters);

    // Then
    assert.strictEqual(result.events.length, 13);
    assert.strictEqual(result.events[0].balance.toFixed(2), '1000.00');
    assert.strictEqual(result.events[1].balance.toFixed(2), '907.98');
    assert.strictEqual(result.events[11].balance.toFixed(2), '0.00');

    assert.deepEqual(result.events[12].date, new Date(2022, 0, 1));
    assert.strictEqual(result.events[12].balance.toFixed(2), '0.00');
    assert.strictEqual(result.events[12].interests.toFixed(2), '0.00');
  });
});
