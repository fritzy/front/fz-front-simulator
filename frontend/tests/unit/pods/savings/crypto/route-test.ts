import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | savings/crypto', (hooks) => {
  setupTest(hooks);

  test('it exists', function (assert) {
    let route = this.owner.lookup('route:savings/crypto');

    assert.ok(route);
  });
});
