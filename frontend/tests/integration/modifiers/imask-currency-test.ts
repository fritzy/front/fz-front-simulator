import { render } from '@ember/test-helpers';
import fillIn from '@ember/test-helpers/dom/fill-in';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';

import hbs from 'htmlbars-inline-precompile';

module('Rendering | Modifiers | imask-currency', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    await render(hbs`<Input id="input" {{imask-currency }} />`);

    assert.dom('#input').exists();
  });

  test('it formats', async function (assert) {
    this.set('value', 1000);

    await render(hbs`<Input id="input" {{imask-currency this.value '€'}} />`);

    assert.dom('#input').hasValue('1 000 €');
  });

  test('it formats on fillIn', async function (assert) {
    // Given
    this.set('value', 1000);
    await render(hbs`<Input id="input" {{imask-currency this.value '€'}} />`);

    // When
    await fillIn('#input', '');
    await fillIn('#input', '123465.56');

    // Then
    assert.dom('#input').hasValue('123 465,56 €');
  });

  test('it formats on value changed', async function (assert) {
    // Given
    this.set('value', 1000);
    await render(hbs`<Input id="input" {{imask-currency this.value '€'}} />`);

    // When
    this.set('value', 2000);

    // Then
    assert.dom('#input').hasValue('2 000 €');
  });

  test('it formats on fillIn after value has changed', async function (assert) {
    // Given
    this.set('value', 1000);
    await render(hbs`<Input id="input" {{imask-currency this.value '€'}} />`);

    // When
    this.set('value', 2000);
    await fillIn('#input', '3000');

    // Then
    assert.dom('#input').hasValue('3 000 €');
  });
});
