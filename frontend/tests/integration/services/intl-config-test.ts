import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';

import type IntlConfigService from 'fz-front-simulator/services/intl-config';

module('Rendering | Service | IntlConfig', function (hooks) {
  setupRenderingTest(hooks);

  test('it sets the locale', async function (assert) {
    const intlConfig: IntlConfigService = this.owner.lookup('service:intl-config');

    await intlConfig.setLocale('fr');
    await render(hbs`{{t "enums.crypto-saving-frequency.ONCE"}}`);

    assert.dom().hasText('Une fois');
  });
});
