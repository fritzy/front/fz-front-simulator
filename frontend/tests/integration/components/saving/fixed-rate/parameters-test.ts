import { render } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';

import { setupIntl } from 'ember-intl/test-support';
import SavingParametersBuilder from 'fz-front-simulator/pods/savings/saving/saving-parameters-builder';
import hbs from 'htmlbars-inline-precompile';

module('Rendering | Component | Saving::FixedRate::Parameters', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks);

  test('it renders', async function (assert) {
    this.set('builder', new SavingParametersBuilder());

    await render(
      hbs`<Saving::FixedRate::Parameters @builder={{this.builder}} @onUpdated={{fn (mut this.parameters)}} />`,
    );

    assert.dom('.card').exists();
  });
});
