import { render } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';

import { setupIntl } from 'ember-intl/test-support';
import SimulationRunner from 'fz-front-simulator/objects/simulation-runner';
import SavingParameters from 'fz-front-simulator/pods/savings/saving/saving-parameters';
import SavingSimulation from 'fz-front-simulator/pods/savings/saving/simulation';
import hbs from 'htmlbars-inline-precompile';

module('Rendering | Component | Saving::FixedRate::Distribution', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks);

  test('it renders', async function (assert) {
    const simulationRunner = new SimulationRunner(new SavingSimulation(), new SavingParameters());

    this.set('runner', simulationRunner);

    await render(hbs`<Saving::FixedRate::Distribution @simulationRunner={{this.runner}} />`);
    assert.dom('.card').exists();
  });
});
