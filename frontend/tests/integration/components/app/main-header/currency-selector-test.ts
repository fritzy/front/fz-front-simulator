import { render } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';

import hbs from 'htmlbars-inline-precompile';

module('Rendering | Component | App::MainHeader::CurrencySelector', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    await render(hbs`
      <BsNavbar as |navbar|><navbar.nav as |nav|><App::MainHeader::CurrencySelector @nav={{nav}}/></navbar.nav></BsNavbar>
      `);

    assert.strictEqual(this.element.textContent?.trim(), 'EUR');
  });
});
