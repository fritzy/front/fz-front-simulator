import { render } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';

import { setupIntl } from 'ember-intl/test-support';
import hbs from 'htmlbars-inline-precompile';

module('Rendering | Component | App::MainHeader::LanguageSelector', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en');

  test('it renders', async function (assert) {
    await render(hbs`
      <BsNavbar as |navbar|><navbar.nav as |nav|><App::MainHeader::LanguageSelector @nav={{nav}}/></navbar.nav></BsNavbar>
      `);

    assert.strictEqual(this.element.textContent?.trim(), 'English');
  });
});
