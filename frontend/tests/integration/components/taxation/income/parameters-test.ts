import { render } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';

import { setupIntl } from 'ember-intl/test-support';
import IncomeTaxationParameters from 'fz-front-simulator/pods/taxation/income/parameters';
import hbs from 'htmlbars-inline-precompile';

module('Rendering | Component | Taxation::Income::Parameters', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks);

  test('it renders', async function (assert) {
    this.set('parameters', new IncomeTaxationParameters());

    await render(
      hbs`<Taxation::Income::Parameters @parameters={{this.parameters}} @onUpdated={{fn (mut this.parameters)}} />`,
    );

    assert.dom('.card').exists();
  });
});
