import { render } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';

import { setupIntl } from 'ember-intl/test-support';
import SimulationRunner from 'fz-front-simulator/objects/simulation-runner';
import IncomeTaxationParameters from 'fz-front-simulator/pods/taxation/income/parameters';
import IncomeTaxationSimulation from 'fz-front-simulator/pods/taxation/income/simulation';
import hbs from 'htmlbars-inline-precompile';

module('Rendering | Component | Taxation::Income::KeyFigures', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks);

  test('it renders', async function (assert) {
    const simulationRunner = new SimulationRunner(new IncomeTaxationSimulation(), new IncomeTaxationParameters());

    this.set('runner', simulationRunner);

    await render(hbs`<Taxation::Income::KeyFigures @simulationRunner={{this.runner}} />`);
    assert.dom('.card').exists();
  });
});
