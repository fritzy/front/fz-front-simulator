import { render } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';

import { setupIntl } from 'ember-intl/test-support';
import TaxationTaxSharesParameters from 'fz-front-simulator/pods/-taxation/tax-shares/parameters';
import hbs from 'htmlbars-inline-precompile';

module('Rendering | Component | Taxation::TaxShares::Content', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks);

  test('it renders', async function (assert) {
    this.set('parameters', new TaxationTaxSharesParameters());

    await render(hbs`<Taxation::TaxShares::Content @parameters={{this.parameters}} />`);

    assert.dom('.content-header').exists();
  });
});
