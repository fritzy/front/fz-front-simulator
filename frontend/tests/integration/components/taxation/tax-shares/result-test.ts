import { render } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';

import { setupIntl } from 'ember-intl/test-support';
import SimulationRunner from 'fz-front-simulator/objects/simulation-runner';
import TaxationTaxSharesParameters from 'fz-front-simulator/pods/-taxation/tax-shares/parameters';
import TaxationTaxSharesSimulation from 'fz-front-simulator/pods/-taxation/tax-shares/simulation';
import hbs from 'htmlbars-inline-precompile';

module('Rendering | Component | Taxation::TaxShares::Result', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks);

  test('it renders', async function (assert) {
    this.set('runner', new SimulationRunner(new TaxationTaxSharesSimulation(), new TaxationTaxSharesParameters()));

    await render(hbs`<Taxation::TaxShares::Result @simulationRunner={{this.runner}} />`);

    assert.dom('.card').exists();
  });
});
