import { render } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';

import { setupIntl } from 'ember-intl/test-support';
import TaxationTaxSharesParameters from 'fz-front-simulator/pods/-taxation/tax-shares/parameters';
import hbs from 'htmlbars-inline-precompile';

module('Rendering | Component | Taxation::TaxShares::Parameters', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks);

  test('it renders', async function (assert) {
    this.set('parameters', new TaxationTaxSharesParameters());

    await render(hbs`<Taxation::TaxShares::Parameters @parameters={{this.parameters}} />`);

    assert.dom('.card').exists();
  });
});
