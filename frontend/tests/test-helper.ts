import { setApplication } from '@ember/test-helpers';
import QUnit from 'qunit';
import { setup } from 'qunit-dom';
import { start } from 'ember-qunit';

import Application from 'fz-front-simulator/app';
import config from 'fz-front-simulator/config/environment';

setApplication(Application.create(config.APP));

setup(QUnit.assert);

start();
