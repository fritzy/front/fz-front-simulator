'use strict';

const broccoliAssetRevDefaults = require('broccoli-asset-rev/lib/default-options');
const EmberApp = require('ember-cli/lib/broccoli/ember-app');
const { addonConfigs } = require('./config/build/addons');

const { applyEnvironmentVariables, configureBabel, logWithAttention } = require('./config/build/ember-build');

const configuration = require('./config/environment');

const { CONCAT_STATS } = process.env;

module.exports = function (defaults) {
  const environment = EmberApp.env();
  const isProduction = environment === 'production';
  const isTest = environment === 'test';

  const embeddedScriptName = 'iframe-integration.js';

  const env = {
    isProduction,
    isTest,
    environment,
    CONCAT_STATS,
    configuration: configuration(environment),
  };

  const appOptions = {
    autoImport: {
      webpack: {
        externals: function (context, request, callback) {
          if (/xlsx|canvg|pdfmake/.test(request)) {
            return callback(null, 'commonjs ' + request);
          }

          callback();
        },
      },
    },

    fingerprint: {
      enabled: isProduction,
      exclude: [embeddedScriptName, '.well-known'],
      extensions: broccoliAssetRevDefaults.extensions.concat(['json', 'webmanifest']),
    },

    sourcemaps: {
      enabled: true,
    },

    tests: isTest, // Don't even generate test files unless a test build

    ...addonConfigs(env),
  };

  configureBabel(appOptions);
  applyEnvironmentVariables(appOptions);

  logWithAttention(env, appOptions);

  const app = new EmberApp(defaults, appOptions);

  app.import('node_modules/iframe-resizer/js/iframeResizer.contentWindow.js');
  app.import('node_modules/@fritzy/simulator-embedded/dist/iframe-integration.js', {
    outputFile: embeddedScriptName,
  });

  return app.toTree();
};
