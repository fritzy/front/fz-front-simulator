# fz-front-simulator

[![coverage report](https://gitlab.com/fritzy/front/fz-front-simulator/badges/develop/coverage.svg)](https://gitlab.com/fritzy/front/fz-front-simulator/-/commits/develop)

This README outlines the details of collaborating on this Ember application.
A short introduction of this app could easily go here.

## Prerequisites

You will need the following things properly installed on your computer.

- [Git](https://git-scm.com/)
- [Ember CLI](https://ember-cli.com/)
- [Google Chrome](https://google.com/chrome/)
- [Volta](https://volta.sh/)

## Installation

### Volta

- `volta install node`
- `volta install yarn`
- `volta install ember-cli`

### Project

- `git clone https://gitlab.com/fritzy/front/fz-front-simulator.git`
- `cd fz-front-simulator`
- `yarn install`

### Fontawesome

export your key as environment variable `$NPM_CONFIG_FONTAWESOME_TOKEN`.
If you don't have a [fontawesome pro](https://fontawesome.com/) license, you can run:

- `yarn nofontawesome`

## Running / Development

- `yarn start`
- Visit your app at [http://localhost:4200](http://localhost:4200).

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Running Tests

- `yarn test`
- `yarn test:coverage` : generate a coverage folder
- `yarn test:server`

### Linting

- `yarn lint:hbs`
- `yarn lint:js`
- `yarn lint:js --fix`

### Building

- `yarn build:development` (development)
- `yarn build:production` (production)

### Deploying

Specify what it takes to deploy your app.

## Further Reading / Useful Links

- [ember.js](https://emberjs.com/)
- [ember-cli](https://ember-cli.com/)
- Development Browser Extensions
  - [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  - [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)
