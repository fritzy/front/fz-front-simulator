import Application from '@ember/application';

import * as Sentry from '@sentry/ember';
import loadInitializers from 'ember-load-initializers';
import Resolver from 'ember-resolver';
import config from 'fz-front-simulator/config/environment';

Sentry.init({
  release: `${config.modulePrefix}@${config.APP.version}`,
});

export default class App extends Application {
  modulePrefix = config.modulePrefix;
  podModulePrefix = config.podModulePrefix;
  Resolver = Resolver;
}

loadInitializers(App, config.modulePrefix);
