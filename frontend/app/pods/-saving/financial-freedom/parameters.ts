import { tracked } from '@glimmer/tracking';

import Money from 'bigint-money';
import CurrencyParameters from 'fz-front-simulator/objects/simulation/currency-parameters';
import Rates from 'fz-front-simulator/utils/rates';

import type Exportable from 'fz-front-simulator/objects/exportable';

interface SavingFinancialFreedomQueryParameters {
  currentWealth: string | undefined;
  expenses: string | undefined;
  income: string | undefined;
  incomeGrowth: string | undefined;
  inflation: string | undefined;
  yield: string | undefined;
}

export default class SavingFinancialFreedomParameters
  extends CurrencyParameters
  implements Exportable<SavingFinancialFreedomQueryParameters>
{
  private static readonly CURRENT_WEALTH_DEFAULT = 10000;
  static readonly #CURRENT_WEALTH_MAX = 1000000;

  private static readonly INCOME_DEFAULT = 2000;
  static readonly #INCOME_MAX = 10000;

  private static readonly INCOME_GROWTH_DEFAULT = 2;
  static readonly #INCOME_GROWTH_MAX = 10;

  private static readonly INFLATION_DEFAULT = 2;
  static readonly #INFLATION_MAX = 10;

  private static readonly EXPENSES_DEFAULT = 1700;
  static readonly #EXPENSES_MAX = 10000;

  private static readonly YIELD_DEFAULT = 7;
  static readonly #YIELD_MAX = 20;

  @tracked private _currentWealth = SavingFinancialFreedomParameters.CURRENT_WEALTH_DEFAULT;

  @tracked private _expenses = SavingFinancialFreedomParameters.EXPENSES_DEFAULT;

  @tracked private _income = SavingFinancialFreedomParameters.INCOME_DEFAULT;

  @tracked private _inflation = SavingFinancialFreedomParameters.INFLATION_DEFAULT;

  @tracked private _incomeGrowth = SavingFinancialFreedomParameters.INCOME_GROWTH_DEFAULT;

  @tracked private _yield = SavingFinancialFreedomParameters.YIELD_DEFAULT;

  private _from: Date;

  constructor(owner: unknown) {
    super(owner);
    this._from = new Date();
    this._from.setHours(0, 0, 0, 0);
  }

  get currentWealth() {
    return this._currentWealth;
  }

  set currentWealth(value: number) {
    if (value >= 0 && value <= this.currentWealthMax) {
      this._currentWealth = value;
    }
  }

  get currentWealthMax() {
    return SavingFinancialFreedomParameters.#CURRENT_WEALTH_MAX;
  }

  get expenses() {
    return this._expenses;
  }

  set expenses(value: number) {
    if (value >= 0 && value <= this.expensesMax) {
      this._expenses = value;
    }
  }

  get expensesMoney() {
    return new Money(this._expenses, this.currency.ticker);
  }

  get expensesMax() {
    return SavingFinancialFreedomParameters.#EXPENSES_MAX;
  }

  get from() {
    return this._from;
  }

  set from(value: Date) {
    if (value < this._from) {
      value.setHours(0, 0, 0, 0);
      this._from = value;
    }
  }

  get income() {
    return this._income;
  }

  set income(value: number) {
    if (value >= 0 && value <= this.incomeMax) {
      this._income = value;
    }
  }

  get incomeMoney() {
    return new Money(this._income, this.currency.ticker);
  }

  get incomeMax() {
    return SavingFinancialFreedomParameters.#INCOME_MAX;
  }

  get incomeGrowth() {
    return this._incomeGrowth;
  }

  set incomeGrowth(value: number) {
    if (value >= 0 && value <= this.incomeMax) {
      this._incomeGrowth = value;
    }
  }

  get incomeGrowthMax() {
    return SavingFinancialFreedomParameters.#INCOME_GROWTH_MAX;
  }

  get inflation() {
    return this._inflation;
  }

  set inflation(value: number) {
    if (value >= 0 && value <= this.inflationMax) {
      this._inflation = value;
    }
  }

  get inflationMax() {
    return SavingFinancialFreedomParameters.#INFLATION_MAX;
  }

  get monthlyInflation() {
    return Rates.periodicRate(this._inflation, 12);
  }

  get yield() {
    return this._yield;
  }

  set yield(value: number) {
    if (value >= 0 && value <= this.yieldMax) {
      this._yield = value;
    }
  }

  get yieldMax() {
    return SavingFinancialFreedomParameters.#YIELD_MAX;
  }

  fromQueryParams(params: SavingFinancialFreedomQueryParameters) {
    if (params.currentWealth) {
      this.currentWealth = Number(params.currentWealth);
    }

    if (params.expenses) {
      this.expenses = Number(params.expenses);
    }

    if (params.income) {
      this.income = Number(params.income);
    }

    if (params.incomeGrowth) {
      this.incomeGrowth = Number(params.incomeGrowth);
    }

    if (params.inflation) {
      this.inflation = Number(params.inflation);
    }

    if (params.yield) {
      this.yield = Number(params.yield);
    }
  }

  toQueryParams() {
    return {
      currentWealth: this.currentWealth.toString(),
      expenses: this.expenses.toString(),
      income: this.income.toString(),
      incomeGrowth: this.incomeGrowth.toString(),
      inflation: this.inflation.toString(),
      yield: this.yield.toString(),
    };
  }

  toString() {
    return `{currency: ${this.currency}, currentWealth: ${this.currentWealth}, expenses: ${this.expenses}, from: ${this.from}, income: ${this.income}, incomeGrowth: ${this.incomeGrowth}, yield: ${this.yield}}`;
  }
}
