import { tracked } from '@glimmer/tracking';
import { getOwner } from '@ember/application';
import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

import SavingFinancialFreedomParameters from './parameters';

import type IntlService from 'ember-intl/services/intl';

export default class SavingFinancialFreedomController extends Controller {
  @service declare intl: IntlService;

  queryParams = [
    'currentWealth',
    'expenses',
    'income',
    'incomeGrowth',
    'inflation',
    'yield',
    {
      queryParamTitle: 'title',
    },
  ];

  currentWealth?: string;
  expenses?: string;
  income?: string;
  incomeGrowth?: string;
  inflation?: string;
  yield?: string;

  queryParamTitle?: string;

  @tracked title = '';

  parameters?: SavingFinancialFreedomParameters;

  setup() {
    this.title = this.queryParamTitle ?? this.intl.t('pods.saving.financial-freedom.title');
    this.parameters = this.parameters ?? new SavingFinancialFreedomParameters(getOwner(this));
    this.parameters.fromQueryParams({
      currentWealth: this.currentWealth,
      expenses: this.expenses,
      income: this.income,
      incomeGrowth: this.incomeGrowth,
      inflation: this.inflation,
      yield: this.yield,
    });
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'saving-financial-freedom-controller': SavingFinancialFreedomController;
  }
}
