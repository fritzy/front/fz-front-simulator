import { debug } from '@ember/debug';

import Money from 'bigint-money';
import { FinancialFreedomPhase } from 'fz-front-simulator/enums/financial-freedom-phase';
import Moneys from 'fz-front-simulator/utils/moneys';

import SavingFinancialFreedomResult from './result';
import { SavingFinancialFreedomResultEventBuilder } from './result-event';

import type SavingFinancialFreedomParameters from './parameters';
import type SavingFinancialFreedomResultEvent from './result-event';
import type Simulation from 'fz-front-simulator/objects/simulation';

export default class SavingFinancialFreedomSimulation
  implements Simulation<SavingFinancialFreedomParameters, SavingFinancialFreedomResult>
{
  static readonly #DURATION_MONTHS = 840;

  #events: SavingFinancialFreedomResultEvent[] = [];
  #parameters!: SavingFinancialFreedomParameters;
  #phase = FinancialFreedomPhase.SAVING;

  async simulate(parameters: SavingFinancialFreedomParameters) {
    debug(`Running saving financial freedom simulation with ${parameters}`);

    this.#events = [];
    this.#parameters = parameters;
    this.#phase = FinancialFreedomPhase.SAVING;

    const from = parameters.from;

    from.setDate(1);

    const contribution = new Money(parameters.currentWealth, 'EUR').add(
      this.computeFlowAmount(parameters.incomeMoney, parameters.expensesMoney),
    );

    const fireThreshold = this.computeFireThreshold(parameters.expensesMoney, parameters.yield, parameters.inflation);

    this.computePhase(fireThreshold, contribution, from);

    this.#events.push(
      new SavingFinancialFreedomResultEventBuilder()
        .date(from)
        .balance(contribution)
        .deposits(contribution)
        .capital(contribution)
        .eventInterests(contribution.multiply((parameters.yield * 100).toFixed()).divide(12 * 10000))
        .fireThreshold(fireThreshold)
        .income(parameters.incomeMoney)
        .expenses(parameters.expensesMoney)
        .build(),
    );

    const totalMonths = SavingFinancialFreedomSimulation.#DURATION_MONTHS;

    for (let m = 0; m < totalMonths; m++) {
      const previous = this.#events[m];
      const newDate = new Date();

      newDate.setDate(1);
      newDate.setHours(0, 0, 0, 0);
      newDate.setMonth(from.getMonth() + m + 1);

      const resultEvent = this.computeSavingResultEvent(previous, newDate, m, totalMonths);

      this.#events.push(resultEvent);
    }

    return new SavingFinancialFreedomResult(this.#events);
  }

  private computeSavingResultEvent(
    previous: SavingFinancialFreedomResultEvent,
    newDate: Date,
    m: number,
    totalMonths: number,
  ) {
    const fireThreshold = this.computeFireThreshold(
      previous.expenses,
      this.#parameters.yield,
      this.#parameters.inflation,
    );

    this.computePhase(fireThreshold, previous.balance, previous.date);

    const income =
      newDate.getMonth() === 0
        ? Moneys.round(previous.income.multiply((1 + this.#parameters.incomeGrowth / 100).toString()), 2)
        : previous.income;
    const expenses = Moneys.round(
      previous.expenses.multiply((1 + this.#parameters.monthlyInflation / 100).toString()),
      2,
    );

    const flowAmount = this.computeFlowAmount(income, expenses);

    const deposits =
      this.#phase === FinancialFreedomPhase.SAVING ? previous.deposits.add(flowAmount) : previous.deposits;

    if (newDate.getMonth() === 0) {
      return this.computeAnnualInteresResultEvent(m, newDate, deposits, income, expenses, flowAmount, fireThreshold);
    } else if (m == totalMonths - 1) {
      return this.computeLastResultEvent(m, newDate, deposits, income, expenses, flowAmount, fireThreshold);
    } else {
      return this.computeResultEventWithoutInterests(
        previous,
        newDate,
        deposits,
        income,
        expenses,
        flowAmount,
        fireThreshold,
      );
    }
  }

  private computeFlowAmount(income: Money, expenses: Money) {
    if (this.#phase === FinancialFreedomPhase.SAVING) {
      return Moneys.max(income.subtract(expenses), Moneys.ZERO_EUR);
    } else {
      return expenses;
    }
  }

  private computeAnnualInteresResultEvent(
    m: number,
    newDate: Date,
    deposits: Money,
    income: Money,
    expenses: Money,
    flowAmount: Money,
    fireThreshold: Money,
  ) {
    const annualInsterests = this.computeInterests(this.#events, Math.max(0, m - 12 + 1), m + 1);

    const previous = this.#events[m];

    let compoundInterests = previous.compoundInterests.add(annualInsterests);

    const newBalanceBeforeTransfer = previous.balance.add(annualInsterests);

    let withdrawal = Moneys.ZERO_EUR;
    let withdrawals = previous.withdrawals;
    let capital = deposits;

    if (this.#phase === FinancialFreedomPhase.FIRE) {
      withdrawal = Moneys.min(newBalanceBeforeTransfer, flowAmount);

      withdrawals = withdrawals.add(withdrawal);

      const interestsFraction = this.computeInterestsFraction(withdrawal, previous.capital, newBalanceBeforeTransfer);

      compoundInterests = compoundInterests.subtract(interestsFraction);
      capital = previous.capital.subtract(withdrawal).add(interestsFraction);
    }

    const newBalance = this.computeNewBalance(newBalanceBeforeTransfer, flowAmount);

    const interests = newBalance.multiply((this.#parameters.yield * 100).toFixed()).divide(12 * 10000);

    return new SavingFinancialFreedomResultEventBuilder()
      .date(newDate)
      .balance(newBalance)
      .deposits(deposits)
      .withdrawal(withdrawal)
      .withdrawals(withdrawals)
      .capital(capital)
      .eventInterests(interests)
      .compoundInterests(compoundInterests)
      .fireThreshold(fireThreshold)
      .income(income)
      .expenses(expenses)
      .build();
  }

  private computeLastResultEvent(
    m: number,
    newDate: Date,
    deposits: Money,
    income: Money,
    expenses: Money,
    flowAmount: Money,
    fireThreshold: Money,
  ) {
    const lastInsterests = this.computeInterests(this.#events, Math.max(0, m - newDate.getMonth()) + 1, m + 1);

    const previous = this.#events[m];
    let compoundInterests = previous.compoundInterests.add(lastInsterests);
    const newBalanceBeforeTransfer = previous.balance.add(lastInsterests);

    let withdrawal = Moneys.ZERO_EUR;
    let withdrawals = previous.withdrawals;
    let capital = deposits;

    if (this.#phase === FinancialFreedomPhase.FIRE) {
      withdrawal = Moneys.min(newBalanceBeforeTransfer, flowAmount);

      withdrawals = withdrawals.add(withdrawal);

      const interestsFraction = this.computeInterestsFraction(withdrawal, previous.capital, newBalanceBeforeTransfer);

      compoundInterests = compoundInterests.subtract(interestsFraction);
      capital = previous.capital.subtract(withdrawal).add(interestsFraction);
    }

    const newBalance = this.computeNewBalance(newBalanceBeforeTransfer, flowAmount);

    const interests = Moneys.ZERO_EUR;

    return new SavingFinancialFreedomResultEventBuilder()
      .date(newDate)
      .balance(newBalance)
      .deposits(deposits)
      .withdrawal(withdrawal)
      .withdrawals(withdrawals)
      .capital(capital)
      .eventInterests(interests)
      .compoundInterests(compoundInterests)
      .fireThreshold(fireThreshold)
      .income(income)
      .expenses(expenses)
      .build();
  }

  private computeInterests(histories: SavingFinancialFreedomResultEvent[], from: number, to: number) {
    return histories
      .slice(from, to)
      .map((e) => e.eventInterests)
      .reduce((a, b) => a.add(b));
  }

  private computeResultEventWithoutInterests(
    previous: SavingFinancialFreedomResultEvent,
    newDate: Date,
    deposits: Money,
    income: Money,
    expenses: Money,
    flowAmount: Money,
    fireThreshold: Money,
  ) {
    const newBalance = this.computeNewBalance(previous.balance, flowAmount);

    let withdrawal = Moneys.ZERO_EUR;
    let withdrawals = previous.withdrawals;

    let compoundInterests = previous.compoundInterests;
    let capital = deposits;

    if (this.#phase === FinancialFreedomPhase.FIRE) {
      withdrawal = Moneys.min(previous.balance, flowAmount);

      const interestsFraction = this.computeInterestsFraction(withdrawal, previous.capital, previous.balance);

      compoundInterests = compoundInterests.subtract(interestsFraction);
      capital = previous.capital.subtract(withdrawal).add(interestsFraction);

      withdrawals = previous.withdrawals.add(withdrawal);
      income = Moneys.ZERO_EUR;
    }

    const interests = newBalance.multiply((this.#parameters.yield * 100).toFixed()).divide(12 * 10000);

    return new SavingFinancialFreedomResultEventBuilder()
      .date(newDate)
      .balance(newBalance)
      .deposits(deposits)
      .withdrawal(withdrawal)
      .withdrawals(withdrawals)
      .capital(capital)
      .eventInterests(interests)
      .compoundInterests(compoundInterests)
      .fireThreshold(fireThreshold)
      .income(income)
      .expenses(expenses)
      .build();
  }

  private computeNewBalance(oldBalance: Money, flowAmount: Money) {
    if (this.#phase === FinancialFreedomPhase.SAVING) {
      return oldBalance.add(flowAmount);
    } else if (oldBalance.isGreaterThanOrEqual(flowAmount)) {
      return oldBalance.subtract(flowAmount);
    } else {
      return Moneys.ZERO_EUR;
    }
  }

  private computeFireThreshold(expenses: Money, savingsYield: number, inflation: number) {
    return savingsYield === 0 ? Moneys.ZERO_EUR : expenses.multiply(1200).divide((savingsYield - inflation).toString());
  }

  private computeInterestsFraction(withdrawal: Money, capital: Money, balance: Money) {
    if (balance.isEqual(Moneys.ZERO_EUR)) {
      return Moneys.ZERO_EUR;
    }

    const rightOperand = capital.multiply(withdrawal).divide(balance);

    return Moneys.max(withdrawal.subtract(rightOperand), Moneys.ZERO_EUR);
  }

  private computePhase(fireThreshold: Money, balance: Money, eventDate: Date) {
    // Set the fire phase on last month on interests generation. Otherwise, the fire threshold amount may not have generated interests.
    if (balance.isGreaterThan(fireThreshold) && eventDate.getMonth() === 11) {
      this.#phase = FinancialFreedomPhase.FIRE;
    }
  }
}
