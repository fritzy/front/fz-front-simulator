import Moneys from 'fz-front-simulator/utils/moneys';

import type SavingFinancialFreedomResultEvent from './result-event';

export default class SavingFinancialFreedomResult {
  constructor(readonly events: SavingFinancialFreedomResultEvent[]) {}

  get fireEvent() {
    return this.events.find((e) => e.withdrawal.isGreaterThan(Moneys.ZERO_EUR));
  }

  get fireThreshold() {
    return (
      this.events.find((e) => e.withdrawal.isGreaterThan(Moneys.ZERO_EUR))?.fireThreshold ??
      this.lastEvent.fireThreshold
    );
  }

  get lastEvent() {
    return this.events[this.events.length - 1];
  }

  get averageExpenses() {
    return this.savingEvents
      ? this.savingEvents
          .map((e) => e.expenses)
          .reduce((i1, i2) => i1.add(i2))
          .divide(this.savingEvents.length)
      : Moneys.ZERO_EUR;
  }

  get averageIncome() {
    return this.savingEvents
      ? this.savingEvents
          .map((e) => e.income)
          .reduce((i1, i2) => i1.add(i2))
          .divide(this.savingEvents.length)
      : Moneys.ZERO_EUR;
  }

  get averageSavingAmount() {
    return Moneys.max(this.averageIncome.subtract(this.averageExpenses), Moneys.ZERO_EUR);
  }

  get averageSavingRate() {
    return this.averageSavingAmount.divide(this.averageIncome).toFixed(2);
  }

  get savingEvents() {
    return this.events.filter((e) => e.income.isGreaterThan(Moneys.ZERO_EUR));
  }
}
