import Moneys from 'fz-front-simulator/utils/moneys';

import type { Money } from 'bigint-money';

export default class SavingFinancialFreedomResultEvent {
  readonly balance: Money;
  readonly capital: Money;
  readonly compoundInterests: Money;
  readonly date: Date;
  readonly deposits: Money;
  readonly eventInterests: Money;
  readonly expenses: Money;
  readonly fireThreshold: Money;
  readonly income: Money;
  readonly withdrawal: Money;
  readonly withdrawals: Money;

  constructor(builder: SavingFinancialFreedomResultEventBuilder) {
    this.balance = builder.balanceParam;
    this.capital = builder.capitalParam;
    this.compoundInterests = builder.compoundInterestsParam;
    this.date = builder.dateParam;
    this.deposits = builder.depositsParam;
    this.eventInterests = builder.eventInterestsParam;
    this.expenses = builder.expensesParam;
    this.fireThreshold = builder.fireThresholdParam;
    this.income = builder.incomeParam;
    this.withdrawal = builder.withdrawalParam;
    this.withdrawals = builder.withdrawalsParam;
  }

  get generatedInterests() {
    return this.balance.subtract(this.deposits).add(this.withdrawals);
  }

  get saving() {
    return this.income.isGreaterThan(this.expenses) ? this.income.subtract(this.expenses) : Moneys.ZERO_EUR;
  }
}

export class SavingFinancialFreedomResultEventBuilder {
  #balance?: Money;
  #capital?: Money;
  #compoundInterests?: Money;
  #date?: Date;
  #deposits?: Money;
  #eventInterests?: Money;
  #expenses?: Money;
  #fireThreshold?: Money;
  #income?: Money;
  #withdrawal?: Money;
  #withdrawals?: Money;

  balance(value: Money) {
    this.#balance = value;

    return this;
  }

  get balanceParam() {
    return this.#balance ?? Moneys.ZERO_EUR;
  }

  capital(value: Money) {
    this.#capital = value;

    return this;
  }

  get capitalParam() {
    return this.#capital ?? Moneys.ZERO_EUR;
  }

  compoundInterests(value: Money) {
    this.#compoundInterests = value;

    return this;
  }

  get compoundInterestsParam() {
    return this.#compoundInterests ?? Moneys.ZERO_EUR;
  }

  date(value: Date) {
    this.#date = value;

    return this;
  }

  get dateParam() {
    return this.#date ?? new Date();
  }

  deposits(value: Money) {
    this.#deposits = value;

    return this;
  }

  get depositsParam() {
    return this.#deposits ?? Moneys.ZERO_EUR;
  }

  eventInterests(value: Money) {
    this.#eventInterests = value;

    return this;
  }

  get eventInterestsParam() {
    return this.#eventInterests ?? Moneys.ZERO_EUR;
  }

  fireThreshold(value: Money) {
    this.#fireThreshold = value;

    return this;
  }

  get fireThresholdParam() {
    return this.#fireThreshold ?? Moneys.ZERO_EUR;
  }

  income(value: Money) {
    this.#income = value;

    return this;
  }

  get incomeParam() {
    return this.#income ?? Moneys.ZERO_EUR;
  }

  expenses(value: Money) {
    this.#expenses = value;

    return this;
  }

  get expensesParam() {
    return this.#expenses ?? Moneys.ZERO_EUR;
  }

  withdrawal(value: Money) {
    this.#withdrawal = value;

    return this;
  }

  get withdrawalParam() {
    return this.#withdrawal ?? Moneys.ZERO_EUR;
  }

  withdrawals(value: Money) {
    this.#withdrawals = value;

    return this;
  }

  get withdrawalsParam() {
    return this.#withdrawals ?? Moneys.ZERO_EUR;
  }

  build() {
    return new SavingFinancialFreedomResultEvent(this);
  }
}
