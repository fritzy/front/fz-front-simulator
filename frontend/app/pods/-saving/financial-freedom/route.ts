import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

import type SavingFinancialFreedomController from './controller';
import type IntlService from 'ember-intl/services/intl';
import type HeadDataService from 'fz-front-simulator/services/head-data';

export default class SavingFinancialFreedom extends Route {
  @service declare headData: HeadDataService;
  @service declare intl: IntlService;

  afterModel() {
    this.headData.title = `${this.intl.t('pods.saving.financial-freedom.breadcrumb')}
    - ${this.intl.t('pods.saving.breadcrumb')} | ${this.intl.t('application.title')}`;
    this.headData.description = this.intl.t('pods.saving.financial-freedom.description');
  }

  setupController(controller: SavingFinancialFreedomController) {
    controller.setup();
  }
}
