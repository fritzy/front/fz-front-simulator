import TaxationTaxSharesController from 'fz-front-simulator/pods/-taxation/tax-shares/controller';

export default class EnTaxationTaxSharesController extends TaxationTaxSharesController {}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'en-taxation-tax-shares-controller': EnTaxationTaxSharesController;
  }
}
