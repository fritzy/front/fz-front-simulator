import TaxationIncomeController from 'fz-front-simulator/pods/taxation/income/controller';

export default class EnTaxationIncomeController extends TaxationIncomeController {}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'en-taxation-income-controller': EnTaxationIncomeController;
  }
}
