import LegalNoticesController from 'fz-front-simulator/pods/legal-notices/controller';

export default class EnLegalNoticesController extends LegalNoticesController {}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'en-legal-notices-controller': EnLegalNoticesController;
  }
}
