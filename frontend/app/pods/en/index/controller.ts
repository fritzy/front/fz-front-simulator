import IndexController from 'fz-front-simulator/pods/-index/controller';

export default class EnIndexController extends IndexController {}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'en-index-controller': EnIndexController;
  }
}
