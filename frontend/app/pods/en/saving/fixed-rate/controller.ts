import SavingController from 'fz-front-simulator/pods/savings/saving/controller';

export default class EnSavingFixedRateController extends SavingController {}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'en-saving-fixed-rate-controller': EnSavingFixedRateController;
  }
}
