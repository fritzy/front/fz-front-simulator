import SavingSixAccountsStrategyController from 'fz-front-simulator/pods/saving/six-accounts-strategy/controller';

export default class EnSavingSixAccountsStrategyController extends SavingSixAccountsStrategyController {}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'en-saving-six-accounts-strategy-controller': EnSavingSixAccountsStrategyController;
  }
}
