import SavingFinancialFreedomController from 'fz-front-simulator/pods/-saving/financial-freedom/controller';

export default class EnSavingFinancialFreedomController extends SavingFinancialFreedomController {}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'en-saving-financial-freedom-controller': EnSavingFinancialFreedomController;
  }
}
