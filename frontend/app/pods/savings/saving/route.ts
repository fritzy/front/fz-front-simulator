import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

import type SavingController from './controller';
import type IntlService from 'ember-intl/services/intl';
import type HeadDataService from 'fz-front-simulator/services/head-data';

export default class SavingsSaving extends Route {
  @service declare headData: HeadDataService;
  @service declare intl: IntlService;

  afterModel() {
    this.headData.title = `${this.intl.t('pods.saving.fixed-rate.breadcrumb')}
    - ${this.intl.t('pods.saving.breadcrumb')} | ${this.intl.t('application.title')}`;
    this.headData.description = this.intl.t('pods.saving.fixed-rate.description');
  }

  setupController(controller: SavingController) {
    controller.setup();
  }
}
