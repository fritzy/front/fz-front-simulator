import { tracked } from '@glimmer/tracking';

import { TransferType } from 'fz-front-simulator/enums/transfer-type';

export default class SavingParameters {
  private static readonly CONTRIBUTION_MAX = 1000000;
  private static readonly DURATION_MAX = 100;
  private static readonly FEES_MAX = 100;
  private static readonly SAVING_MAX = 10000;
  private static readonly YIELD_MAX = 100;

  private static readonly CONTRIBUTION_DEFAULT = 1000;
  private static readonly DURATION_DEFAULT = 10;
  private static readonly FEES_DEFAULT = 1.5;
  private static readonly SAVING_DEFAULT = 100;
  private static readonly TRANSFER_TYPE_DEFAULT = TransferType.DEPOSIT;
  private static readonly YIELD_DEFAULT = 5;

  @tracked private _contribution = SavingParameters.CONTRIBUTION_DEFAULT;

  @tracked private _duration = SavingParameters.DURATION_DEFAULT;

  @tracked private _fees = SavingParameters.FEES_DEFAULT;

  @tracked private _saving = SavingParameters.SAVING_DEFAULT;

  @tracked private _transferType = SavingParameters.TRANSFER_TYPE_DEFAULT;

  @tracked private _yield = SavingParameters.YIELD_DEFAULT;

  private _from: Date;

  constructor() {
    this._from = new Date();
    this._from.setHours(0, 0, 0, 0);
  }

  get contributionMax() {
    return SavingParameters.CONTRIBUTION_MAX;
  }

  get contribution() {
    return this._contribution;
  }

  set contribution(contribution: number) {
    if (contribution < 0 || contribution > this.contributionMax) {
      this._contribution = SavingParameters.CONTRIBUTION_DEFAULT;
    } else {
      this._contribution = contribution;
    }
  }

  get durationMax() {
    return SavingParameters.DURATION_MAX;
  }

  get duration() {
    return this._duration;
  }

  set duration(duration: number) {
    if (duration < 0 || duration > this.durationMax) {
      this._duration = SavingParameters.DURATION_DEFAULT;
    } else {
      this._duration = duration;
    }
  }

  get feesMax() {
    return SavingParameters.FEES_MAX;
  }

  get fees() {
    return this._fees;
  }

  set fees(fees: number) {
    if (fees < 0 || fees > this.feesMax) {
      this._fees = SavingParameters.FEES_DEFAULT;
    } else {
      this._fees = fees;
    }
  }

  get from() {
    return this._from;
  }

  set from(value: Date) {
    if (value < this._from) {
      value.setHours(0, 0, 0, 0);
      this._from = value;
    }
  }

  get savingMax() {
    return SavingParameters.SAVING_MAX;
  }

  get saving() {
    return this._saving;
  }

  set saving(saving: number) {
    if (saving < 0 || saving > this.savingMax) {
      this._saving = SavingParameters.SAVING_DEFAULT;
    } else {
      this._saving = saving;
    }
  }

  get transferType() {
    return this._transferType;
  }

  set transferType(value: TransferType) {
    if (!value) {
      this._transferType = SavingParameters.TRANSFER_TYPE_DEFAULT;
    } else {
      this._transferType = value;
    }
  }

  get yieldMax() {
    return SavingParameters.YIELD_MAX;
  }

  get yield() {
    return this._yield;
  }

  set yield(value: number) {
    if (value < 0 || value > this.yieldMax) {
      this._yield = SavingParameters.YIELD_DEFAULT;
    } else {
      this._yield = value;
    }
  }

  toQueryParams() {
    return {
      contribution: this.contribution,
      duration: this.duration,
      saving: this.saving,
      transferType: this.transferType,
      yield: this.yield,
      fees: this.fees,
    };
  }
}
