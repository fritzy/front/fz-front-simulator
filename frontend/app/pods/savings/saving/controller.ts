import { tracked } from '@glimmer/tracking';
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import SimulationRunner from 'fz-front-simulator/objects/simulation-runner';

import SavingParametersBuilder from './saving-parameters-builder';
import SavingSimulation from './simulation';

import type FixedRateSavingResult from './result';
import type SavingParameters from './saving-parameters';
import type IntlService from 'ember-intl/services/intl';
import type { TransferType } from 'fz-front-simulator/enums/transfer-type';
import type ApplicationService from 'fz-front-simulator/services/application';
export default class SavingController extends Controller {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;

  queryParams = ['contribution', 'duration', 'saving', 'title', 'transferType', 'yield'];

  title?: string;

  contribution?: number;

  duration?: number;

  saving?: number;

  transferType?: TransferType;

  yield?: number;

  @tracked finalTitle = '';

  builder = new SavingParametersBuilder();

  simulationRunner?: SimulationRunner<SavingParameters, FixedRateSavingResult>;

  setup() {
    this.finalTitle = this.title || this.intl.t('pods.saving.fixed-rate.title');

    this.builder = this.builder
      .contribution(this.contribution)
      .duration(this.duration)
      .saving(this.saving)
      .savingType(this.transferType)
      .yield(this.yield);

    this.simulationRunner = new SimulationRunner(new SavingSimulation(), this.builder.build());
  }

  @action
  updateLanguage() {
    const queryParams = this.simulationRunner?.parameters?.toQueryParams();

    switch (this.intl.primaryLocale) {
      case 'en':
        this.transitionToRoute('en.saving.fixed-rate', { queryParams: queryParams });

        break;

      case 'fr':
        this.transitionToRoute('fr.epargne.taux-fixe', { queryParams: queryParams });

        break;
    }
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'saving-controller': SavingController;
  }
}
