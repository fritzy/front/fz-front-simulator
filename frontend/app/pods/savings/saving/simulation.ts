import { debug } from '@ember/debug';

import Money from 'bigint-money';
import { TransferType } from 'fz-front-simulator/enums/transfer-type';
import Moneys from 'fz-front-simulator/utils/moneys';

import FixedRateSavingResult from './result';
import FixedRateSavingResultEvent from './result-event';

import type SavingParameters from './saving-parameters';
import type Simulation from 'fz-front-simulator/objects/simulation';

export default class SavingSimulation implements Simulation<SavingParameters, FixedRateSavingResult> {
  async simulate(parameters: SavingParameters) {
    debug(
      `Running constant yield saving simulation with {contribution: ${parameters.contribution}, duration: ${parameters.duration}, saving: ${parameters.saving}, transferType: ${parameters.transferType}, yield: ${parameters.yield}, fees: ${parameters.fees}}`,
    );

    const events: FixedRateSavingResultEvent[] = [];
    const from = parameters.from;

    from.setDate(1);

    const contribution = new Money(parameters.contribution, 'EUR');
    const transferAmount = Number.isSafeInteger(parameters.saving)
      ? new Money(parameters.saving, 'EUR')
      : new Money(parameters.saving.toString(), 'EUR');

    events.push(
      new FixedRateSavingResultEvent.Builder()
        .date(from)
        .deposits(contribution)
        .withdrawals(Moneys.ZERO_EUR)
        .capital(contribution)
        .interests(Moneys.ZERO_EUR)
        .fees(Moneys.ZERO_EUR)
        .build(),
    );

    const totalMonths = parameters.duration * 12;
    const yieldApr = this.computeApr(parameters.yield, 12);
    const feesApr = this.computeApr(parameters.fees, 12);

    for (let m = 0; m < totalMonths; m++) {
      const previous = events[m];
      const newDate = new Date(from.getTime());

      newDate.setMonth(from.getMonth() + m + 1);

      const resultEvent = this.computeNextResultEvent(
        events,
        m,
        newDate,
        parameters.transferType === TransferType.DEPOSIT ? previous.deposits.add(transferAmount) : previous.deposits,
        transferAmount,
        parameters,
        yieldApr,
        feesApr,
      );

      events.push(resultEvent);
    }

    return new FixedRateSavingResult(events);
  }

  private computeNextResultEvent(
    histories: FixedRateSavingResultEvent[],
    m: number,
    newDate: Date,
    deposits: Money,
    transferAmount: Money,
    parameters: SavingParameters,
    yieldApr: number,
    feesApr: number,
  ) {
    const previous = histories[m];
    let interests = previous.interests;
    let newBalance = previous.balance;

    if (parameters.transferType === TransferType.DEPOSIT) {
      newBalance = newBalance.add(transferAmount);
    }

    // interests
    const eventInterests = newBalance.multiply((yieldApr * 100).toFixed()).divide(12 * 10000);

    interests = interests.add(eventInterests);
    newBalance = newBalance.add(eventInterests);

    let withdrawals = previous.withdrawals;
    let capital = newBalance.subtract(interests);

    // fees
    const eventFees = newBalance.multiply((feesApr * 100).toFixed()).divide(12 * 10000);

    const interestsShare = this.computeInterestsShare(capital, interests);
    const feesOnInterests = interestsShare.multiply(eventFees);

    interests = interests.subtract(feesOnInterests);
    capital = capital.subtract(eventFees).add(feesOnInterests);
    newBalance = capital.add(interests);

    // withdrawal
    if (parameters.transferType === TransferType.WITHDRAWAL) {
      const withdrawal = Moneys.min(newBalance, transferAmount);

      const interestsShare = this.computeInterestsShare(capital, interests);
      const withdrawalOnInterests = interestsShare.multiply(withdrawal);

      interests = interests.subtract(withdrawalOnInterests);
      // avoid -0.00 with abs
      capital = capital.subtract(withdrawal).add(withdrawalOnInterests).abs();
      newBalance = capital.add(interests);

      withdrawals = previous.withdrawals.add(withdrawal);
    }

    const fees = previous.fees.add(eventFees);

    return new FixedRateSavingResultEvent.Builder()
      .date(newDate)
      .deposits(deposits)
      .withdrawals(withdrawals)
      .capital(capital)
      .interests(interests)
      .fees(fees)
      .build();
  }

  private computeInterestsShare(capital: Money, interests: Money) {
    if (interests.isEqual(Moneys.ZERO_EUR) || capital.add(interests).isEqual(Moneys.ZERO_EUR)) {
      return Moneys.ZERO_EUR;
    }

    return interests.divide(capital.add(interests));
  }

  private computeApr(apy: number, m: number) {
    // https://www.inchcalculator.com/apr-to-apy-calculator/

    return ((apy / 100 + 1) ** (1 / m) - 1) * m * 100;
  }
}
