import type FixedRateSavingResultEvent from './result-event';

export default class FixedRateSavingResult {
  constructor(readonly events: FixedRateSavingResultEvent[]) {}

  get lastEvent() {
    return this.events[this.events.length - 1];
  }
}
