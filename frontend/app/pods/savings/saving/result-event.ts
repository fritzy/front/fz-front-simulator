import Moneys from 'fz-front-simulator/utils/moneys';

import type { Money } from 'bigint-money';

export default class FixedRateSavingResultEvent {
  #date!: Date;
  #deposits!: Money;
  #withdrawals!: Money;
  #capital!: Money;
  #interests!: Money;
  #fees!: Money;

  private constructor() {
    // private constructor
  }

  get date() {
    return this.#date;
  }
  get deposits() {
    return this.#deposits;
  }
  get withdrawals() {
    return this.#withdrawals;
  }
  get capital() {
    return this.#capital;
  }
  get interests() {
    return this.#interests;
  }
  get fees() {
    return this.#fees;
  }

  get balance() {
    return this.#capital.add(this.#interests);
  }
  get grossBalance() {
    return this.#capital.add(this.#interests).add(this.#fees);
  }

  static Builder = class {
    private _date?: Date;
    private _deposits?: Money;
    private _withdrawals?: Money;
    private _capital?: Money;
    private _interests?: Money;
    private _fees?: Money;

    date(value: Date) {
      this._date = value;

      return this;
    }
    deposits(value: Money) {
      this._deposits = value;

      return this;
    }
    withdrawals(value: Money) {
      this._withdrawals = value;

      return this;
    }
    capital(value: Money) {
      this._capital = value;

      return this;
    }
    interests(value: Money) {
      this._interests = value;

      return this;
    }
    fees(value: Money) {
      this._fees = value;

      return this;
    }

    build() {
      const event = new FixedRateSavingResultEvent();

      event.#date = this._date ?? new Date();
      event.#deposits = this._deposits ?? Moneys.ZERO;
      event.#withdrawals = this._withdrawals ?? Moneys.ZERO;
      event.#capital = this._capital ?? Moneys.ZERO;
      event.#interests = this._interests ?? Moneys.ZERO;
      event.#fees = this._fees ?? Moneys.ZERO;

      return event;
    }
  };
}
