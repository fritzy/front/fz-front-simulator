// eslint-disable-next-line simple-import-sort/imports
import type { TransferType } from 'fz-front-simulator/enums/transfer-type';
import SavingParameters from './saving-parameters';

export default class SavingParametersBuilder {
  #contribution?: number;
  #duration?: number;
  #from?: Date;
  #saving?: number;
  #transferType?: TransferType;
  #yield?: number;
  #fees?: number;

  contribution(value?: number) {
    this.#contribution = value;

    return this;
  }

  duration(value?: number) {
    this.#duration = value;

    return this;
  }

  from(value?: Date) {
    this.#from = value;

    return this;
  }

  saving(value?: number) {
    this.#saving = value;

    return this;
  }

  savingType(value?: TransferType) {
    this.#transferType = value;

    return this;
  }

  yield(value?: number) {
    this.#yield = value;

    return this;
  }

  fees(value?: number) {
    this.#fees = value;

    return this;
  }

  build() {
    const parameters = new SavingParameters();

    this.setNonNull(this.#contribution, (v) => (parameters.contribution = v));
    this.setNonNull(this.#duration, (v) => (parameters.duration = v));
    this.setNonNull(this.#from, (v) => (parameters.from = v));
    this.setNonNull(this.#saving, (v) => (parameters.saving = v));
    this.setNonNull(this.#transferType, (v) => (parameters.transferType = v));
    this.setNonNull(this.#yield, (v) => (parameters.yield = v));
    this.setNonNull(this.#fees, (v) => (parameters.fees = v));

    return parameters;
  }

  private setNonNull<T>(value: T, setter: (v: NonNullable<T>) => void) {
    if (value !== undefined && value !== null) {
      setter(value as NonNullable<T>);
    }
  }
}
