import type CryptoSavingResultEvent from './result-event';

export default class CryptoSavingResult {
  constructor(readonly events: CryptoSavingResultEvent[]) {}

  get lastEvent(): CryptoSavingResultEvent | undefined {
    return this.events[this.events.length - 1];
  }

  get accumulated() {
    return this.lastEvent?.accumulated;
  }

  get invested() {
    return this.lastEvent?.invested;
  }

  get getBuyingEventsNumber(): number {
    return this.events.filter((e) => e.toBuy.isGreaterThan(0)).length;
  }

  get averageBuyingPrice() {
    if (!this.accumulated || !this.invested) {
      return undefined;
    }

    return this.accumulated.toSource() !== 0n ? this.invested.divide(this.accumulated) : 0;
  }

  get averageBuyingPriceAmount() {
    return this.averageBuyingPrice?.toFixed(4);
  }

  get performance() {
    if (!this.invested || !this.lastEvent) {
      return undefined;
    }

    return this.invested.toSource() !== 0n ? this.lastEvent.plusMinusValue.divide(this.invested).toFixed(4) : '0';
  }

  get total() {
    return this.lastEvent?.value;
  }
}
