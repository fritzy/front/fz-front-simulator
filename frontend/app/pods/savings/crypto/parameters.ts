import { tracked } from '@glimmer/tracking';
import { setOwner } from '@ember/application';
import { inject as service } from '@ember/service';

import { CryptoSavingFrequency } from 'fz-front-simulator/enums/crypto-saving-frequency';
import Dates from 'fz-front-simulator/utils/dates';

import type CoinGeckoCoin from 'fz-front-simulator/objects/cgc/coin-gecko-coin';
import type Currency from 'fz-front-simulator/objects/currency';
import type ApplicationService from 'fz-front-simulator/services/application';
import type CryptoService from 'fz-front-simulator/services/crypto-service';

export default class CryptoSavingParameters {
  private static readonly AMOUNT_MAX = 10000;

  private static readonly AMOUNT_DEFAULT = 25;
  private static readonly FREQUENCY_DEFAULT = CryptoSavingFrequency.WEEKLY;
  private static readonly FROM_DEFAULT = '2018-01-01';
  private static readonly TO_DEFAULT = Dates.getTodayAsString();

  @service declare application: ApplicationService;
  @service declare cryptoService: CryptoService;

  @tracked private _asset?: CoinGeckoCoin;
  @tracked private _assetId?: string;
  @tracked private _amount = CryptoSavingParameters.AMOUNT_DEFAULT;
  @tracked private _currency;
  @tracked private _frequency = CryptoSavingParameters.FREQUENCY_DEFAULT;
  @tracked private _from = CryptoSavingParameters.FROM_DEFAULT;
  @tracked private _to = CryptoSavingParameters.TO_DEFAULT;

  constructor(owner: unknown) {
    setOwner(this, owner);
    this._currency = this.application.currency;
  }

  get amountMax() {
    return CryptoSavingParameters.AMOUNT_MAX;
  }

  get amount() {
    return this._amount;
  }

  set amount(value: number) {
    if (value < 0 || value > this.amountMax) {
      this._amount = CryptoSavingParameters.AMOUNT_DEFAULT;
    } else {
      this._amount = value;
    }
  }

  get assetId() {
    return this._assetId;
  }

  set assetId(value) {
    this._assetId = value;
  }

  get currency() {
    return this._currency;
  }

  set currency(value: Currency) {
    this._currency = value;
  }

  get asset() {
    return this._asset;
  }

  set asset(value) {
    this._asset = value;

    if (value) {
      this.assetId = value.id;
    }
  }

  get frequency() {
    return this._frequency;
  }

  set frequency(value: CryptoSavingFrequency) {
    this._frequency = value;
  }

  get from() {
    return this._from;
  }

  set from(value: string) {
    this._from = value;
  }

  get fromDate() {
    return new Date(this._from);
  }

  get to() {
    return this._to;
  }

  set to(value: string) {
    this._to = value;
  }

  get toDate() {
    return new Date(this._to);
  }

  async getPrices() {
    return this._asset ? this.cryptoService.getHistoricalPrice(this._asset, this.currency) : undefined;
  }
}
