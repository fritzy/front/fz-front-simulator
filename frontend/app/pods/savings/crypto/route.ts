import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

import type SavingCryptoController from './controller';
import type Transition from '@ember/routing/-private/transition';
import type IntlService from 'ember-intl/services/intl';
import type CoinGeckoCoin from 'fz-front-simulator/objects/cgc/coin-gecko-coin';
import type CryptoService from 'fz-front-simulator/services/crypto-service';
import type HeadDataService from 'fz-front-simulator/services/head-data';

export default class SavingsCrypto extends Route {
  @service declare cryptoService: CryptoService;
  @service declare headData: HeadDataService;
  @service declare intl: IntlService;

  async afterModel() {
    this.headData.title = `${this.intl.t('pods.saving.crypto.breadcrumb')}
    - ${this.intl.t('pods.saving.breadcrumb')} | ${this.intl.t('application.title')}`;
    this.headData.description = this.intl.t('pods.saving.crypto.description');
  }

  setupController(
    controller: SavingCryptoController,
    model: CoinGeckoCoin | undefined,
    transition: Transition<unknown>,
  ) {
    super.setupController(controller, model, transition);
    controller.setup();
  }
}
