import { tracked } from '@glimmer/tracking';
import { getOwner } from '@ember/application';
import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

import CryptoSavingParameters from './parameters';

import type IntlService from 'ember-intl/services/intl';

export default class SavingCryptoController extends Controller {
  @service declare intl: IntlService;

  queryParams = [
    {
      queryParamCoin: 'coin',
      queryParamTitle: 'title',
    },
  ];

  queryParamCoin?: string;
  queryParamTitle?: string;

  @tracked title = '';

  parameters: CryptoSavingParameters;

  constructor(args: Record<string, unknown> | undefined) {
    super(args);
    this.parameters = new CryptoSavingParameters(getOwner(this));
  }

  setup() {
    this.title = this.queryParamTitle || this.intl.t('pods.saving.crypto.title');

    if (this.queryParamCoin) {
      this.parameters.assetId = this.queryParamCoin;
    }
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'saving-crypto-controller': SavingCryptoController;
  }
}
