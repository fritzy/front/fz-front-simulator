import { debug } from '@ember/debug';

import Money from 'bigint-money';
import { CryptoSavingFrequency } from 'fz-front-simulator/enums/crypto-saving-frequency';
import Dates from 'fz-front-simulator/utils/dates';

import CryptoSavingResult from './result';
import CryptoSavingResultEvent from './result-event';

import type CryptoSavingParameters from './parameters';
import type HistoricalPrice from 'fz-front-simulator/objects/historical-price';
import type Simulation from 'fz-front-simulator/objects/simulation';

export default class SavingCryptoSimulation implements Simulation<CryptoSavingParameters, CryptoSavingResult> {
  async simulate(parameters: CryptoSavingParameters) {
    debug(
      `Running crypto saving simulation with {asset: ${parameters.asset?.id}, amount: ${parameters.amount}, currency: ${parameters.currency.ticker}, frequency: ${parameters.frequency}, from: ${parameters.from}, to: ${parameters.to}}`,
    );

    const toBuy = new Money(parameters.amount.toFixed(parameters.currency.fractionDigits), parameters.currency.ticker);
    const sinceDate = new Date(parameters.from);

    let events: CryptoSavingResultEvent[] = [];

    if (parameters.asset) {
      const prices = await parameters.getPrices();

      if (prices) {
        events = prices
          .filter((h) => h.date >= sinceDate)
          .reduce(
            (values: CryptoSavingResultEvent[], value: HistoricalPrice) =>
              this.computeEvent(values, value, parameters, toBuy),
            new Array<CryptoSavingResultEvent>(),
          );
      }
    }

    return new CryptoSavingResult(events);
  }

  private computeEvent(
    values: CryptoSavingResultEvent[],
    value: HistoricalPrice,
    parameters: CryptoSavingParameters,
    toBuy: Money,
  ) {
    values.push(
      new CryptoSavingResultEvent(
        value,
        this.isBuyDay(value.date, parameters) ? toBuy : new Money(0, parameters.currency.ticker),
        values.length === 0 ? new Money(0, this.getAssetSymbol(parameters)) : values[values.length - 1].accumulated,
        values.length === 0 ? new Money(0, parameters.currency.ticker) : values[values.length - 1].invested,
      ),
    );

    return values;
  }

  private getAssetSymbol(parameters: CryptoSavingParameters) {
    return parameters.asset ? parameters.asset.symbol : '';
  }

  private isBuyDay(date: Date, parameters: CryptoSavingParameters): boolean {
    if (date > parameters.toDate) {
      return false;
    }

    switch (parameters.frequency) {
      case CryptoSavingFrequency.ONCE:
        return Dates.isSameDay(date, parameters.fromDate);

      case CryptoSavingFrequency.DAILY:
        return true;

      case CryptoSavingFrequency.WEEKLY:
        return Dates.isSameDayOfWeek(parameters.fromDate, date);

      case CryptoSavingFrequency.MONTHLY:
        return Dates.isSameDayOfMonth(parameters.fromDate, date);
    }
  }
}
