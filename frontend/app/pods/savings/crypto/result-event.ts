import Money from 'bigint-money';

import type HistoricalPrice from 'fz-front-simulator/objects/historical-price';

export default class CryptoSavingResultEvent {
  constructor(
    readonly historicalPrice: HistoricalPrice,
    readonly toBuy: Money,
    readonly alreadyAccumulated: Money,
    readonly alreadyInvested: Money,
  ) {}

  get accumulated(): Money {
    if (this.historicalPrice.price.isEqual(0)) {
      return this.alreadyAccumulated;
    } else {
      return new Money(1, this.alreadyAccumulated.currency)
        .multiply(this.toBuy)
        .divide(this.historicalPrice.price)
        .add(this.alreadyAccumulated);
    }
  }

  get accumulatedAmount(): string {
    return this.accumulated.toFixed(8);
  }

  get date(): Date {
    return this.historicalPrice.date;
  }

  get priceAmount(): string {
    return this.historicalPrice.price.toFixed(4);
  }

  get invested(): Money {
    return this.alreadyInvested.add(this.toBuy);
  }

  get investedAmount(): string {
    return this.invested.toFixed(0);
  }

  get value(): Money {
    return this.historicalPrice.price.multiply(this.accumulated);
  }

  get valueAmount(): string {
    return this.value.toFixed(0);
  }

  get plusMinusValue(): Money {
    return this.value.subtract(this.invested);
  }

  get plusMinusValueAmount(): string {
    return this.plusMinusValue.toFixed(0);
  }
}
