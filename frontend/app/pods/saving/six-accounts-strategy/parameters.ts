import { tracked } from '@glimmer/tracking';

import CurrencyParameters from 'fz-front-simulator/objects/simulation/currency-parameters';

export default class SixAccountsStrategySavingParameters extends CurrencyParameters {
  private static readonly INCOME_MAX = 100000;

  private static readonly INCOME_DEFAULT = 1800;

  @tracked private _income = SixAccountsStrategySavingParameters.INCOME_DEFAULT;

  get income() {
    return this._income;
  }

  set income(value: number) {
    if (value < 0 || value > this.incomeMax) {
      this._income = SixAccountsStrategySavingParameters.INCOME_DEFAULT;
    } else {
      this._income = value;
    }
  }

  get incomeMax() {
    return SixAccountsStrategySavingParameters.INCOME_MAX;
  }

  toQueryParams() {
    return {
      income: this._income,
    };
  }
}
