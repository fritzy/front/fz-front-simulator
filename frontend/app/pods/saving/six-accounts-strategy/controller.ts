import { tracked } from '@glimmer/tracking';
import { getOwner } from '@ember/application';
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import Simulators from 'fz-front-simulator/utils/simulators';

import SixAccountsStrategySavingParameters from './parameters';

import type IntlService from 'ember-intl/services/intl';
import type ApplicationService from 'fz-front-simulator/services/application';
import type RouterConfigService from 'fz-front-simulator/services/router-config';

export default class SavingSixAccountsStrategyController extends Controller {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;
  @service declare routerConfig: RouterConfigService;

  queryParams = [
    {
      queryParamIncome: 'income',
      queryParamTitle: 'title',
    },
  ];

  queryParamIncome?: string;
  queryParamTitle?: string;

  @tracked title = '';

  parameters: SixAccountsStrategySavingParameters;

  constructor(args: Record<string, unknown> | undefined) {
    super(args);
    this.parameters = new SixAccountsStrategySavingParameters(getOwner(this));
  }

  setup() {
    this.title = this.queryParamTitle || this.intl.t('pods.saving.six-accounts-strategy.title');

    if (Number(this.queryParamIncome)) {
      this.parameters.income = Number(this.queryParamIncome);
    }
  }

  @action
  onLanguageChange() {
    const route = this.routerConfig.getSimulatorRoute(Simulators.SAVING_SIX_ACCOUNTS_STRATEGY);
    const queryParams = this.parameters.toQueryParams();

    if (route) {
      this.transitionToRoute(route, { queryParams: queryParams });
    }
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'savings-six-account-strategy-controller': SavingSixAccountsStrategyController;
  }
}
