import { debug } from '@ember/debug';

import Money from 'bigint-money';

import SixAccountsStrategySavingResult from './result';

import type SixAccountsStrategySavingParameters from './parameters';
import type Simulation from 'fz-front-simulator/objects/simulation';

export default class SixAccountsStrategySimulation
  implements Simulation<SixAccountsStrategySavingParameters, SixAccountsStrategySavingResult>
{
  async simulate(parameters: SixAccountsStrategySavingParameters) {
    debug(
      `Running six accounts strategy simulation with {income: ${parameters.income}, currency: ${parameters.currency.ticker}}`,
    );

    const incomeMoney = new Money(
      parameters.income.toFixed(parameters.currency.fractionDigits),
      parameters.currency.ticker,
    );

    const tenPercentIncomeMoney = incomeMoney.divide(10);

    const result = new SixAccountsStrategySavingResult(
      tenPercentIncomeMoney,
      tenPercentIncomeMoney,
      tenPercentIncomeMoney,
      tenPercentIncomeMoney.multiply('5.5'),
      tenPercentIncomeMoney,
      tenPercentIncomeMoney.divide(2),
    );

    return result;
  }
}
