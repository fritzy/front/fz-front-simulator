import type Money from 'bigint-money';

export default class SixAccountsStrategySavingResult {
  constructor(
    readonly financialFreedom: Money,
    readonly longTerm: Money,
    readonly training: Money,
    readonly livingExpenses: Money,
    readonly leisure: Money,
    readonly donations: Money,
  ) {}
}
