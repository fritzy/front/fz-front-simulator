import { tracked } from '@glimmer/tracking';

export default class IncomeTaxationParameters {
  private static readonly INCOME_MAX = 200000;

  private static readonly INCOME_DEFAULT = 25000;

  @tracked private _income = IncomeTaxationParameters.INCOME_DEFAULT;

  get income() {
    return this._income;
  }

  set income(value: number) {
    if (value < 0 || value > this.incomeMax) {
      this._income = IncomeTaxationParameters.INCOME_DEFAULT;
    } else {
      this._income = value;
    }
  }

  get incomeMax() {
    return IncomeTaxationParameters.INCOME_MAX;
  }

  toQueryParams() {
    return {
      income: this._income,
    };
  }
}
