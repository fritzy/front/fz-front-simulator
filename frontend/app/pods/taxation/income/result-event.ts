import Money from 'bigint-money';

import type TaxSliceResult from './tax-slice-result';

export default class IncomeTaxationResultEvent {
  constructor(readonly year: number, readonly slices: TaxSliceResult[]) {}

  get total() {
    return new Money(
      this.slices
        .map((s) => s.amount)
        .reduce((a, b) => a.add(b))
        .toFixed(0),
      'EUR',
    );
  }
}
