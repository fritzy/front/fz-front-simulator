import type IncomeTaxationResultEvent from './result-event';
import type Money from 'bigint-money';

export default class IncomeTaxationResult {
  constructor(readonly taxableIncome: Money, readonly events: IncomeTaxationResultEvent[]) {}
}
