import { debug } from '@ember/debug';

import Money, { Round } from 'bigint-money';
import Moneys from 'fz-front-simulator/utils/moneys';

import IncomeTaxationResult from './result';
import IncomeTaxationResultEvent from './result-event';
import TaxSlice from './tax-slice';
import TaxSliceResult from './tax-slice-result';

import type IncomeTaxationParameters from './parameters';
import type Simulation from 'fz-front-simulator/objects/simulation';

export default class IncomeTaxationSimulation implements Simulation<IncomeTaxationParameters, IncomeTaxationResult> {
  private static readonly RATES: ReadonlyMap<number, TaxSlice[]> = new Map([
    [
      2012,
      [
        new TaxSlice(0, Moneys.ZERO_EUR, new Money(5963, 'EUR')),
        new TaxSlice(5.5, new Money(5964, 'EUR'), new Money(11896, 'EUR')),
        new TaxSlice(14, new Money(11897, 'EUR'), new Money(26420, 'EUR')),
        new TaxSlice(30, new Money(26421, 'EUR'), new Money(70830, 'EUR')),
        new TaxSlice(41, new Money(70831, 'EUR')),
      ],
    ],
    [
      2013,
      [
        new TaxSlice(0, Moneys.ZERO_EUR, new Money(5963, 'EUR')),
        new TaxSlice(5.5, new Money(5964, 'EUR'), new Money(11896, 'EUR')),
        new TaxSlice(14, new Money(11897, 'EUR'), new Money(26420, 'EUR')),
        new TaxSlice(30, new Money(26421, 'EUR'), new Money(70830, 'EUR')),
        new TaxSlice(41, new Money(70831, 'EUR'), new Money(150000, 'EUR')),
        new TaxSlice(45, new Money(150001, 'EUR')),
      ],
    ],
    [
      2014,
      [
        new TaxSlice(0, Moneys.ZERO_EUR, new Money(6011, 'EUR')),
        new TaxSlice(5.5, new Money(6012, 'EUR'), new Money(11991, 'EUR')),
        new TaxSlice(14, new Money(11992, 'EUR'), new Money(26631, 'EUR')),
        new TaxSlice(30, new Money(26632, 'EUR'), new Money(71397, 'EUR')),
        new TaxSlice(41, new Money(71398, 'EUR'), new Money(150200, 'EUR')),
        new TaxSlice(45, new Money(150201, 'EUR')),
      ],
    ],
    [
      2015,
      [
        new TaxSlice(0, Moneys.ZERO_EUR, new Money(9690, 'EUR')),
        new TaxSlice(14, new Money(9691, 'EUR'), new Money(26764, 'EUR')),
        new TaxSlice(30, new Money(26765, 'EUR'), new Money(71754, 'EUR')),
        new TaxSlice(41, new Money(71755, 'EUR'), new Money(151956, 'EUR')),
        new TaxSlice(45, new Money(151957, 'EUR')),
      ],
    ],
    [
      2016,
      [
        new TaxSlice(0, Moneys.ZERO_EUR, new Money(9700, 'EUR')),
        new TaxSlice(14, new Money(9701, 'EUR'), new Money(26791, 'EUR')),
        new TaxSlice(30, new Money(26792, 'EUR'), new Money(71826, 'EUR')),
        new TaxSlice(41, new Money(71827, 'EUR'), new Money(152108, 'EUR')),
        new TaxSlice(45, new Money(152109, 'EUR')),
      ],
    ],
    [
      2017,
      [
        new TaxSlice(0, Moneys.ZERO_EUR, new Money(9710, 'EUR')),
        new TaxSlice(14, new Money(9711, 'EUR'), new Money(26818, 'EUR')),
        new TaxSlice(30, new Money(26819, 'EUR'), new Money(71898, 'EUR')),
        new TaxSlice(41, new Money(71899, 'EUR'), new Money(152260, 'EUR')),
        new TaxSlice(45, new Money(152260, 'EUR')),
      ],
    ],
    [
      2018,
      [
        new TaxSlice(0, Moneys.ZERO_EUR, new Money(9807, 'EUR')),
        new TaxSlice(14, new Money(9808, 'EUR'), new Money(27086, 'EUR')),
        new TaxSlice(30, new Money(27087, 'EUR'), new Money(72617, 'EUR')),
        new TaxSlice(41, new Money(72618, 'EUR'), new Money(153783, 'EUR')),
        new TaxSlice(45, new Money(153784, 'EUR')),
      ],
    ],
    [
      2019,
      [
        new TaxSlice(0, Moneys.ZERO_EUR, new Money(9964, 'EUR')),
        new TaxSlice(14, new Money(9965, 'EUR'), new Money(27519, 'EUR')),
        new TaxSlice(30, new Money(27520, 'EUR'), new Money(73779, 'EUR')),
        new TaxSlice(41, new Money(73780, 'EUR'), new Money(156244, 'EUR')),
        new TaxSlice(45, new Money(156245, 'EUR')),
      ],
    ],
    [
      2020,
      [
        new TaxSlice(0, Moneys.ZERO_EUR, new Money(10064, 'EUR')),
        new TaxSlice(11, new Money(10065, 'EUR'), new Money(25659, 'EUR')),
        new TaxSlice(30, new Money(25660, 'EUR'), new Money(73369, 'EUR')),
        new TaxSlice(41, new Money(73370, 'EUR'), new Money(157806, 'EUR')),
        new TaxSlice(45, new Money(157807, 'EUR')),
      ],
    ],
    [
      2021,
      [
        new TaxSlice(0, Moneys.ZERO_EUR, new Money(10084, 'EUR')),
        new TaxSlice(11, new Money(10085, 'EUR'), new Money(25710, 'EUR')),
        new TaxSlice(30, new Money(25711, 'EUR'), new Money(73516, 'EUR')),
        new TaxSlice(41, new Money(73517, 'EUR'), new Money(158122, 'EUR')),
        new TaxSlice(45, new Money(158123, 'EUR')),
      ],
    ],
    [
      2022,
      [
        new TaxSlice(0, Moneys.ZERO_EUR, new Money(10224, 'EUR')),
        new TaxSlice(11, new Money(10225, 'EUR'), new Money(26070, 'EUR')),
        new TaxSlice(30, new Money(26071, 'EUR'), new Money(74545, 'EUR')),
        new TaxSlice(41, new Money(74546, 'EUR'), new Money(160336, 'EUR')),
        new TaxSlice(45, new Money(160337, 'EUR')),
      ],
    ],
  ]);

  async simulate(parameters: IncomeTaxationParameters) {
    debug(`Running income taxes simulation with {income: ${parameters.income}}`);

    const incomeMoney = new Money(parameters.income.toFixed(2), 'EUR');

    const taxableIncomeMoney = new Money(
      new Money('0.9', 'EUR', Round.HALF_AWAY_FROM_0).multiply(incomeMoney).toFixed(0),
      'EUR',
    );

    const taxes = [...IncomeTaxationSimulation.RATES.entries()].map(
      ([year, slices]: [number, TaxSlice[]]) =>
        new IncomeTaxationResultEvent(
          year,
          slices.map((slice) => new TaxSliceResult(slice, this.computeAmount(taxableIncomeMoney, slice))),
        ),
    );

    return new IncomeTaxationResult(taxableIncomeMoney, taxes);
  }

  private computeAmount(value: Money, taxSlice: TaxSlice): Money {
    if (value.isLesserThan(taxSlice.startInclusive)) {
      return Moneys.ZERO_EUR;
    } else if (!taxSlice.endInclusive || value.isLesserThan(taxSlice.endInclusive)) {
      return value
        .subtract(Moneys.max(taxSlice.startInclusive.subtract(1), Moneys.ZERO_EUR))
        .multiply(taxSlice.taxYield.toFixed(1))
        .divide(100);
    } else {
      return taxSlice.endInclusive
        .subtract(Moneys.max(taxSlice.startInclusive.subtract(1), Moneys.ZERO_EUR))
        .multiply(taxSlice.taxYield.toFixed(1))
        .divide(100);
    }
  }
}
