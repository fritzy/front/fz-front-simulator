import type TaxSlice from './tax-slice';
import type Money from 'bigint-money';

export default class TaxSliceResult {
  constructor(readonly slice: TaxSlice, readonly amount: Money) {}
}
