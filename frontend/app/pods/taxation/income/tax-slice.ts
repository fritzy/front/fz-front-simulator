import type Money from 'bigint-money';

export default class TaxSlice {
  constructor(readonly taxYield: number, readonly startInclusive: Money, readonly endInclusive?: Money) {}
}
