import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

import type TaxationIncomeController from './controller';
import type IntlService from 'ember-intl/services/intl';
import type HeadDataService from 'fz-front-simulator/services/head-data';

export default class TaxationIncome extends Route {
  @service declare headData: HeadDataService;
  @service declare intl: IntlService;

  async afterModel() {
    this.headData.title = `${this.intl.t('pods.taxation.income.breadcrumb')}
    - ${this.intl.t('pods.taxation.breadcrumb')} | ${this.intl.t('application.title')}`;
    this.headData.description = this.intl.t('pods.taxation.income.description');
  }

  setupController(controller: TaxationIncomeController) {
    controller.setup();
  }
}
