import { tracked } from '@glimmer/tracking';
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import Simulators from 'fz-front-simulator/utils/simulators';

import IncomeTaxationParameters from './parameters';

import type IntlService from 'ember-intl/services/intl';
import type ApplicationService from 'fz-front-simulator/services/application';
import type RouterConfigService from 'fz-front-simulator/services/router-config';

export default class TaxationIncomeController extends Controller {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;
  @service declare routerConfig: RouterConfigService;

  queryParams = [
    {
      queryParamIncome: 'income',
      queryParamTitle: 'title',
    },
  ];

  queryParamIncome?: string;
  queryParamTitle?: string;

  @tracked title = '';

  parameters: IncomeTaxationParameters;

  constructor(args: Record<string, unknown> | undefined) {
    super(args);
    this.parameters = new IncomeTaxationParameters();
  }

  setup() {
    this.title = this.queryParamTitle || this.intl.t('pods.taxation.income.title');

    if (Number(this.queryParamIncome)) {
      this.parameters.income = Number(this.queryParamIncome);
    }
  }

  @action
  onLanguageChange() {
    const route = this.routerConfig.getSimulatorRoute(Simulators.TAXATION_INCOME);
    const queryParams = this.parameters.toQueryParams();

    if (route) {
      this.transitionToRoute(route, { queryParams: queryParams });
    }
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'taxes-income-controller': TaxationIncomeController;
  }
}
