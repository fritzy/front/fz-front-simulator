import type Money from 'bigint-money';
import type CryptoTaxEventFunding from 'fz-front-simulator/models/crypto-tax-event-funding';

export default class CryptoCapitalGainLossHistory {
  constructor(readonly event: CryptoTaxEventFunding, readonly value: Money) {}
}
