import { set } from '@ember/object';

import Money from 'bigint-money';
import Moneys from 'fz-front-simulator/utils/moneys';

export default class CryptoBalance {
  balance: Money;
  fractionSold: Money;
  purchasePrice: Money;
  total: Money;

  constructor(currency: string, balance?: Money, total?: Money, purchasePrice?: Money, fractionSold?: Money) {
    this.balance = balance ? balance : new Money(0, currency);
    this.purchasePrice = purchasePrice ? purchasePrice : Moneys.ZERO_EUR;
    this.fractionSold = fractionSold ? fractionSold : Moneys.ZERO_EUR;
    this.total = total ? total : Moneys.ZERO_EUR;
  }

  add(balance: Money, total: Money): void {
    set(this, 'balance', this.balance.add(balance));
    this.purchasePrice = this.purchasePrice.add(total);

    if (balance.sign() !== 0) {
      set(this, 'total', total.divide(balance).multiply(this.balance));
    }
  }

  setPrice(price: Money): void {
    set(this, 'total', price.multiply(this.balance));
  }

  remove(balance: Money, total: Money): void {
    set(this, 'balance', this.balance.subtract(balance));

    if (balance.sign() !== 0) {
      set(this, 'total', total.divide(balance).multiply(this.balance));
    }
  }

  get averagePrice(): Money {
    if (this.balance.toSource() === 0n) {
      return this.total;
    } else {
      return this.total.divide(this.balance);
    }
  }
}
