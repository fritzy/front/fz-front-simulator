import type CryptoBalance from './crypto-balance';
import type CryptoTaxEvent from 'fz-front-simulator/models/crypto-tax-event';

export default class CryptoBalanceHistory {
  constructor(readonly event: CryptoTaxEvent, readonly balances: CryptoBalance[]) {}
}
