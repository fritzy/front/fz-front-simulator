import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

import { perform } from 'ember-concurrency-ts';

import type Ember from 'ember';
import type IntlService from 'ember-intl/services/intl';
import type CryptoTaxEventFunding from 'fz-front-simulator/models/crypto-tax-event-funding';
import type CryptoService from 'fz-front-simulator/services/crypto-service';
import type HeadDataService from 'fz-front-simulator/services/head-data';
export default class TaxesCrypto extends Route {
  @service declare cryptoService: CryptoService;
  @service declare headData: HeadDataService;
  @service declare intl: IntlService;

  async beforeModel(): Promise<void> {
    void perform(this.cryptoService.fetchCryptosTask);
  }

  model(): Ember.ArrayProxy<CryptoTaxEventFunding[]> {
    return this.store.findAll('crypto-tax-event-funding');
  }

  async afterModel() {
    this.headData.title = `${this.intl.t('pods.taxation.crypto.breadcrumb')}
    - ${this.intl.t('pods.taxation.breadcrumb')} | ${this.intl.t('application.title')}`;
    this.headData.description = this.intl.t('pods.taxation.crypto.description');
  }
}
