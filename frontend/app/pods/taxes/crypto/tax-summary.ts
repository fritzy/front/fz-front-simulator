import { set } from '@ember/object';

import Money from 'bigint-money';

export default class TaxSummary {
  public static readonly ZERO_EUR = new Money(0, 'EUR');

  profit: Money;

  constructor(readonly year: number) {
    this.profit = TaxSummary.ZERO_EUR;
  }

  add(profit: Money): void {
    set(this, 'profit', this.profit.add(profit));
  }

  get taxes(): Money {
    return this.profit.sign() > 0 ? this.profit.multiply('0.30') : TaxSummary.ZERO_EUR;
  }
}
