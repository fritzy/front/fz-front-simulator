import { tracked } from '@glimmer/tracking';
import { A } from '@ember/array';
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import Money from 'bigint-money';
import { CryptoTaxEventType } from 'fz-front-simulator/enums/crypto-tax-event-type';
import CryptoTaxEventFunding from 'fz-front-simulator/models/crypto-tax-event-funding';
import Am4ChartsStackedHistoryData from 'fz-front-simulator/objects/ac4/stacked-history-data';
import Am4ChartsStackedHistoryElementData from 'fz-front-simulator/objects/ac4/stacked-history-element-data';
import Am4ChartsStackedHistorySerie from 'fz-front-simulator/objects/ac4/stacked-history-serie';
import Moneys from 'fz-front-simulator/utils/moneys';

import CryptoBalance from './crypto-balance';
import CryptoBalanceHistory from './crypto-balance-history';
import CryptoCapitalGainLossHistory from './crypto-capital-gain-loss-history';
import TaxSummary from './tax-summary';

import type Ember from 'ember';
import type CryptoTaxEvent from 'fz-front-simulator/models/crypto-tax-event';
import type ApplicationService from 'fz-front-simulator/services/application';

export default class TaxesCryptoController extends Controller {
  @service declare application: ApplicationService;

  public static readonly ZERO_EUR = new Money(0, 'EUR');

  @tracked addModalVisible = false;

  @tracked _events: Ember.NativeArray<CryptoTaxEvent> = A();

  get persistedEvents() {
    return this.model.filter((m: CryptoTaxEvent) => !m.isNew);
  }

  get sortedEvents() {
    return this.persistedEvents.sort(
      (e1: CryptoTaxEvent, e2: CryptoTaxEvent) => e1.dateAsDate.getTime() - e2.dateAsDate.getTime(),
    );
  }

  get balances(): CryptoBalance[] {
    if (this.balanceHistories.length > 0) {
      return this.balanceHistories[this.balanceHistories.length - 1].balances;
    }

    return [];
  }

  get balanceHistories(): CryptoBalanceHistory[] {
    return this.sortedEvents.reduce((array: CryptoBalanceHistory[], event: CryptoTaxEvent) => {
      if (event instanceof CryptoTaxEventFunding) {
        const history = new CryptoBalanceHistory(event, []);

        if (array.length === 0) {
          history.balances.push(new CryptoBalance(event.quantity.currency));
        } else {
          array[array.length - 1].balances.forEach((b) => {
            const balance = new CryptoBalance(b.balance.currency, b.balance, b.total, b.purchasePrice);

            history.balances.push(balance);
          });
        }

        if (event.tickers) {
          event.tickers.forEach((t) =>
            history.balances.find((b) => b.balance.currency === t.cryptoCurrency)?.setPrice(t.price),
          );
        }

        let currentBalance = history.balances.find((b) => b.balance.currency === event.quantity.currency);

        if (!currentBalance) {
          currentBalance = new CryptoBalance(event.quantity.currency);
          history.balances.push(currentBalance);
        }

        if (currentBalance && event.type === CryptoTaxEventType.BUY) {
          currentBalance.add(event.quantity, event.price);
        } else if (currentBalance && event.type === CryptoTaxEventType.SELL) {
          currentBalance.remove(event.quantity, event.price);
        }

        array.push(history);
      }

      return array;
    }, new Array<CryptoBalanceHistory>());
  }

  get historyData(): Am4ChartsStackedHistoryData | undefined {
    if (this.balanceHistories.length > 1) {
      const labels = this.balanceHistories[this.balanceHistories.length - 1].balances.map((b) => b.balance.currency);

      const elements = this.balanceHistories.reduce(
        (values: Am4ChartsStackedHistoryElementData[], value: CryptoBalanceHistory) => {
          const current = new Am4ChartsStackedHistoryElementData(new Date(value.event.date));

          values.push(current);

          const indexes = [...Array(labels.length).keys()];

          indexes.forEach(
            (i) => (current[i + 1] = value.balances[i] ? parseFloat(value.balances[i].total.toFixed(2)) : 0),
          );

          return values;
        },
        new Array<Am4ChartsStackedHistoryElementData>(),
      );

      const series = labels.map((l) => new Am4ChartsStackedHistorySerie(l, true));

      return new Am4ChartsStackedHistoryData('€', series, elements);
    }

    return undefined;
  }

  get gainLossHistories(): CryptoCapitalGainLossHistory[] {
    return this.sortedEvents
      .filter((e: CryptoTaxEvent) => e.type === CryptoTaxEventType.SELL)
      .reduce((array: CryptoCapitalGainLossHistory[], event: CryptoTaxEventFunding) => {
        const currentBalanceHistory = this.balanceHistories.find((e) => e.event === event);

        if (currentBalanceHistory) {
          const porfolioAmount = currentBalanceHistory.balances
            .reduce((total, balance) => total.add(balance.total), Moneys.ZERO_EUR)
            .add(event.price); // 212

          const purchasePrice = currentBalanceHistory.balances.reduce(
            (total, balance) => total.add(balance.purchasePrice),
            Moneys.ZERO_EUR,
          ); // 220

          const netPurchasePrice = purchasePrice.subtract(
            currentBalanceHistory.balances.reduce((total, balance) => total.add(balance.fractionSold), Moneys.ZERO_EUR),
          ); // 223

          const cryptoBalance = currentBalanceHistory.balances.find(
            (b) => b.balance.currency === event.quantity.currency,
          );

          if (cryptoBalance) {
            const fractionSold = porfolioAmount.isGreaterThan(Moneys.ZERO_EUR)
              ? netPurchasePrice.multiply(event.price).divide(porfolioAmount)
              : Moneys.ZERO_EUR; // 221

            cryptoBalance.fractionSold = fractionSold;

            const currentDate = new Date(event.date);

            this.balanceHistories
              .filter((h) => new Date(h.event.date) > currentDate)
              .forEach((h) =>
                h.balances
                  .filter((b) => b.balance.currency === event.quantity.currency)
                  .forEach((b) => (b.fractionSold = fractionSold)),
              );
          }

          const gainLoss = porfolioAmount.isGreaterThan(TaxesCryptoController.ZERO_EUR)
            ? event.price.subtract(netPurchasePrice.multiply(event.price).divide(porfolioAmount))
            : TaxesCryptoController.ZERO_EUR;

          array.push(new CryptoCapitalGainLossHistory(event, gainLoss));
        }

        return array;
      }, new Array<CryptoCapitalGainLossHistory>());
  }

  get gainLossSummaries(): TaxSummary[] {
    return this.gainLossHistories.reduce((array, history) => {
      const year = parseInt(history.event.date.substring(0, 4));
      let currentHistory = array.find((h) => h.year === year);

      if (!currentHistory) {
        currentHistory = new TaxSummary(year);
        array.push(currentHistory);
      }

      currentHistory.add(history.value);

      return array;
    }, new Array<TaxSummary>());
  }

  @action
  async onAdd(event: CryptoTaxEvent): Promise<void> {
    await event.save();
  }

  @action
  hideAddModal(): void {
    this.addModalVisible = false;
  }

  @action
  showAddModal(): void {
    this.addModalVisible = true;
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'taxes-crypto-controller': TaxesCryptoController;
  }
}
