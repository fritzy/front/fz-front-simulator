import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import type RouterService from '@ember/routing/router-service';
import type IntlService from 'ember-intl/services/intl';
import type RouterConfigService from 'fz-front-simulator/services/router-config';

export default class LegalNoticesController extends Controller {
  @service declare intl: IntlService;
  @service declare router: RouterService;
  @service declare routerConfig: RouterConfigService;

  title = '';

  setup() {
    this.title = this.intl.t('pods.legal-notices.title');
  }

  @action
  onLanguageChange() {
    const route = this.routerConfig.legalNoticesRoute;

    if (route) {
      this.router.transitionTo(route);
    }
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'legal-notices-controller': LegalNoticesController;
  }
}
