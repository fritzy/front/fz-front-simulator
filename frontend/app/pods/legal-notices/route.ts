import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

import type LegalNoticesController from './controller';
import type IntlService from 'ember-intl/services/intl';
import type HeadDataService from 'fz-front-simulator/services/head-data';

export default class LegalNotices extends Route {
  @service declare headData: HeadDataService;
  @service declare intl: IntlService;

  afterModel() {
    this.headData.title = `${this.intl.t('pods.legal-notices.breadcrumb')} | ${this.intl.t('application.title')}`;
    this.headData.description = this.intl.t('pods.legal-notices.description');
  }

  setupController(controller: LegalNoticesController) {
    controller.setup();
  }
}
