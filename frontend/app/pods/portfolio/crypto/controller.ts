import { tracked } from '@glimmer/tracking';
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import { perform, taskFor } from 'ember-concurrency-ts';
import Moneys from 'fz-front-simulator/utils/moneys';

import AggregatedCryptoAsset from './aggregated-crypto-asset';
import CryptoAsset from './crypto-asset';

import type Money from 'bigint-money';
import type IntlService from 'ember-intl/services/intl';
import type CryptoHolding from 'fz-front-simulator/models/crypto-holding';
import type CoinGeckoCoin from 'fz-front-simulator/objects/cgc/coin-gecko-coin';
import type ApplicationService from 'fz-front-simulator/services/application';
import type CryptoService from 'fz-front-simulator/services/crypto-service';

export default class PorfolioCryptoController extends Controller {
  @service cryptoService!: CryptoService;
  @service application!: ApplicationService;
  @service intl!: IntlService;

  queryParams = ['title'];

  title = '';

  @tracked addModalVisible = false;

  @tracked finalTitle = '';

  setup() {
    this.finalTitle = this.title || this.intl.t('pods.saving.crypto.title');
  }

  get assets(): CryptoAsset[] {
    return this.persistedHoldings.map((h) => {
      const price = this.cryptoService.mapCurrentPrices.get(h.cryptoCurrency?.id as string) || Moneys.ZERO_EUR;

      return new CryptoAsset(h, price.multiply(h.quantity));
    });
  }

  get aggregatedAssets(): AggregatedCryptoAsset[] {
    return this.assets.reduce((array, asset) => {
      const aggregated = array.find(
        (agg) => agg.assets[0].holding.quantity.currency === asset.holding.quantity.currency,
      );

      if (!aggregated) {
        array.push(new AggregatedCryptoAsset([asset]));
      } else {
        aggregated.assets.push(asset);
      }

      return array;
    }, new Array<AggregatedCryptoAsset>());
  }

  get persistedHoldings(): CryptoHolding[] {
    return this.model.filter((m: CryptoHolding) => !m.isNew);
  }

  get total(): Money {
    return this.aggregatedAssets.reduce((total, agg) => total.add(agg.total), Moneys.ZERO_EUR);
  }

  get btcShare(): number {
    const btcAmount =
      this.aggregatedAssets.find((agg) => agg.assets[0].holding.quantity.currency === 'bitcoin')?.total ||
      Moneys.ZERO_EUR;

    if (this.total.isGreaterThan(Moneys.ZERO_EUR)) {
      return parseFloat(btcAmount.divide(this.total).multiply(100).toFixed(4));
    }

    return 0;
  }

  @action
  async onAdd(holding: CryptoHolding): Promise<void> {
    await holding.save();

    await perform(
      this.cryptoService.loadCurrentPricesTask,
      this.persistedHoldings.map((h) => h.cryptoCurrency) as CoinGeckoCoin[],
    );
  }

  get assetsLoading(): boolean {
    return taskFor(this.cryptoService.loadCurrentPricesTask).isRunning;
  }

  @action
  hideAddModal(): void {
    this.addModalVisible = false;
  }

  @action
  showAddModal(): void {
    this.addModalVisible = true;
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'portfolio-crypto-controller': PorfolioCryptoController;
  }
}
