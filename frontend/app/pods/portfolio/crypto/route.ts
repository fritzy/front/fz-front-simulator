import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

import { perform } from 'ember-concurrency-ts';

import type PorfolioCryptoController from './controller';
import type Transition from '@ember/routing/-private/transition';
import type { TaskInstanceForAsyncTaskFunction } from 'ember-concurrency';
import type IntlService from 'ember-intl/services/intl';
import type CryptoHolding from 'fz-front-simulator/models/crypto-holding';
import type CoinGeckoCoin from 'fz-front-simulator/objects/cgc/coin-gecko-coin';
import type ApplicationService from 'fz-front-simulator/services/application';
import type CryptoService from 'fz-front-simulator/services/crypto-service';
import type HeadDataService from 'fz-front-simulator/services/head-data';

export default class PortfolioCrypto extends Route {
  @service declare cryptoService: CryptoService;
  @service declare headData: HeadDataService;
  @service declare application: ApplicationService;
  @service declare intl: IntlService;

  fetchCryptosTaskInstance?: TaskInstanceForAsyncTaskFunction<() => Promise<void>>;

  async beforeModel(): Promise<void> {
    this.fetchCryptosTaskInstance = perform(this.cryptoService.fetchCryptosTask);
  }

  async model(): Promise<CryptoHolding[]> {
    const holdings = this.application.indexedDbAvailable
      ? ((await this.store.findAll('crypto-holding')) as unknown as CryptoHolding[])
      : [];

    await this.fetchCryptosTaskInstance;
    await perform(this.cryptoService.loadCurrentPricesTask, holdings.map((h) => h.cryptoCurrency) as CoinGeckoCoin[]);

    return holdings;
  }

  async afterModel() {
    this.headData.title = `${this.intl.t('pods.portfolio.crypto.breadcrumb')}
    - ${this.intl.t('pods.portfolio.breadcrumb')} | ${this.intl.t('application.title')}`;
    this.headData.description = this.intl.t('pods.portfolio.crypto.description');
  }

  setupController(controller: PorfolioCryptoController, model: CryptoHolding[], transition: Transition<unknown>) {
    super.setupController(controller, model, transition);
    controller.setup();
  }
}
