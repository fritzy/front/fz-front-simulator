import type Money from 'bigint-money';
import type CryptoHolding from 'fz-front-simulator/models/crypto-holding';

export default class CryptoAsset {
  constructor(readonly holding: CryptoHolding, readonly total: Money) {}

  get crypto(): string | undefined {
    return this.holding.cryptoCurrency?.name;
  }

  get totalAsNumber(): number {
    return parseFloat(this.total.toFixed(2));
  }
}
