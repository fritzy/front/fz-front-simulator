import Money from 'bigint-money';
import Moneys from 'fz-front-simulator/utils/moneys';

import type CryptoAsset from './crypto-asset';

export default class AggregatedCryptoAsset {
  constructor(readonly assets: CryptoAsset[]) {}

  get crypto(): string | undefined {
    return this.assets[0].crypto;
  }

  get quantity(): Money {
    if (this.assets[0].holding.cryptoCurrency) {
      return this.assets.reduce(
        (t, asset) => t.add(asset.holding.quantity),
        new Money(0, this.assets[0].holding.cryptoCurrency.id),
      );
    }

    return Moneys.ZERO_EUR;
  }

  get total(): Money {
    return this.assets.reduce((t, asset) => t.add(asset.total), Moneys.ZERO_EUR);
  }

  get totalAsNumber(): number {
    return parseFloat(this.total.toFixed(2));
  }
}
