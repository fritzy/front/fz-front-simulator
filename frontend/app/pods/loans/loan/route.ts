import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

import type IntlService from 'ember-intl/services/intl';
import type HeadDataService from 'fz-front-simulator/services/head-data';

export default class LoansLoan extends Route {
  @service declare headData: HeadDataService;
  @service declare intl: IntlService;

  async afterModel() {
    this.headData.title = `${this.intl.t('pods.loan.breadcrumb')} | ${this.intl.t('application.title')}`;
    this.headData.description = this.intl.t('pods.loan.description');
  }
}
