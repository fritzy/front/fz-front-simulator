import { tracked } from '@glimmer/tracking';
import Controller from '@ember/controller';
import { debug } from '@ember/debug';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import { Money } from 'bigint-money';
import { restartableTask, timeout } from 'ember-concurrency';
import { perform } from 'ember-concurrency-ts';
import Am4ChartsStackedHistoryData from 'fz-front-simulator/objects/ac4/stacked-history-data';
import Am4ChartsStackedHistoryElementData from 'fz-front-simulator/objects/ac4/stacked-history-element-data';
import Am4ChartsStackedHistorySerie from 'fz-front-simulator/objects/ac4/stacked-history-serie';
import LoanHistory from 'fz-front-simulator/objects/loan-history';
import Moneys from 'fz-front-simulator/utils/moneys';

import type StoreService from '@ember-data/store';
import type Loan from 'fz-front-simulator/models/loan';
import type ApplicationService from 'fz-front-simulator/services/application';

export default class LoanController extends Controller {
  @service application!: ApplicationService;
  @service store!: StoreService;

  queryParams = ['title'];
  title = 'Crédit';

  @tracked histories: LoanHistory[] = [];

  @tracked loan!: Loan;

  constructor(args: Record<string, unknown> | undefined) {
    super(args);

    this.loan = this.store.createRecord('loan');
    this.loan.amount = 100000;
    this.loan.duration = 15;
    this.loan.insuranceYield = 0;
    this.loan.yield = 1.1;
    this.doComputeHistories();
  }

  @restartableTask
  async historiesComputation(): Promise<void> {
    await timeout(250);
    this.computeHistories();
  }

  @action
  async doComputeHistories(): Promise<void> {
    perform(this.historiesComputation);
  }

  computeHistories(): void {
    debug('hitories computation');

    const histories: LoanHistory[] = [];
    const date = new Date();
    const moneyBalance = new Money(this.loan.amount, 'EUR');
    const baseRate = this.loan.yield / 100;
    const moneyRate = new Money(baseRate.toString(), 'EUR');

    const monthlyPayment = this.computeMonthlyPayment(moneyBalance, moneyRate, this.loan.duration * 12);

    histories.push(new LoanHistory(date, Moneys.ZERO_EUR, Moneys.ZERO_EUR, Moneys.ZERO_EUR, moneyBalance));

    for (let m = 0; m < this.loan.duration * 12; m++) {
      const previous = histories[m];
      const newDate = new Date();

      newDate.setMonth(date.getMonth() + m + 1);

      const interests = previous.balance.multiply(moneyRate).divide(12);

      histories.push(
        new LoanHistory(
          newDate,
          monthlyPayment,
          monthlyPayment.subtract(interests),
          interests,
          previous.balance.add(interests).subtract(monthlyPayment),
        ),
      );
    }

    this.histories = histories;
  }

  computeMonthlyPayment(amount: Money, rate: Money, months: number): Money {
    if (rate.isEqual(Moneys.ZERO_EUR)) {
      return amount.divide(months);
    }

    const numerator = amount.multiply(rate).divide(12);
    const denominator = rate.divide(12).add(1).pow(-months).multiply(-1).add(1);

    return numerator.divide(denominator);
  }

  @action
  dummyAction(): void {
    // Dummy action
  }

  get historyData(): Am4ChartsStackedHistoryData {
    debug('histories data computation');

    const elements = this.histories
      .slice(1, this.histories.length)
      .reduce((values: Am4ChartsStackedHistoryElementData[], value: LoanHistory) => {
        const current = new Am4ChartsStackedHistoryElementData(value.date);

        values.push(current);
        current[1] = parseFloat(value.capital.toFixed(2));
        current[2] = parseFloat(value.interests.toFixed(2));

        return values;
      }, new Array<Am4ChartsStackedHistoryElementData>());

    const labels = [
      new Am4ChartsStackedHistorySerie('Capital', true),
      new Am4ChartsStackedHistorySerie('Intérêts', true),
    ];

    return new Am4ChartsStackedHistoryData('€', labels, elements);
  }

  get hasHistoryData(): boolean {
    return this.historyData.elements.length > 0;
  }

  get pieData(): { label: string; value: string }[] {
    const pieData = [];

    pieData.push({ label: 'Capital', value: this.loan.amount.toString() });
    pieData.push({ label: 'Intérêts', value: this.interestsCost.toFixed(2) });

    return pieData;
  }

  get result(): LoanHistory {
    return this.histories[this.histories.length - 1];
  }

  get interestsCost(): Money {
    return this.histories.map((h) => h.interests).reduce((a, b) => a.add(b));
  }

  @action
  setAmount(value: number): void {
    this.loan.amount = value;
    this.doComputeHistories();
  }

  @action
  setInsuranceYield(value: number): void {
    this.loan.insuranceYield = value;
    this.doComputeHistories();
  }

  @action
  setYield(value: number): void {
    this.loan.yield = value;
    this.doComputeHistories();
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'loan-controller': LoanController;
  }
}
