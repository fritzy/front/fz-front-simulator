import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

import type IntlService from 'ember-intl/services/intl';
import type HeadDataService from 'fz-front-simulator/services/head-data';
import type IntlConfigService from 'fz-front-simulator/services/intl-config';

export default class Integration extends Route {
  @service declare headData: HeadDataService;
  @service declare intl: IntlService;
  @service declare intlConfig: IntlConfigService;

  async afterModel() {
    this.headData.title = `${this.intl.t('pods.integration.breadcrumb')} | ${this.intl.t('application.title')}`;
    this.headData.description = this.intl.t('pods.integration.description');
  }
}
