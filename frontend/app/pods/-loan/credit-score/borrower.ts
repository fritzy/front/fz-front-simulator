import { tracked } from '@glimmer/tracking';

import { SocioProfessionalCategory } from 'fz-front-simulator/enums/socio-professional-category';

export default class Borrower {
  private static readonly AGE_DEFAULT = 37;
  private static readonly AGE_MAX = 100;

  private static readonly SOCIO_PROFESSIONAL_CATEGORY_YEARS_DEFAULT = 2;
  private static readonly SOCIO_PROFESSIONAL_CATEGORY_YEARS_MAX = 40;

  @tracked private _age = Borrower.AGE_DEFAULT;
  @tracked private _socioProfessionalCategory = SocioProfessionalCategory.PRIVATE_SECTOR_EMPLOYEE;
  @tracked private _socioProfessionalCategoryYears = Borrower.SOCIO_PROFESSIONAL_CATEGORY_YEARS_DEFAULT;

  get age() {
    return this._age;
  }

  set age(value: number) {
    if (value >= 18 && value <= this.ageMax) {
      this._age = value;
    }
  }

  get ageMax() {
    return Borrower.AGE_MAX;
  }

  get socioProfessionalCategory() {
    return this._socioProfessionalCategory;
  }

  set socioProfessionalCategory(value: SocioProfessionalCategory) {
    this._socioProfessionalCategory = value;
  }

  get socioProfessionalCategoryYears() {
    return this._socioProfessionalCategoryYears;
  }

  set socioProfessionalCategoryYears(value: number) {
    if (value >= 0 && value <= this.socioProfessionalCategoryYearsMax) {
      this._socioProfessionalCategoryYears = value;
    }
  }

  get socioProfessionalCategoryYearsMax() {
    return Borrower.SOCIO_PROFESSIONAL_CATEGORY_YEARS_MAX;
  }
}
