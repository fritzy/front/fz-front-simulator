export default class LoanCreditScoreResult {
  constructor(readonly score: number) {}
}
