import { tracked } from '@glimmer/tracking';
import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

import LoanCreditScoreParameters from './parameters';

import type IntlService from 'ember-intl/services/intl';
import type ApplicationService from 'fz-front-simulator/services/application';
import type RouterConfigService from 'fz-front-simulator/services/router-config';

export default class LoanCreditScoreController extends Controller {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;
  @service declare routerConfig: RouterConfigService;

  queryParams = [
    'bankingIncidents',
    'children',
    'contribution',
    'debtRate',
    'duration',
    'income',
    'liveRemainder',
    'loanAmount',
    'residentialStatus',
    'savings',
    {
      queryParamTitle: 'title',
      borrowersAge: {
        type: 'array',
      } as const,
      borrowersSocioProfessionalCategory: {
        type: 'array',
      } as const,
      borrowersSocioProfessionalCategoryYears: {
        type: 'array',
      } as const,
    },
  ];

  borrowersAge?: string[];
  borrowersSocioProfessionalCategory?: string[];
  borrowersSocioProfessionalCategoryYears?: string[];
  bankingIncidents?: string;
  children?: string;
  contribution?: string;
  debtRate?: string;
  duration?: string;
  income?: string;
  liveRemainder?: string;
  loanAmount?: string;
  residentialStatus?: string;
  savings?: string;

  queryParamTitle?: string;

  @tracked title = '';

  parameters: LoanCreditScoreParameters;

  constructor(args: Record<string, unknown> | undefined) {
    super(args);
    this.parameters = new LoanCreditScoreParameters();
  }

  setup() {
    this.title = this.queryParamTitle || this.intl.t('pods.loan.credit-score.title');
    this.parameters.fromQueryParams({
      borrowersAge: this.borrowersAge,
      borrowersSocioProfessionalCategory: this.borrowersSocioProfessionalCategory,
      borrowersSocioProfessionalCategoryYears: this.borrowersSocioProfessionalCategoryYears,
      bankingIncidents: this.bankingIncidents,
      children: this.children,
      contribution: this.contribution,
      debtRate: this.debtRate,
      duration: this.duration,
      income: this.income,
      liveRemainder: this.liveRemainder,
      loanAmount: this.loanAmount,
      residentialStatus: this.residentialStatus,
      savings: this.savings,
    });
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'loan-credit-score-controller': LoanCreditScoreController;
  }
}
