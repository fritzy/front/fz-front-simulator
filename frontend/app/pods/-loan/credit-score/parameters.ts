import { tracked } from '@glimmer/tracking';

import { ResidentialStatus } from 'fz-front-simulator/enums/residential-status';

import Borrower from './borrower';

import type Exportable from 'fz-front-simulator/objects/exportable';

interface LoanCreditScoreParametersQueryParameters {
  borrowersAge: string[] | undefined;
  borrowersSocioProfessionalCategory: string[] | undefined;
  borrowersSocioProfessionalCategoryYears: string[] | undefined;
  bankingIncidents: string | undefined;
  children: string | undefined;
  contribution: string | undefined;
  debtRate: string | undefined;
  duration: string | undefined;
  income: string | undefined;
  liveRemainder: string | undefined;
  loanAmount: string | undefined;
  residentialStatus: string | undefined;
  savings: string | undefined;
}

export default class LoanCreditScoreParameters implements Exportable<LoanCreditScoreParametersQueryParameters> {
  private static readonly BANKING_INCIDENTS_DEFAULT = 0;
  private static readonly BANKING_INCIDENTS_MAX = 10;

  private static readonly BORROWERS_MAX = 2;

  private static readonly CHILDREN_DEFAULT = 0;
  private static readonly CHILDREN_MAX = 10;

  private static readonly CONTRIBUTION_DEFAULT = 10000;
  private static readonly CONTRIBUTION_MAX = 500000;

  private static readonly DEBT_RATE_DEFAULT = 25;
  private static readonly DEBT_RATE_MAX = 40;

  private static readonly DURATION_DEFAULT = 20;
  private static readonly DURATION_MAX = 30;
  private static readonly DURATION_MIN = 1;

  private static readonly INCOME_DEFAULT = 25000;
  private static readonly INCOME_MAX = 500000;

  private static readonly LIVE_REMAINDER_DEFAULT = 900;
  private static readonly LIVE_REMAINDER_MAX = 40000;

  private static readonly LOAN_AMOUNT_DEFAULT = 100000;
  private static readonly LOAN_AMOUNT_MAX = 500000;

  private static readonly RESIDENTIAL_STATUS_DEFAULT = ResidentialStatus.TENANT;

  private static readonly SAVINGS_DEFAULT = 20000;
  private static readonly SAVINGS_MAX = 2000000;

  @tracked private _bankingIncidents = LoanCreditScoreParameters.BANKING_INCIDENTS_DEFAULT;

  @tracked private _borrowers = [new Borrower()];

  @tracked private _children = LoanCreditScoreParameters.CHILDREN_DEFAULT;

  @tracked private _contribution = LoanCreditScoreParameters.CONTRIBUTION_DEFAULT;

  @tracked private _debtRate = LoanCreditScoreParameters.DEBT_RATE_DEFAULT;

  @tracked private _duration = LoanCreditScoreParameters.DURATION_DEFAULT;

  @tracked private _income = LoanCreditScoreParameters.INCOME_DEFAULT;

  @tracked private _liveRemainder = LoanCreditScoreParameters.LIVE_REMAINDER_DEFAULT;

  @tracked private _loanAmount = LoanCreditScoreParameters.LOAN_AMOUNT_DEFAULT;

  @tracked private _residentialStatus = LoanCreditScoreParameters.RESIDENTIAL_STATUS_DEFAULT;

  @tracked private _savings = LoanCreditScoreParameters.SAVINGS_DEFAULT;

  get bankingIncidents() {
    return this._bankingIncidents;
  }

  set bankingIncidents(value: number) {
    if (value >= 0 && value <= this.bankingIncidentsMax) {
      this._bankingIncidents = value;
    }
  }

  get bankingIncidentsMax() {
    return LoanCreditScoreParameters.BANKING_INCIDENTS_MAX;
  }

  get borrowersMax() {
    return LoanCreditScoreParameters.BORROWERS_MAX;
  }

  get borrowers() {
    return this._borrowers;
  }

  get children() {
    return this._children;
  }

  set children(value: number) {
    if (value >= 0 && value <= this.childrenMax) {
      this._children = value;
    }
  }

  get childrenMax() {
    return LoanCreditScoreParameters.CHILDREN_MAX;
  }

  get contribution() {
    return this._contribution;
  }

  set contribution(value: number) {
    if (value >= 0 && value <= this.contributionMax) {
      this._contribution = value;
    }
  }

  get contributionMax() {
    return LoanCreditScoreParameters.CONTRIBUTION_MAX;
  }

  get debtRate() {
    return this._debtRate;
  }

  set debtRate(value: number) {
    if (value >= 0 && value <= this.debtRateMax) {
      this._debtRate = value;
    }
  }

  get debtRateMax() {
    return LoanCreditScoreParameters.DEBT_RATE_MAX;
  }

  get duration() {
    return this._duration;
  }

  set duration(value: number) {
    if (value >= this.durationMin && value <= this.durationMax) {
      this._duration = value;
    }
  }

  get durationMax() {
    return LoanCreditScoreParameters.DURATION_MAX;
  }

  get durationMin() {
    return LoanCreditScoreParameters.DURATION_MIN;
  }

  get income() {
    return this._income;
  }

  set income(value: number) {
    if (value >= 0 && value <= this.incomeMax) {
      this._income = value;
    }
  }

  get incomeMax() {
    return LoanCreditScoreParameters.INCOME_MAX;
  }

  get liveRemainder() {
    return this._liveRemainder;
  }

  set liveRemainder(value: number) {
    if (value >= 0 && value <= this.liveRemainderMax) {
      this._liveRemainder = value;
    }
  }

  get liveRemainderMax() {
    return LoanCreditScoreParameters.LIVE_REMAINDER_MAX;
  }

  get loanAmount() {
    return this._loanAmount;
  }

  set loanAmount(value: number) {
    if (value >= 0 && value <= this.loanAmountMax) {
      this._loanAmount = value;
    }
  }

  get loanAmountMax() {
    return LoanCreditScoreParameters.LOAN_AMOUNT_MAX;
  }

  get numberBorrowers() {
    return this._borrowers.length;
  }

  set numberBorrowers(value: number) {
    if (value > 0 && value <= this.borrowersMax && value !== this._borrowers.length) {
      if (value > this._borrowers.length) {
        this._borrowers = [...this._borrowers, ...[...Array(value - this._borrowers.length)].map(() => new Borrower())];
      } else {
        this._borrowers = this._borrowers.slice(0, value);
      }
    }
  }

  get residentialStatus() {
    return this._residentialStatus;
  }

  set residentialStatus(value: ResidentialStatus) {
    this._residentialStatus = value;
  }

  get savings() {
    return this._savings;
  }

  set savings(value: number) {
    if (value >= 0 && value <= this.savingsMax) {
      this._savings = value;
    }
  }

  get savingsMax() {
    return LoanCreditScoreParameters.SAVINGS_MAX;
  }

  fromQueryParams(params: LoanCreditScoreParametersQueryParameters) {
    if (params.borrowersAge?.length && params.borrowersAge.length > 0) {
      this.numberBorrowers = params.borrowersAge.length;
    }

    if (params.bankingIncidents) {
      this.bankingIncidents = Number(params.bankingIncidents);
    }

    if (params.children) {
      this.children = Number(params.children);
    }

    if (params.contribution) {
      this.bankingIncidents = Number(params.bankingIncidents);
    }

    if (params.debtRate) {
      this.debtRate = Number(params.debtRate);
    }

    if (params.duration) {
      this.duration = Number(params.duration);
    }

    if (params.income) {
      this.income = Number(params.income);
    }

    if (params.liveRemainder) {
      this.liveRemainder = Number(params.liveRemainder);
    }

    if (params.loanAmount) {
      this.loanAmount = Number(params.loanAmount);
    }

    if (params.residentialStatus && Object.keys(ResidentialStatus).includes(params.residentialStatus)) {
      this.residentialStatus = params.residentialStatus as ResidentialStatus;
    }

    if (params.savings) {
      this.savings = Number(params.savings);
    }
  }

  toQueryParams() {
    return {
      borrowersAge: this.borrowers.map((b) => b.age.toString()),
      borrowersSocioProfessionalCategory: this.borrowers.map((b) => b.socioProfessionalCategory),
      borrowersSocioProfessionalCategoryYears: this.borrowers.map((b) => b.socioProfessionalCategoryYears.toString()),
      bankingIncidents: this.bankingIncidents.toString(),
      children: this.children.toString(),
      contribution: this.contribution.toString(),
      debtRate: this.debtRate.toString(),
      duration: this.duration.toString(),
      income: this.income.toString(),
      liveRemainder: this.liveRemainder.toString(),
      loanAmount: this.loanAmount.toString(),
      residentialStatus: this.residentialStatus,
      savings: this.savings.toString(),
    };
  }
}
