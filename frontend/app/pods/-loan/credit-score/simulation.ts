import { debug } from '@ember/debug';

import { ResidentialStatus } from 'fz-front-simulator/enums/residential-status';
import { SocioProfessionalCategory } from 'fz-front-simulator/enums/socio-professional-category';

import LoanCreditScoreResult from './result';

import type Borrower from './borrower';
import type LoanCreditScoreParameters from './parameters';
import type Simulation from 'fz-front-simulator/objects/simulation';

export default class LoanCreditScoreSimulation implements Simulation<LoanCreditScoreParameters, LoanCreditScoreResult> {
  private static readonly SOCIO_PROFESSIONAL_CATEGORY_GROUP_1: readonly SocioProfessionalCategory[] = [
    SocioProfessionalCategory.PUBLIC_SECTOR_EMPLOYEE,
    SocioProfessionalCategory.PUBLIC_SECTOR_MANAGER,
    SocioProfessionalCategory.RETIRED,
  ];

  private static readonly SOCIO_PROFESSIONAL_CATEGORY_GROUP_2: readonly SocioProfessionalCategory[] = [
    SocioProfessionalCategory.BUSINESS_OWNER,
    SocioProfessionalCategory.INDEPENDENT_PROFESSIONAL,
    SocioProfessionalCategory.LIBERAL_PROFESSIONAL,
    SocioProfessionalCategory.MEDICAL_PROFESSIONAL,
    SocioProfessionalCategory.PRIVATE_SECTOR_MANAGER,
  ];

  private static readonly SOCIO_PROFESSIONAL_CATEGORY_GROUP_3: readonly SocioProfessionalCategory[] = [
    SocioProfessionalCategory.ARTISAN,
    SocioProfessionalCategory.FARMER,
    SocioProfessionalCategory.MERCHANT,
    SocioProfessionalCategory.PRIVATE_SECTOR_EMPLOYEE,
  ];

  private static readonly SOCIO_PROFESSIONAL_CATEGORY_GROUP_4: readonly SocioProfessionalCategory[] = [
    SocioProfessionalCategory.OTHER,
    SocioProfessionalCategory.WORKER,
  ];

  private static readonly SOCIO_PROFESSIONAL_CATEGORY_GROUP_5: readonly SocioProfessionalCategory[] = [
    SocioProfessionalCategory.STUDENT,
    SocioProfessionalCategory.UNEMPLOYED,
  ];

  private static readonly SOCIO_PROFESSIONAL_CATEGORY_SCORES: ReadonlyMap<
    0 | 4 | 10,
    ReadonlyMap<readonly SocioProfessionalCategory[], number>
  > = new Map([
    [
      0,
      new Map([
        [this.SOCIO_PROFESSIONAL_CATEGORY_GROUP_1, 90],
        [this.SOCIO_PROFESSIONAL_CATEGORY_GROUP_2, 75],
        [this.SOCIO_PROFESSIONAL_CATEGORY_GROUP_3, 25],
        [this.SOCIO_PROFESSIONAL_CATEGORY_GROUP_4, 0],
        [this.SOCIO_PROFESSIONAL_CATEGORY_GROUP_5, 0],
      ]),
    ],
    [
      4,
      new Map([
        [this.SOCIO_PROFESSIONAL_CATEGORY_GROUP_1, 140],
        [this.SOCIO_PROFESSIONAL_CATEGORY_GROUP_2, 110],
        [this.SOCIO_PROFESSIONAL_CATEGORY_GROUP_3, 60],
        [this.SOCIO_PROFESSIONAL_CATEGORY_GROUP_4, 25],
        [this.SOCIO_PROFESSIONAL_CATEGORY_GROUP_5, 0],
      ]),
    ],
    [
      10,
      new Map([
        [this.SOCIO_PROFESSIONAL_CATEGORY_GROUP_1, 140],
        [this.SOCIO_PROFESSIONAL_CATEGORY_GROUP_2, 125],
        [this.SOCIO_PROFESSIONAL_CATEGORY_GROUP_3, 85],
        [this.SOCIO_PROFESSIONAL_CATEGORY_GROUP_4, 50],
        [this.SOCIO_PROFESSIONAL_CATEGORY_GROUP_5, 0],
      ]),
    ],
  ]);

  async simulate(parameters: LoanCreditScoreParameters) {
    debug(`Running loan credit score simulation`);
    debug(`Borrower 1 : age: ${parameters.borrowers[0].age}`);

    if (parameters.borrowers.length === 2) {
      debug(`Borrower 2 : age: ${parameters.borrowers[1].age}`);
    }

    const score =
      this.bankingIncidentsScore(parameters.bankingIncidents) +
      this.borrowersScore(parameters.borrowers) +
      this.contributionScore(parameters.contribution, parameters.loanAmount) +
      this.debtRateScore(parameters.debtRate) +
      this.durationScore(parameters.duration) +
      this.householdCompositionScore(parameters.borrowers.length, parameters.children) +
      this.incomeScore(parameters.income) +
      this.liveRemainderScore(parameters.liveRemainder, parameters.borrowers.length, parameters.children) +
      this.loanAmountScore(parameters.loanAmount) +
      this.residentialStatusScore(parameters.residentialStatus) +
      this.savingsScore(parameters.savings, parameters.income);

    return new LoanCreditScoreResult(score);
  }

  private bankingIncidentsScore(bankingIncidents: number) {
    if (bankingIncidents === 0) {
      return 125;
    } else {
      return 0;
    }
  }

  private borrowersScore(borrowers: Borrower[]) {
    return borrowers.map((b) => this.borrowerScore(b)).reduce((a, b) => a + b) / borrowers.length;
  }

  private borrowerScore(borrower: Borrower) {
    return (
      this.borrowerAgeScore(borrower.age) +
      this.borrowerSocioProfessionalCategoryScore(
        borrower.socioProfessionalCategory,
        borrower.socioProfessionalCategoryYears,
      )
    );
  }

  private borrowerAgeScore(age: number) {
    if (age < 24) {
      return 0;
    } else if (age < 34) {
      return 25;
    } else if (age < 54) {
      return 10;
    } else {
      return 5;
    }
  }

  private borrowerSocioProfessionalCategoryScore(category: SocioProfessionalCategory, years: number) {
    let yearCategory: 0 | 4 | 10 = 0;

    if (years >= 10) {
      yearCategory = 10;
    } else if (years >= 4) {
      yearCategory = 4;
    }

    const socioProfessionalCategoryGroup = [
      LoanCreditScoreSimulation.SOCIO_PROFESSIONAL_CATEGORY_GROUP_1,
      LoanCreditScoreSimulation.SOCIO_PROFESSIONAL_CATEGORY_GROUP_2,
      LoanCreditScoreSimulation.SOCIO_PROFESSIONAL_CATEGORY_GROUP_3,
      LoanCreditScoreSimulation.SOCIO_PROFESSIONAL_CATEGORY_GROUP_4,
      LoanCreditScoreSimulation.SOCIO_PROFESSIONAL_CATEGORY_GROUP_5,
    ].find((g) => g.includes(category));

    if (!socioProfessionalCategoryGroup) {
      throw new Error(`socio professional category group not found for category: ${category}`);
    }

    return (
      LoanCreditScoreSimulation.SOCIO_PROFESSIONAL_CATEGORY_SCORES.get(yearCategory)?.get(
        socioProfessionalCategoryGroup,
      ) || 0
    );
  }

  private contributionScore(contribution: number, loanAmount: number) {
    if (loanAmount === 0) {
      return 0;
    }

    const contributionPercent = (contribution / loanAmount) * 100;

    if (contributionPercent < 5) {
      return 0;
    } else if (contributionPercent < 10) {
      return 10;
    } else if (contributionPercent < 25) {
      return 50;
    } else {
      return 110;
    }
  }

  private debtRateScore(debtRate: number) {
    if (debtRate < 10) {
      return 95;
    } else if (debtRate < 20) {
      return 80;
    } else if (debtRate < 30) {
      return 40;
    } else {
      return 0;
    }
  }

  private durationScore(duration: number) {
    if (duration < 7) {
      return 35;
    } else if (duration < 14) {
      return 25;
    } else if (duration < 24) {
      return 0;
    } else {
      return 10;
    }
  }

  private incomeScore(income: number) {
    if (income < 14000) {
      return 0;
    } else if (income < 35000) {
      return 15;
    } else {
      return 45;
    }
  }

  private householdCompositionScore(numberBorrowers: number, children: number) {
    if (numberBorrowers === 1) {
      if (children === 0) {
        return 40;
      } else if (children <= 2) {
        return 25;
      } else {
        return 0;
      }
    } else {
      if (children === 0) {
        return 50;
      } else if (children <= 2) {
        return 100;
      } else if (children <= 4) {
        return 80;
      } else {
        return 50;
      }
    }
  }

  private liveRemainderScore(liveRemainder: number, numberBorrowers: number, children: number) {
    const limit1 = (numberBorrowers === 1 ? 800 : 1000) + children * 400;
    const limit2 = (numberBorrowers === 1 ? 1000 : 1200) + children * 400;
    const limit3 = (numberBorrowers === 1 ? 1700 : 2500) + children * 400;

    if (liveRemainder < limit1) {
      return 0;
    } else if (liveRemainder < limit2) {
      return 25;
    } else if (liveRemainder < limit3) {
      return 60;
    } else {
      return 100;
    }
  }

  private loanAmountScore(value: number) {
    if (value < 20000) {
      return 75;
    } else if (value < 60000) {
      return 30;
    } else if (value < 220000) {
      return 20;
    } else {
      return 0;
    }
  }

  private residentialStatusScore(value: ResidentialStatus) {
    switch (value) {
      case ResidentialStatus.FREE_LODGER:
        return 0;
      case ResidentialStatus.OWNER:
        return 50;
      case ResidentialStatus.TENANT:
        return 10;
    }
  }

  private savingsScore(savings: number, income: number) {
    if (income === 0) {
      return 0;
    } else {
      const savingMonths = (savings / income) * 12;

      if (savingMonths < 3) {
        return 0;
      } else if (savingMonths < 6) {
        return 35;
      } else if (savingMonths < 18) {
        return 75;
      } else {
        return 100;
      }
    }
  }
}
