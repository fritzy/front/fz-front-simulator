import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

import type RouterConfigService from 'fz-front-simulator/services/router-config';

export default class NotFoundController extends Controller {
  @service declare routerConfig: RouterConfigService;
}
