import { tracked } from '@glimmer/tracking';
import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

import TaxationTaxSharesParameters from './parameters';

import type IntlService from 'ember-intl/services/intl';

export default class TaxationTaxSharesController extends Controller {
  @service declare intl: IntlService;

  queryParams = [
    'alternatingCustodyChildren',
    'dependentChildren',
    'disabledAlternatingCustodyChildren',
    'disabledDependentPeople',
    'familyStatus',
    'specialStatus',
    {
      queryParamTitle: 'title',
    },
  ];

  alternatingCustodyChildren?: string;
  dependentChildren?: string;
  disabledAlternatingCustodyChildren?: string;
  disabledDependentPeople?: string;
  familyStatus?: string;
  specialStatus?: string;

  queryParamTitle?: string;

  @tracked title = '';

  parameters: TaxationTaxSharesParameters;

  constructor(args: never) {
    super(args);
    this.parameters = new TaxationTaxSharesParameters();
  }

  setup() {
    this.title = this.queryParamTitle ?? this.intl.t('pods.taxation.tax-shares.title');
    this.parameters.fromQueryParams({
      alternatingCustodyChildren: this.alternatingCustodyChildren,
      dependentChildren: this.dependentChildren,
      disabledAlternatingCustodyChildren: this.disabledAlternatingCustodyChildren,
      disabledDependentPeople: this.disabledDependentPeople,
      familyStatus: this.familyStatus,
      specialStatus: this.specialStatus,
    });
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'taxation-tax-shares-controller': TaxationTaxSharesController;
  }
}
