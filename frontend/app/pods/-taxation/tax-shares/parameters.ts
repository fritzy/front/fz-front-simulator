import { tracked } from '@glimmer/tracking';

import { FamilyStatus } from 'fz-front-simulator/enums/family-status';
import { TaxSharesSpecialStatus } from 'fz-front-simulator/enums/tax-shares-special-status';

import type Exportable from 'fz-front-simulator/objects/exportable';

interface TaxationTaxSharesQueryParameters {
  alternatingCustodyChildren: string | undefined;
  dependentChildren: string | undefined;
  disabledAlternatingCustodyChildren: string | undefined;
  disabledDependentPeople: string | undefined;
  familyStatus: string | undefined;
  specialStatus: string | undefined;
}

export default class TaxationTaxSharesParameters implements Exportable<TaxationTaxSharesQueryParameters> {
  private static readonly ALTERNATING_CUSTODY_CHILDREN_DEFAULT = 0;
  private static readonly ALTERNATING_CUSTODY_CHILDREN_MAX = 10;

  private static readonly BORROWERS_MAX = 2;

  private static readonly DEPENDENT_CHILDREN_DEFAULT = 0;
  private static readonly DEPENDENT_CHILDREN_MAX = 10;

  private static readonly DISABLED_ALTERNATING_CUSTODY_CHILDREN_DEFAULT = 0;
  private static readonly DISABLED_ALTERNATING_CUSTODY_CHILDREN_MAX = 10;

  private static readonly DISABLED_DEPENDENT_PEOPLE_DEFAULT = 0;
  private static readonly DISABLED_DEPENDENT_PEOPLE_MAX = 10;

  private static readonly FAMILY_STATUS_DEFAULT = FamilyStatus.ALONE;

  private static readonly SPECIAL_STATUS_DEFAULT = TaxSharesSpecialStatus.NONE;

  @tracked private _alternatingCustodyChildren = TaxationTaxSharesParameters.ALTERNATING_CUSTODY_CHILDREN_DEFAULT;

  @tracked private _dependentChildren = TaxationTaxSharesParameters.DEPENDENT_CHILDREN_DEFAULT;

  @tracked private _disabledAlternatingCustodyChildren =
    TaxationTaxSharesParameters.DISABLED_ALTERNATING_CUSTODY_CHILDREN_DEFAULT;

  @tracked private _disabledDependentPeople = TaxationTaxSharesParameters.DISABLED_DEPENDENT_PEOPLE_DEFAULT;

  @tracked private _familyStatus = TaxationTaxSharesParameters.FAMILY_STATUS_DEFAULT;

  @tracked private _specialStatus = TaxationTaxSharesParameters.SPECIAL_STATUS_DEFAULT;

  get alternatingCustodyChildren() {
    return this._alternatingCustodyChildren;
  }

  set alternatingCustodyChildren(value: number) {
    if (value >= 0 && value <= this.alternatingCustodyChildrenMax) {
      this._alternatingCustodyChildren = value;
    }
  }

  get alternatingCustodyChildrenMax() {
    return TaxationTaxSharesParameters.ALTERNATING_CUSTODY_CHILDREN_MAX;
  }

  get borrowersMax() {
    return TaxationTaxSharesParameters.BORROWERS_MAX;
  }

  get dependentChildren() {
    return this._dependentChildren;
  }

  set dependentChildren(value: number) {
    if (value >= 0 && value <= this.dependentChildrenMax) {
      this._dependentChildren = value;
    }
  }

  get dependentChildrenMax() {
    return TaxationTaxSharesParameters.DEPENDENT_CHILDREN_MAX;
  }

  get disabledAlternatingCustodyChildren() {
    return this._disabledAlternatingCustodyChildren;
  }

  set disabledAlternatingCustodyChildren(value: number) {
    if (value >= 0 && value <= this.disabledAlternatingCustodyChildrenMax) {
      this._disabledAlternatingCustodyChildren = value;
    }
  }

  get disabledAlternatingCustodyChildrenMax() {
    return TaxationTaxSharesParameters.DISABLED_ALTERNATING_CUSTODY_CHILDREN_MAX;
  }

  get disabledDependentPeople() {
    return this._disabledDependentPeople;
  }

  set disabledDependentPeople(value: number) {
    if (value >= 0 && value <= this.disabledDependentPeopleMax) {
      this._disabledDependentPeople = value;
    }
  }

  get disabledDependentPeopleMax() {
    return TaxationTaxSharesParameters.DISABLED_DEPENDENT_PEOPLE_MAX;
  }

  get familyStatus() {
    return this._familyStatus;
  }

  set familyStatus(value: FamilyStatus) {
    this._familyStatus = value;
  }

  get specialStatus() {
    if (this._specialStatus && this.specialStatuses.includes(this._specialStatus)) {
      return this._specialStatus;
    }

    return TaxationTaxSharesParameters.SPECIAL_STATUS_DEFAULT;
  }

  set specialStatus(value: TaxSharesSpecialStatus) {
    if (value && this.specialStatuses.includes(value)) {
      this._specialStatus = value;
    }
  }

  get specialStatuses() {
    if (this.familyStatus === FamilyStatus.UNION_UNDER_CONTRACT) {
      return [
        TaxSharesSpecialStatus.NONE,
        TaxSharesSpecialStatus.ONE_DISABLED,
        TaxSharesSpecialStatus.TWO_DISABLED,
        TaxSharesSpecialStatus.VETERAN,
      ];
    }

    return [
      TaxSharesSpecialStatus.ALONE,
      TaxSharesSpecialStatus.ALONE_CHILD_RAISED,
      TaxSharesSpecialStatus.NONE,
      TaxSharesSpecialStatus.ONE_DISABLED,
      TaxSharesSpecialStatus.VETERAN,
    ];
  }

  fromQueryParams(params: TaxationTaxSharesQueryParameters) {
    if (params.alternatingCustodyChildren) {
      this.alternatingCustodyChildren = Number(params.alternatingCustodyChildren);
    }

    if (params.dependentChildren) {
      this.dependentChildren = Number(params.dependentChildren);
    }

    if (params.disabledAlternatingCustodyChildren) {
      this.disabledAlternatingCustodyChildren = Number(params.disabledAlternatingCustodyChildren);
    }

    if (params.disabledDependentPeople) {
      this.disabledDependentPeople = Number(params.disabledDependentPeople);
    }

    if (params.familyStatus && Object.keys(FamilyStatus).includes(params.familyStatus)) {
      this.familyStatus = params.familyStatus as FamilyStatus;
    }

    if (params.specialStatus && Object.keys(TaxSharesSpecialStatus).includes(params.specialStatus)) {
      this.specialStatus = params.specialStatus as TaxSharesSpecialStatus;
    }
  }

  toQueryParams() {
    return {
      alternatingCustodyChildren: this.alternatingCustodyChildren.toString(),
      dependentChildren: this.dependentChildren.toString(),
      disabledAlternatingCustodyChildren: this.disabledAlternatingCustodyChildren.toString(),
      disabledDependentPeople: this.disabledDependentPeople.toString(),
      familyStatus: this.familyStatus,
      specialStatus: this.specialStatus,
    };
  }
}
