export default class TaxationTaxSharesResult {
  constructor(
    readonly mainPeople: number,
    readonly dependentPeople: number,
    readonly mainShares: number,
    readonly mainAdditionalShares: number,
    readonly dependentShares: number,
    readonly dependentAdditionalShares: number,
  ) {}

  get taxShares() {
    return this.mainShares + this.mainAdditionalShares + this.dependentShares + this.dependentAdditionalShares;
  }
}
