import { debug } from '@ember/debug';

import { FamilyStatus } from 'fz-front-simulator/enums/family-status';
import { TaxSharesSpecialStatus } from 'fz-front-simulator/enums/tax-shares-special-status';

import TaxationTaxSharesResult from './result';

import type TaxationTaxSharesParameters from './parameters';
import type Simulation from 'fz-front-simulator/objects/simulation';

export default class TaxationTaxSharesSimulation
  implements Simulation<TaxationTaxSharesParameters, TaxationTaxSharesResult>
{
  async simulate(parameters: TaxationTaxSharesParameters) {
    debug(`Running taxation tax shares simulation with {familyStatus: ${parameters.familyStatus}}`);

    return new TaxationTaxSharesResult(
      this.computeMainPeople(parameters.familyStatus),
      this.computeDependentPeople(
        parameters.dependentChildren,
        parameters.disabledDependentPeople,
        parameters.alternatingCustodyChildren,
        parameters.disabledAlternatingCustodyChildren,
      ),
      this.computeMainShares(parameters.familyStatus),
      this.computeMainAdditionalShares(
        parameters.familyStatus,
        parameters.specialStatus,
        parameters.dependentChildren,
        parameters.disabledDependentPeople,
        parameters.alternatingCustodyChildren,
        parameters.disabledAlternatingCustodyChildren,
      ),
      this.computeAllDependentShares(
        parameters.dependentChildren,
        parameters.disabledDependentPeople,
        parameters.alternatingCustodyChildren,
        parameters.disabledAlternatingCustodyChildren,
      ),
      this.computeAllDependentAdditionalShares(
        parameters.disabledDependentPeople,
        parameters.disabledAlternatingCustodyChildren,
      ),
    );
  }

  private computeMainPeople(familyStatus: FamilyStatus) {
    return familyStatus === FamilyStatus.UNION_UNDER_CONTRACT ? 2 : 1;
  }

  private computeDependentPeople(
    dependentChildren: number,
    disabledDependentPeople: number,
    alternatingCustodyChildren: number,
    disabledAlternatingCustodyChildren: number,
  ) {
    return (
      dependentChildren + disabledDependentPeople + alternatingCustodyChildren + disabledAlternatingCustodyChildren
    );
  }

  private computeAllDependentShares(
    dependentChildren: number,
    disabledDependentPeople: number,
    alternatingCustodyChildren: number,
    disabledAlternatingCustodyChildren: number,
  ) {
    const threshold = 2;

    return (
      this.computeChildrenShares(dependentChildren + disabledDependentPeople, 1, threshold) +
      this.computeChildrenShares(
        alternatingCustodyChildren + disabledAlternatingCustodyChildren,
        0.5,
        Math.max(threshold - dependentChildren, 0),
      )
    );
  }

  private computeAllDependentAdditionalShares(
    disabledDependentPeople: number,
    disabledAlternatingCustodyChildren: number,
  ) {
    return disabledDependentPeople / 2 + disabledAlternatingCustodyChildren / 4;
  }

  private computeChildrenShares(children: number, weight: number, threshold: number) {
    return Math.min(children, threshold) * 0.5 * weight + Math.max(children - threshold, 0) * weight;
  }

  private computeMainShares(familyStatus: FamilyStatus) {
    if (familyStatus === FamilyStatus.UNION_UNDER_CONTRACT) {
      return 2;
    }

    return 1;
  }

  private computeMainAdditionalShares(
    familyStatus: FamilyStatus,
    specialStatus: TaxSharesSpecialStatus | undefined,
    dependentChildren: number,
    disabledDependentPeople: number,
    alternatingCustodyChildren: number,
    disabledAlternatingCustodyChildren: number,
  ) {
    let mainAdditionalShares = 0;

    if (familyStatus === FamilyStatus.WIDOWER && (dependentChildren > 0 || alternatingCustodyChildren > 0)) {
      mainAdditionalShares++;
    }

    if (familyStatus === FamilyStatus.UNION_UNDER_CONTRACT && specialStatus === TaxSharesSpecialStatus.TWO_DISABLED) {
      mainAdditionalShares++;
    }

    if (
      familyStatus !== FamilyStatus.UNION_UNDER_CONTRACT &&
      specialStatus === TaxSharesSpecialStatus.ALONE &&
      (dependentChildren > 0 ||
        alternatingCustodyChildren > 0 ||
        disabledDependentPeople > 0 ||
        disabledAlternatingCustodyChildren > 0)
    ) {
      mainAdditionalShares = mainAdditionalShares + 0.5;
    }

    if (
      specialStatus === TaxSharesSpecialStatus.ALONE_CHILD_RAISED &&
      dependentChildren === 0 &&
      alternatingCustodyChildren === 0
    ) {
      mainAdditionalShares = mainAdditionalShares + 0.5;
    }

    if (specialStatus === TaxSharesSpecialStatus.ONE_DISABLED || specialStatus === TaxSharesSpecialStatus.VETERAN) {
      mainAdditionalShares = mainAdditionalShares + 0.5;
    }

    return mainAdditionalShares;
  }
}
