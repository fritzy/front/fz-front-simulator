import LegalNoticesController from 'fz-front-simulator/pods/legal-notices/controller';

export default class FrLegalNoticesController extends LegalNoticesController {}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'fr-legal-notices-controller': FrLegalNoticesController;
  }
}
