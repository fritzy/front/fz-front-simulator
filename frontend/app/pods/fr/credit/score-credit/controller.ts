import LoanCreditScoreController from 'fz-front-simulator/pods/-loan/credit-score/controller';

export default class FrCreditScoreCreditController extends LoanCreditScoreController {}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'fr-credit-score-credit-controller': FrCreditScoreCreditController;
  }
}
