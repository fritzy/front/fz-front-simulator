import TaxationTaxSharesController from 'fz-front-simulator/pods/-taxation/tax-shares/controller';

export default class FrFiscalitePartsFiscalesController extends TaxationTaxSharesController {}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'fr-fiscalite-parts-fiscales-controller': FrFiscalitePartsFiscalesController;
  }
}
