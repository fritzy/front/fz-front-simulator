import TaxationIncomeController from 'fz-front-simulator/pods/taxation/income/controller';

export default class FrFiscaliteRevenuController extends TaxationIncomeController {}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'fr-fiscalite-revenu-controller': FrFiscaliteRevenuController;
  }
}
