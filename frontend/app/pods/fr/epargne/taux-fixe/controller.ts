import SavingController from 'fz-front-simulator/pods/savings/saving/controller';

export default class FrEpargneTauxFixeController extends SavingController {}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'fr-epargne-taux-fixe-controller': FrEpargneTauxFixeController;
  }
}
