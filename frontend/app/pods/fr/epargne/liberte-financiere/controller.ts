import SavingFinancialFreedomController from 'fz-front-simulator/pods/-saving/financial-freedom/controller';

export default class FrEpargneLiberteFinanciereController extends SavingFinancialFreedomController {}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'fr-epargne-liberte-financiere-controller': FrEpargneLiberteFinanciereController;
  }
}
