import SavingSixAccountsStrategyController from 'fz-front-simulator/pods/saving/six-accounts-strategy/controller';

export default class FrEpargneStrategieSixComptesController extends SavingSixAccountsStrategyController {}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'fr-epargne-stategie-six-comptes-controller': FrEpargneStrategieSixComptesController;
  }
}
