import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

import type IntlConfigService from 'fz-front-simulator/services/intl-config';

export default class Fr extends Route {
  @service declare intlConfig: IntlConfigService;

  async beforeModel() {
    await this.intlConfig.setLocale('fr');
  }
}
