import EmberRouter from '@ember/routing/router';

import config from 'fz-front-simulator/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function () {
  this.route('not-found', { path: '/*path' });
  this.route('savings', function () {
    this.route('saving');
    this.route('crypto');
  });
  this.route('privacy');

  this.route('loans', function () {
    this.route('loan');
  });

  this.route('taxes', function () {
    this.route('crypto');
  });

  this.route('portfolio', function () {
    this.route('crypto');
  });

  this.route('fr', function () {
    this.route('epargne', function () {
      this.route('taux-fixe');
      this.route('strategie-six-comptes');
      this.route('liberte-financiere');
    });
    this.route('fiscalite', function () {
      this.route('revenu');
      this.route('parts-fiscales');
    });
    this.route('mentions-legales');

    this.route('credit', function () {
      this.route('score-credit');
    });
    this.route('integration');
  });

  this.route('en', function () {
    this.route('saving', function () {
      this.route('fixed-rate');
      this.route('six-accounts-strategy');
      this.route('financial-freedom');
    });
    this.route('legal-notices');

    this.route('taxation', function () {
      this.route('income');
      this.route('tax-shares');
    });

    this.route('loan', function () {
      this.route('credit-score');
    });
    this.route('integration');
  });
});
