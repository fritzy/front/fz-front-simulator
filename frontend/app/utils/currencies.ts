import Currency from 'fz-front-simulator/objects/currency';

export default class Currencies {
  public static readonly EURO = new Currency('euro', 'EUR', '€', 2, true);
  public static readonly US_DOLLAR = new Currency('us-dollar', 'USD', '$', 2, true);

  public static readonly BITCOIN = new Currency('bitcoin', 'BTC', '₿', 8, false);
  public static readonly ETHEREUM = new Currency('ethereum', 'ETH', '⧫', 8, false);
  public static readonly SATOSHI = new Currency('satoshi', 'SATS', 'SATS', 0, false);

  private static readonly CURRENCIES: readonly Currency[] = [
    Currencies.EURO,
    Currencies.US_DOLLAR,
    Currencies.BITCOIN,
    Currencies.ETHEREUM,
    Currencies.SATOSHI,
  ];

  private constructor() {
    // Utility class
  }

  public static getCurrency(ticker: string) {
    return this.CURRENCIES.find((c) => c.ticker === ticker);
  }

  public static getFractionDigits(currency: Currency | undefined) {
    return currency ? currency.fractionDigits : 2;
  }
}
