import Money from 'bigint-money';

export default class Rates {
  private constructor() {
    // Utility class
  }

  static periodicRate(apy: number, periods: number) {
    const pow = Math.pow(1 + apy / 100, 1 / periods);

    return Number(new Money(pow.toFixed(20), '').subtract(1).multiply(100).toFixed(14));
  }
}
