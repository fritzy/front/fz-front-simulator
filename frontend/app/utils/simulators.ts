import { SimulatorCategory } from 'fz-front-simulator/enums/simulator-category';
import Simulator from 'fz-front-simulator/objects/app/simulator';

export default class Simulators {
  public static readonly LOAN_CREDIT_SCORE = new Simulator(SimulatorCategory.LOAN, 'credit-score', 'gavel', true);
  public static readonly LOAN_LOAN = new Simulator(SimulatorCategory.LOAN, 'loan', 'balance-scale-left', true);

  public static readonly LOAN_SIMULATORS = [Simulators.LOAN_CREDIT_SCORE, Simulators.LOAN_LOAN];

  public static readonly PORTFOLIO_DIGITAL_ASSET = new Simulator(
    SimulatorCategory.PORTFOLIO,
    'digital-asset',
    'btc',
    false,
  );

  public static readonly PORTFOLIO_SIMULATORS = [Simulators.PORTFOLIO_DIGITAL_ASSET];

  public static readonly SAVING_DIGITAL_ASSET = new Simulator(SimulatorCategory.SAVING, 'digital-asset', 'btc', true);

  public static readonly SAVING_FINANCIAL_FREEDOM = new Simulator(
    SimulatorCategory.SAVING,
    'financial-freedom',
    'umbrella-beach',
    true,
  );

  public static readonly SAVING_FIXED_RATE = new Simulator(SimulatorCategory.SAVING, 'fixed-rate', 'percentage', true);

  public static readonly SAVING_SIX_ACCOUNTS_STRATEGY = new Simulator(
    SimulatorCategory.SAVING,
    'six-accounts-strategy',
    'chart-pie',
    true,
  );

  public static readonly SAVING_SIMULATORS = [
    Simulators.SAVING_DIGITAL_ASSET,
    Simulators.SAVING_FINANCIAL_FREEDOM,
    Simulators.SAVING_FIXED_RATE,
    Simulators.SAVING_SIX_ACCOUNTS_STRATEGY,
  ];

  public static readonly TAXATION_DIGITAL_ASSET = new Simulator(
    SimulatorCategory.TAXATION,
    'digital-asset',
    'btc',
    false,
  );

  public static readonly TAXATION_TAX_SHARES = new Simulator(SimulatorCategory.TAXATION, 'tax-shares', 'users', true);

  public static readonly TAXATION_INCOME = new Simulator(SimulatorCategory.TAXATION, 'income', 'landmark', true);

  public static readonly TAXATAION_SIMULATORS = [
    Simulators.TAXATION_DIGITAL_ASSET,
    Simulators.TAXATION_INCOME,
    Simulators.TAXATION_TAX_SHARES,
  ];
}
