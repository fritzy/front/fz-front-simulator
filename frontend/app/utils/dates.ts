export default class Dates {
  private constructor() {
    // Utility class
  }

  static MS_PER_DAY = 1000 * 60 * 60 * 24;

  static isLeapYear(year: number): boolean {
    return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
  }

  static getDaysInMonth(year: number, month: number): number {
    return [31, Dates.isLeapYear(year) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
  }

  static getTodayAsString(): string {
    const local = new Date();

    local.setMinutes(local.getMinutes() - local.getTimezoneOffset());

    return local.toJSON().slice(0, 10);
  }

  static addMonths(date: Date, value: number): Date {
    const newDate = new Date(date);
    const day = date.getDate();

    newDate.setDate(1);
    newDate.setMonth(newDate.getMonth() + value);
    newDate.setDate(Math.min(day, Dates.getDaysInMonth(newDate.getFullYear(), newDate.getMonth())));

    return newDate;
  }

  static isLastDayOfMonth(date: Date): boolean {
    return Dates.getDaysInMonth(date.getFullYear(), date.getMonth()) === date.getDate();
  }

  static isSameDay(date1: Date, date2: Date): boolean {
    const utc1 = Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate());
    const utc2 = Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate());

    return utc1 === utc2;
  }

  static isSameDayOfWeek(date1: Date, date2: Date): boolean {
    const utc1 = Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate());
    const utc2 = Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate());
    const diffDays = Math.floor((utc2 - utc1) / Dates.MS_PER_DAY);

    return diffDays % 7 === 0;
  }

  static isSameDayOfMonth(date1: Date, date2: Date): boolean {
    const day1 = date1.getDate();
    const day2 = date2.getDate();

    let sameDay = day1 === day2;

    if (!sameDay && day1 > 28 && day2 > 27 && day1 > day2) {
      sameDay = Dates.isLastDayOfMonth(date2);
    }

    return sameDay;
  }
}
