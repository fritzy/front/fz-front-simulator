import Money from 'bigint-money';

export default class Moneys {
  public static readonly ZERO = new Money(0, '');
  public static readonly ONE = new Money(1, '');

  public static readonly ZERO_EUR = new Money(0, 'EUR');
  public static readonly ONE_EUR = new Money(1, 'EUR');

  private constructor() {
    // Utility class
  }

  static max(a: Money, b: Money) {
    if (a.isGreaterThan(b)) {
      return a;
    } else {
      return b;
    }
  }

  static min(a: Money, b: Money) {
    if (a.isLesserThan(b)) {
      return a;
    } else {
      return b;
    }
  }

  static round(amount: Money, precision: number) {
    return new Money(amount.toFixed(precision), amount.currency);
  }
}
