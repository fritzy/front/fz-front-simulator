import IndexedDbAdapter from 'ember-indexeddb/adapters/indexed-db';

export default class ApplicationAdapter extends IndexedDbAdapter {}

declare module 'ember-data/types/registries/adapter' {
  export default interface AdapterRegistry {
    'application-adapter': ApplicationAdapter;
  }
}
