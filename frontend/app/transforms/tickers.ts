import Transform from '@ember-data/serializer/transform';

import CryptoCurrencyTicker from 'fz-front-simulator/objects/crypto-currency-ticker';

import MoneyTransform from './money';

export default class TickersTransform extends Transform {
  public static readonly MONEY_TRANSFORM = MoneyTransform.create();

  deserialize(tickers: { cryptoCurrency: string; price: [string, string] }[]): CryptoCurrencyTicker[] {
    return tickers.map(
      (t) => new CryptoCurrencyTicker(t.cryptoCurrency, TickersTransform.MONEY_TRANSFORM.deserialize(t.price)),
    );
  }

  serialize(tickers: CryptoCurrencyTicker[]): { cryptoCurrency: string; price: [string, string] }[] {
    return tickers.map((t) => {
      return {
        cryptoCurrency: t.cryptoCurrency,
        price: TickersTransform.MONEY_TRANSFORM.serialize(t.price),
      };
    });
  }
}

declare module 'ember-data/types/registries/transform' {
  export default interface TransformRegistry {
    tickers: TickersTransform;
  }
}
