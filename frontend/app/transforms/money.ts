import Transform from '@ember-data/serializer/transform';

import Money from 'bigint-money';

export default class MoneyTransform extends Transform {
  deserialize(jsonMoney: [string, string]): Money {
    return new Money(jsonMoney[0], jsonMoney[1]);
  }

  serialize(money: Money): [string, string] {
    return money.toJSON();
  }
}

declare module 'ember-data/types/registries/transform' {
  export default interface TransformRegistry {
    money: MoneyTransform;
  }
}
