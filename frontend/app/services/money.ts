import Service, { inject as service } from '@ember/service';

import Currencies from 'fz-front-simulator/utils/currencies';

import type CryptoService from './crypto-service';
import type Money from 'bigint-money';
import type IntlService from 'ember-intl/services/intl';
import type Currency from 'fz-front-simulator/objects/currency';

export default class MoneyService extends Service {
  @service declare cryptoService: CryptoService;
  @service declare intl: IntlService;

  format(value: Money, fractionDigits?: number) {
    const currency = Currencies.getCurrency(value.currency);

    const fractionDigitsApplied = fractionDigits != undefined ? fractionDigits : Currencies.getFractionDigits(currency);

    if (currency?.iso4217) {
      return this.formatIso4217Currency(value, currency, fractionDigitsApplied);
    } else if (currency) {
      return this.formatCurrency(value, currency.symbol, fractionDigitsApplied);
    } else if (value.currency) {
      const symbol = this.cryptoService.getCrypto(value.currency)?.symbol || value.currency;

      return this.formatCurrency(value, symbol, fractionDigitsApplied);
    } else {
      return this.intl.formatNumber(parseFloat(value.toFixed(2)), {});
    }
  }

  private formatIso4217Currency(value: Money, currency: Currency, fractionDigits: number) {
    return this.intl.formatNumber(parseFloat(value.toFixed(fractionDigits)), {
      currency: currency.ticker,
      minimumFractionDigits: fractionDigits,
      style: 'currency',
    });
  }

  private formatCurrency(value: Money, symbol: string, fractionDigits: number) {
    return this.formatIso4217Currency(value, Currencies.EURO, fractionDigits).replace('€', symbol);
  }
}
