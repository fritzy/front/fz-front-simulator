import Service, { inject as service } from '@ember/service';

import Simulators from 'fz-front-simulator/utils/simulators';

import type IntlService from 'ember-intl/services/intl';
import type Simulator from 'fz-front-simulator/objects/app/simulator';

type routes = 'index' | 'integration' | 'legalNotices';

export default class RouterConfigService extends Service {
  @service declare intl: IntlService;

  private static readonly APP_ROUTES: ReadonlyMap<routes, ReadonlyMap<string, string>> = new Map([
    [
      'index',
      new Map([
        ['en', 'en.index'],
        ['fr', 'fr.index'],
      ]),
    ],
    [
      'integration',
      new Map([
        ['en', 'en.integration'],
        ['fr', 'fr.integration'],
      ]),
    ],
    [
      'legalNotices',
      new Map([
        ['en', 'en.legal-notices'],
        ['fr', 'fr.mentions-legales'],
      ]),
    ],
  ]);

  private static readonly SIMULATORS_ROUTES: ReadonlyMap<Simulator, ReadonlyMap<string, string>> = new Map([
    [
      Simulators.LOAN_CREDIT_SCORE,
      new Map([
        ['en', 'en.loan.credit-score'],
        ['fr', 'fr.credit.score-credit'],
      ]),
    ],
    [
      Simulators.LOAN_LOAN,
      new Map([
        ['en', 'loans.loan'],
        ['fr', 'loans.loan'],
      ]),
    ],
    [
      Simulators.PORTFOLIO_DIGITAL_ASSET,
      new Map([
        ['en', 'portfolio.crypto'],
        ['fr', 'portfolio.crypto'],
      ]),
    ],
    [
      Simulators.SAVING_DIGITAL_ASSET,
      new Map([
        ['en', 'savings.crypto'],
        ['fr', 'savings.crypto'],
      ]),
    ],
    [
      Simulators.SAVING_FINANCIAL_FREEDOM,
      new Map([
        ['en', 'en.saving.financial-freedom'],
        ['fr', 'fr.epargne.liberte-financiere'],
      ]),
    ],
    [
      Simulators.SAVING_FIXED_RATE,
      new Map([
        ['en', 'en.saving.fixed-rate'],
        ['fr', 'fr.epargne.taux-fixe'],
      ]),
    ],
    [
      Simulators.SAVING_SIX_ACCOUNTS_STRATEGY,
      new Map([
        ['en', 'en.saving.six-accounts-strategy'],
        ['fr', 'fr.epargne.strategie-six-comptes'],
      ]),
    ],
    [
      Simulators.TAXATION_DIGITAL_ASSET,
      new Map([
        ['en', 'taxes.crypto'],
        ['fr', 'taxes.crypto'],
      ]),
    ],
    [
      Simulators.TAXATION_INCOME,
      new Map([
        ['en', 'en.taxation.income'],
        ['fr', 'fr.fiscalite.revenu'],
      ]),
    ],
    [
      Simulators.TAXATION_TAX_SHARES,
      new Map([
        ['en', 'en.taxation.tax-shares'],
        ['fr', 'fr.fiscalite.parts-fiscales'],
      ]),
    ],
  ]);

  get indexRoute() {
    return this.getRoute('index');
  }

  get integrationRoute() {
    return this.getRoute('integration');
  }

  get legalNoticesRoute() {
    return this.getRoute('legalNotices');
  }

  private getRoute(route: routes) {
    return RouterConfigService.APP_ROUTES.get(route)?.get(this.intl.primaryLocale);
  }

  getSimulatorRoute(simulator: Simulator) {
    return RouterConfigService.SIMULATORS_ROUTES.get(simulator)?.get(this.intl.primaryLocale);
  }
}
