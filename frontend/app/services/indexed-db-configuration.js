import IndexedDbConfigurationService from 'ember-indexeddb/services/indexed-db-configuration';

export default class ExtendedIndexedDbConfigurationService extends IndexedDbConfigurationService {
  currentVersion = 2;

  version1 = {
    stores: {
      'crypto-tax-event-funding': '&id',
    },
  };

  version2 = {
    stores: {
      'crypto-holding': '&id',
    },
  };
}
