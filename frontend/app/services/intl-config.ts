import Service, { inject as service } from '@ember/service';

import fetch from 'fetch';

import type ApplicationService from './application';
import type IntlService from 'ember-intl/services/intl';
import type Simulator from 'fz-front-simulator/objects/app/simulator';

export default class IntlConfigService extends Service {
  private static readonly TRANSLATIONS_PATHS = new Map<string, string>([
    ['en', 'translations/en.json'],
    ['fr', 'translations/fr.json'],
  ]);

  private static readonly LOCALE_KEY = 'locale';

  @service declare application: ApplicationService;
  @service declare intl: IntlService;

  async setLocale(locale: 'en' | 'fr') {
    if (!this.intl.exists('application.title', locale)) {
      const translationPath = IntlConfigService.TRANSLATIONS_PATHS.get(locale);

      if (!translationPath) {
        throw new Error(`Unsupported locale ${locale}`);
      }

      const translations = await fetch(`/${translationPath}`);
      const translationsAsJson = await translations.json();

      this.intl.addTranslations(locale, translationsAsJson);
    }

    this.intl.setLocale(locale);

    if (this.application.localStorageAvailable) {
      localStorage.setItem(IntlConfigService.LOCALE_KEY, locale);
    }
  }

  private get defautLocale() {
    if (this.application.localStorageAvailable) {
      const storedLocale = localStorage.getItem(IntlConfigService.LOCALE_KEY);

      if (storedLocale) {
        return storedLocale;
      }
    }

    const supportedLocales = [...IntlConfigService.TRANSLATIONS_PATHS.keys()];

    const supportedLanguage = navigator.languages.map((l) => l.split('-')[0]).find((l) => supportedLocales.includes(l));

    return supportedLanguage ?? 'en';
  }

  get userLocale(): 'en' | 'fr' {
    const primaryLocale = this.intl.primaryLocale !== 'en-us' ? this.intl.primaryLocale : undefined;

    return (primaryLocale ?? this.defautLocale) as 'en' | 'fr';
  }

  sortByLabelKey(simulators: Simulator[]) {
    return simulators.sort((s1, s2) => this.intl.t(s1.labelKey).localeCompare(this.intl.t(s2.labelKey)));
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your services.
declare module '@ember/service' {
  interface Registry {
    'intl-config-service': IntlConfigService;
  }
}
