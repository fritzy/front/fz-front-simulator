import { tracked } from '@glimmer/tracking';
import Service, { inject as service } from '@ember/service';

import BsTheme from 'fz-front-simulator/objects/bs/theme';
import Currencies from 'fz-front-simulator/utils/currencies';

import type IntlService from 'ember-intl/services/intl';

export default class ApplicationService extends Service {
  @service declare intl: IntlService;

  @tracked colorScheme: 'light' | 'dark' = 'light';

  @tracked currency = Currencies.EURO;

  darkTheme = BsTheme.EMPTY_THEME;
  embedded = false;
  embeddedBackgroundColor = '#f4f6f9';

  localStorageAvailable = false;

  indexedDbAvailable = false;
  lightTheme = BsTheme.EMPTY_THEME;

  constructor() {
    super();
    this.testLocalStorageAvailable();
  }

  get bsTheme() {
    return this.darkMode ? this.darkTheme : this.lightTheme;
  }

  get darkMode() {
    return this.colorScheme === 'dark';
  }

  private testLocalStorageAvailable() {
    const test = 'test';

    try {
      localStorage.setItem(test, test);
      localStorage.removeItem(test);
      this.localStorageAvailable = true;
    } catch (e) {
      this.localStorageAvailable = false;
    }
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your services.
declare module '@ember/service' {
  interface Registry {
    'application-service': ApplicationService;
  }
}
