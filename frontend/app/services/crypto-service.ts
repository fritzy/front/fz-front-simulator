import Service from '@ember/service';

import Money from 'bigint-money';
import { dropTask, restartableTask, task } from 'ember-concurrency';
import { perform } from 'ember-concurrency-ts';
import fetch from 'fetch';
import CoinGeckoCoin from 'fz-front-simulator/objects/cgc/coin-gecko-coin';
import HistoricalPrice from 'fz-front-simulator/objects/historical-price';
import Moneys from 'fz-front-simulator/utils/moneys';

import type Currency from 'fz-front-simulator/objects/currency';

export default class CryptoService extends Service {
  private static readonly COINGECKO_URL = 'https://digital-assets.fritzy.finance';

  private static readonly HEADERS = { headers: new Headers({ accept: 'application/json' }) };

  mapCryptos = new Map<string, CoinGeckoCoin>();
  mapCurrentPrices = new Map<string, Money>();
  mapHistoricalPrices = new Map<CoinGeckoCoin, Map<Currency, HistoricalPrice[]>>();
  topCryptos: CoinGeckoCoin[] = [];

  @dropTask
  async fetchCryptosTask() {
    if (this.mapCryptos.size === 0) {
      const response = await fetch(`${CryptoService.COINGECKO_URL}/coins/list`, CryptoService.HEADERS);

      const json = await response.json();

      const map = new Map<string, CoinGeckoCoin>();

      json
        .map(
          (r: { id: string; symbol: string; name: string }) => new CoinGeckoCoin(r.id, r.symbol.toUpperCase(), r.name),
        )
        .forEach((c: CoinGeckoCoin) => map.set(c.id, c));

      this.mapCryptos = map;
    }
  }

  @dropTask
  async fetchTopCryptosByMarketCapTask() {
    if (this.topCryptos.length === 0) {
      const response = await fetch(
        `${CryptoService.COINGECKO_URL}/coins/markets?vs_currency=usd`,
        CryptoService.HEADERS,
      );
      const json = await response.json();

      this.topCryptos = json.map((r: { id: string }) => this.mapCryptos.get(r.id));
    }
  }

  @task
  private async fetchCryptosPrice(id: string, date: string): Promise<Money> {
    const response = await fetch(
      `${CryptoService.COINGECKO_URL}/coins/${id}/history?date=${date}&localization=false`,
      CryptoService.HEADERS,
    );

    const json = await response.json();

    return new Money(json.market_data.current_price['eur'].toString(), 'EUR');
  }

  @restartableTask
  private async fetchHistoricalPricesTask(crypto: CoinGeckoCoin, currency: Currency): Promise<void> {
    const response = await fetch(
      `${CryptoService.COINGECKO_URL}/coins/${crypto.id}/market_chart?vs_currency=${currency.ticker}&days=3650`,
      CryptoService.HEADERS,
    );

    const json = await response.json();

    const historicalPrices = json['prices']
      .filter((p: [number, number]) => p[0] && p[1])
      .map((p: [number, number]) => new HistoricalPrice(new Date(p[0]), new Money(p[1].toFixed(20), currency.ticker)));

    const cryptoPrices = this.mapHistoricalPrices.get(crypto) ?? new Map();

    cryptoPrices.set(currency, historicalPrices);
    this.mapHistoricalPrices.set(crypto, cryptoPrices);
  }

  @dropTask
  async loadCurrentPricesTask(coins: CoinGeckoCoin[]): Promise<void> {
    const ids = coins.map((c) => c.id);
    const response = await fetch(
      `${CryptoService.COINGECKO_URL}/simple/price?ids=${ids.join(',')}&vs_currencies=eur`,
      CryptoService.HEADERS,
    );

    const json = await response.json();
    const map = new Map<string, Money>();

    ids.forEach((id) => {
      map.set(id, json[id].eur ? new Money(json[id].eur.toString(), 'EUR') : Moneys.ZERO_EUR);
    });

    this.mapCurrentPrices = map;
  }

  getCrypto(id: string): CoinGeckoCoin | undefined {
    return this.mapCryptos?.get(id);
  }

  getCryptosByCode(filter: string): CoinGeckoCoin[] {
    let cryptos;

    if (filter !== '' || !this.topCryptos) {
      cryptos =
        [...this.mapCryptos.values()]
          .filter((c) => c.name.toLowerCase().includes(filter.toLowerCase()) || c.symbol.includes(filter.toUpperCase()))
          .slice(0, 100) || [];
    } else {
      cryptos = this.topCryptos;
    }

    const firstCryptos = cryptos.filter(
      (c) => c.symbol === filter.toUpperCase() || c.name.toLowerCase() === filter.toLowerCase(),
    );
    const othersCryptos = cryptos.filter((c) => !firstCryptos.includes(c));

    return [...firstCryptos, ...othersCryptos];
  }

  async getHistoricalPrice(crypto: CoinGeckoCoin, currency: Currency): Promise<HistoricalPrice[] | undefined> {
    if (!this.mapHistoricalPrices.get(crypto)?.get(currency)) {
      await perform(this.fetchHistoricalPricesTask, crypto, currency);
    }

    return this.mapHistoricalPrices.get(crypto)?.get(currency);
  }

  async getPrice(crypto: CoinGeckoCoin, date: Date): Promise<Money> {
    const dateTimeFormat = new Intl.DateTimeFormat('en', {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
    });
    const [{ value: month }, , { value: day }, , { value: year }] = dateTimeFormat.formatToParts(date);

    return perform(this.fetchCryptosPrice, crypto.id, `${day}-${month}-${year}`);
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your services.
declare module '@ember/service' {
  interface Registry {
    'crypto-service': CryptoService;
  }
}
