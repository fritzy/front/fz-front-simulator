import { inject as service } from '@ember/service';

import Modifier from 'ember-modifier';

import type ApplicationService from 'fz-front-simulator/services/application';

export default class InIframeModifier extends Modifier {
  @service declare application: ApplicationService;

  didInstall() {
    if (this.application.embedded) {
      this.element.setAttribute('data-iframe-height', '');
    }
  }
}
