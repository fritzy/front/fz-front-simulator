import 'vanilla-colorful';

import { registerDestructor } from '@ember/destroyable';
import { inject as service } from '@ember/service';

import Modifier from 'ember-modifier';

import type ApplicationService from 'fz-front-simulator/services/application';

interface ColorPickerModifierArgs {
  positional: unknown[];
  named: {
    onChange?: (color: string) => void;
  };
}

export default class ColorPickerModifier extends Modifier<ColorPickerModifierArgs> {
  @service declare application: ApplicationService;

  private colorChangedEvent = 'color-changed';

  private onColorChanged = (event: CustomEvent) => {
    const color = event.detail.value as string;

    if (this.args.named.onChange !== undefined) {
      this.args.named.onChange(color);
    }
  };

  didInstall() {
    if (this.args.named.onChange !== undefined) {
      this.element.addEventListener(this.colorChangedEvent, this.onColorChanged);

      registerDestructor(this, () => {
        this.element.removeEventListener(this.colorChangedEvent, this.onColorChanged);
      });
    }
  }
}
