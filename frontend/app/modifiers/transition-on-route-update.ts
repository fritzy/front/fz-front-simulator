import { inject as service } from '@ember/service';

import Modifier from 'ember-modifier';

import type RouterService from '@ember/routing/router-service';
import type Exportable from 'fz-front-simulator/objects/exportable';

interface LanguageChangeListenerArgs {
  positional: [string];
  named: {
    parameters?: Exportable<never>;
  };
}

export default class TransitionOnRouteUpdateModifier extends Modifier<LanguageChangeListenerArgs> {
  @service declare router: RouterService;

  get route(): string {
    return this.args.positional[0];
  }

  didUpdateArguments() {
    this.router.transitionTo(this.route, { queryParams: this.args.named.parameters?.toQueryParams() });
  }
}
