import 'imask/esm/masked/number';

import { registerDestructor } from '@ember/destroyable';

import Modifier from 'ember-modifier';
import IMask from 'imask/esm/imask';

import type { MaskedPattern } from 'imask';

interface ImaskPercentageArgs {
  positional: [number | string];
  named: {
    max?: number;
    min?: number;
    onChange?: (number: number) => void;
  };
}

export default class ImaskPercentage extends Modifier<ImaskPercentageArgs> {
  mask?: IMask.InputMask<MaskedPattern>;

  constructor(owner: unknown, args: ImaskPercentageArgs) {
    super(owner, args);

    if (this.args.named.onChange && typeof this.args.named.onChange !== 'function') {
      throw new Error('onChange must be a function');
    }
  }

  get min(): number {
    return this.args.named.min || 0;
  }

  get max(): number {
    return this.args.named.max || 100;
  }

  get positionalValue(): string | undefined {
    if (this.args.positional[0] != undefined) {
      return this.args.positional[0].toString();
    }

    return undefined;
  }

  didInstall(): void {
    this.mask = this.setupMask();

    if (this.positionalValue) {
      this.mask.unmaskedValue = this.positionalValue;
    }

    if (this.args.named.onChange) {
      this.mask.on('complete', this.onChangeHandler);
    }
  }

  didUpdateArguments(): void {
    if (this.mask && this.positionalValue) {
      this.mask.unmaskedValue = this.positionalValue;
    }
  }

  onChangeHandler: () => void = () => {
    if (
      this.mask &&
      this.args.named.onChange &&
      this.mask.unmaskedValue &&
      this.positionalValue &&
      parseFloat(this.mask.unmaskedValue) !== parseFloat(this.positionalValue)
    ) {
      this.args.named.onChange(parseFloat(this.mask.unmaskedValue));
    }
  };

  private setupMask() {
    this.mask = IMask(this.element as HTMLElement, {
      mask: 'num %',
      lazy: false,
      blocks: {
        num: {
          mask: Number,
          min: this.min,
          max: this.max,
          thousandsSeparator: ' ',
          padFractionalZeros: true,
          scale: 2,
        },
      },
    }) as unknown as IMask.InputMask<MaskedPattern>;

    registerDestructor(this, () => this.mask?.destroy());

    return this.mask;
  }
}
