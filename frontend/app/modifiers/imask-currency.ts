import 'imask/esm/masked/number';

import { registerDestructor } from '@ember/destroyable';

import Money from 'bigint-money';
import Modifier from 'ember-modifier';
import Currency from 'fz-front-simulator/objects/currency';
import IMask from 'imask/esm/imask';

import type { MaskedPattern } from 'imask';

interface ImaskCurrencyArgs {
  positional: [number | Money | string | undefined, Currency | string | undefined];
  named: {
    fractionDigits?: number;
    max?: number;
    min?: number;
    onChange?: (number: number) => void;
  };
}

export default class ImaskCurrency extends Modifier<ImaskCurrencyArgs> {
  currentSymbol?: string;
  mask?: IMask.InputMask<MaskedPattern>;

  constructor(owner: unknown, args: ImaskCurrencyArgs) {
    super(owner, args);

    if (this.args.named.onChange && typeof this.args.named.onChange !== 'function') {
      throw new Error('onChange must be a function');
    }
  }

  get fractionDigits(): number {
    return this.args.named.fractionDigits !== undefined ? this.args.named.fractionDigits : 2;
  }

  get maskPattern(): string {
    return `num${this.currentSymbol ? ' ' + this.currentSymbol : ''}`;
  }

  get positionalValue(): string | undefined {
    if (this.args.positional[0] != undefined) {
      if (this.args.positional[0] instanceof Money) {
        return this.args.positional[0].toFixed(this.fractionDigits);
      } else {
        return this.args.positional[0].toString();
      }
    }

    return undefined;
  }

  get symbol() {
    return this.args.positional[1] instanceof Currency ? this.args.positional[1].symbol : this.args.positional[1];
  }

  didInstall(): void {
    this.currentSymbol = this.symbol;
    this.setupMask();
  }

  didUpdateArguments(): void {
    if (this.currentSymbol !== this.symbol) {
      this.currentSymbol = this.symbol;
      this.mask?.destroy();
      this.setupMask();
    } else if (this.mask && this.positionalValue) {
      this.mask.unmaskedValue = this.positionalValue;
    }
  }

  onChangeHandler: () => void = () => {
    if (
      this.mask &&
      this.args.named.onChange &&
      this.mask.unmaskedValue &&
      this.positionalValue &&
      parseFloat(this.mask.unmaskedValue) !== parseFloat(this.positionalValue)
    ) {
      this.args.named.onChange(parseFloat(this.mask.unmaskedValue));
    }
  };

  private setupMask() {
    this.mask = IMask(this.element as HTMLElement, {
      mask: this.maskPattern,
      lazy: false,
      blocks: {
        num: {
          mask: Number,
          ...(this.args.named.min && { min: this.args.named.min }),
          ...(this.args.named.max && { max: this.args.named.max }),
          thousandsSeparator: ' ',
          scale: this.fractionDigits,
        },
      },
    }) as unknown as IMask.InputMask<MaskedPattern>;

    if (this.args.named.onChange) {
      this.mask.on('complete', this.onChangeHandler);
    }

    registerDestructor(this, () => this.mask?.destroy());

    if (this.positionalValue) {
      this.mask.unmaskedValue = this.positionalValue;
    }
  }
}
