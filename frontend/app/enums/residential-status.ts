export enum ResidentialStatus {
  FREE_LODGER = 'FREE_LODGER',
  OWNER = 'OWNER',
  TENANT = 'TENANT',
}
