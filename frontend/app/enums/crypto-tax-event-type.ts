export enum CryptoTaxEventType {
  BUY = 'BUY',
  SELL = 'SELL',
}
