export enum FinancialFreedomPhase {
  FIRE = 'FIRE',
  SAVING = 'SAVING',
}
