export enum SimulatorCategory {
  LOAN = 'LOAN',
  SAVING = 'SAVING',
  TAXATION = 'TAXATION',
  PORTFOLIO = 'PORTFOLIO',
}
