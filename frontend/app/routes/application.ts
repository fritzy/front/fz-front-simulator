import { debug } from '@ember/debug';
import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

import { options } from '@amcharts/amcharts4/core';
import { perform } from 'ember-concurrency-ts';

import type IndexedDbService from 'ember-indexeddb/services/indexed-db';
import type ApplicationService from 'fz-front-simulator/services/application';
import type IntlConfigService from 'fz-front-simulator/services/intl-config';

export default class ApplicationRoute extends Route {
  @service declare application: ApplicationService;
  @service declare indexedDb: IndexedDbService;
  @service declare intlConfig: IntlConfigService;

  async beforeModel() {
    await this.intlConfig.setLocale(this.intlConfig.userLocale);

    try {
      await perform(this.indexedDb.setupTask);
      this.application.indexedDbAvailable = true;
    } catch (e) {
      this.application.indexedDbAvailable = false;
      debug(`IndexedDb is not availlable: ${e}`);
    }

    this.setupAmCharts4();
  }

  model(params: { backgroundColor?: string; embedded: string }) {
    this.application.embedded = params.embedded === 'true';

    if (params.backgroundColor) {
      this.application.embeddedBackgroundColor = params.backgroundColor;
    }
  }

  private setupAmCharts4() {
    options.onlyShowOnViewport = true;
    // Daisy-chaining multiple charts
    //options.queue = true; https://github.com/amcharts/amcharts4/issues/3730
  }
}
