import Helper from '@ember/component/helper';
import { inject as service } from '@ember/service';

import type Simulator from 'fz-front-simulator/objects/app/simulator';
import type RouterConfigService from 'fz-front-simulator/services/router-config';

export default class SimulatorRouteHelper extends Helper {
  @service declare routerConfig: RouterConfigService;

  compute([value]: [Simulator]): string | undefined {
    return this.routerConfig.getSimulatorRoute(value);
  }
}
