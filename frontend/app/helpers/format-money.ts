import Helper from '@ember/component/helper';
import { inject as service } from '@ember/service';

import type { Money } from 'bigint-money';
import type MoneyService from 'fz-front-simulator/services/money';

export default class FormatMoneyHelper extends Helper {
  @service declare money: MoneyService;

  compute([value]: [Money], { fractionDigits }: { fractionDigits: number }): string {
    return this.money.format(value, fractionDigits);
  }
}
