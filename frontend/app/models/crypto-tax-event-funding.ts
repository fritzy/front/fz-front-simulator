import { attr } from '@ember-data/model';

import CryptoTaxEvent from './crypto-tax-event';

import type Money from 'bigint-money';

export default class CryptoTaxEventFunding extends CryptoTaxEvent {
  @attr('money') quantity!: Money;
  @attr('money') price!: Money;
}

// DO NOT DELETE: this is how TypeScript knows how to look up your models.
declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    'crypto-tax-event-buy': CryptoTaxEventFunding;
  }
}
