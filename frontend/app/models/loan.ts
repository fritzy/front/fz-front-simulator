import Model, { attr } from '@ember-data/model';

export default class Loan extends Model {
  @attr() amount!: number;
  @attr() duration!: number;
  @attr() insuranceYield!: number;
  @attr() yield!: number;
}

// DO NOT DELETE: this is how TypeScript knows how to look up your models.
declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    loan: Loan;
  }
}
