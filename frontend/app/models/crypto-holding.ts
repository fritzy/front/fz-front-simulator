import { inject as service } from '@ember/service';
import Model, { attr } from '@ember-data/model';

import type Money from 'bigint-money';
import type CoinGeckoCoin from 'fz-front-simulator/objects/cgc/coin-gecko-coin';
import type CryptoService from 'fz-front-simulator/services/crypto-service';

export default class CryptoHolding extends Model {
  @service cryptoService!: CryptoService;

  @attr() description!: string;
  @attr('money') quantity!: Money;

  get cryptoCurrency(): CoinGeckoCoin | undefined {
    if (this.quantity?.currency) {
      return this.cryptoService.getCrypto(this.quantity.currency);
    } else {
      return undefined;
    }
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your models.
declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    'crypto-holding': CryptoHolding;
  }
}
