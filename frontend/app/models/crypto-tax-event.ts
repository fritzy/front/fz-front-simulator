import Model, { attr } from '@ember-data/model';

import type { CryptoTaxEventType } from 'fz-front-simulator/enums/crypto-tax-event-type';
import type CryptoCurrencyTicker from 'fz-front-simulator/objects/crypto-currency-ticker';

export default class CryptoTaxEvent extends Model {
  @attr() date!: string;
  @attr('tickers') tickers!: CryptoCurrencyTicker[];
  @attr() type!: CryptoTaxEventType;

  get dateAsDate() {
    return new Date(this.date);
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your models.
declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    'crypto-tax-event': CryptoTaxEvent;
  }
}
