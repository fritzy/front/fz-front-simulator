import Component from '@glimmer/component';

export default class AppLoaderRemover extends Component {
  constructor(owner: unknown, args: Record<string, never>) {
    super(owner, args);

    this.removeAppLoader();
  }

  private removeAppLoader() {
    const loader = document.getElementById('app-loader');

    if (loader) {
      loader.remove();
      window.onerror = null;
    }
  }
}
