import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class AppManual extends Component {
  @tracked helpCardCollapsed = true;

  @action
  toggleHelpCard() {
    this.helpCardCollapsed = !this.helpCardCollapsed;
  }
}
