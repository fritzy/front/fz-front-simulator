import { inject as service } from '@ember/service';

import Modifier from 'ember-modifier';

import type ApplicationService from 'fz-front-simulator/services/application';

export default class AppLayoutEmbeddedModifier extends Modifier {
  @service declare application: ApplicationService;

  didInstall() {
    if (this.application.embedded) {
      if (this.element instanceof HTMLElement) {
        this.element.style.backgroundColor = this.application.embeddedBackgroundColor;
      }
    }
  }
}
