import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

import AppLayoutEmbeddedModifier from './embedded';

import type ApplicationService from 'fz-front-simulator/services/application';

export default class AppLayoutContentWrapper extends Component {
  @service declare application: ApplicationService;

  embedded = AppLayoutEmbeddedModifier;
}
