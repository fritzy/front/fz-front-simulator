import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

import type ApplicationService from 'fz-front-simulator/services/application';

export default class AppAlertCurrency extends Component {
  @service declare application: ApplicationService;
}
