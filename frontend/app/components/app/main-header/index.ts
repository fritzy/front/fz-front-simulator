import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import type ApplicationService from 'fz-front-simulator/services/application';

interface MainHeaderArgs {
  onDarkModeChanged: (isDark: boolean) => void;
}

export default class MainHeader extends Component<MainHeaderArgs> {
  @service declare application: ApplicationService;

  @tracked lightIcon = 'lightbulb-on';

  @action
  darken() {
    this.application.colorScheme = 'dark';
    this.lightIcon = 'lightbulb';

    if (this.args.onDarkModeChanged) {
      this.args.onDarkModeChanged(true);
    }
  }

  @action
  lighten() {
    this.application.colorScheme = 'light';
    this.lightIcon = 'lightbulb-on';

    if (this.args.onDarkModeChanged) {
      this.args.onDarkModeChanged(false);
    }
  }
}
