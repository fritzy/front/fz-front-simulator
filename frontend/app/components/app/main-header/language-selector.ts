import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import type { BsNavbar } from 'ember-bootstrap';
import type IntlService from 'ember-intl/services/intl';
import type ApplicationService from 'fz-front-simulator/services/application';
import type IntlConfigService from 'fz-front-simulator/services/intl-config';

interface AppMainHeaderLanguageSelectorArgs {
  nav: BsNavbar;
}

export default class AppMainHeaderLanguageSelector extends Component<AppMainHeaderLanguageSelectorArgs> {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;
  @service declare intlConfig: IntlConfigService;

  @tracked opened = false;

  @action
  onHide() {
    this.opened = false;
  }

  @action
  onShow() {
    this.opened = true;
  }

  @action
  setLanguage(locale: 'en' | 'fr') {
    this.intlConfig.setLocale(locale);
  }
}
