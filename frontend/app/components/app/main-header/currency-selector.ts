import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import Currencies from 'fz-front-simulator/utils/currencies';

import type { BsNavbar } from 'ember-bootstrap';
import type ApplicationService from 'fz-front-simulator/services/application';

interface AppMainHeaderCurrencySelectorArgs {
  nav: BsNavbar;
}

export default class AppMainHeaderCurrencySelector extends Component<AppMainHeaderCurrencySelectorArgs> {
  @service declare application: ApplicationService;

  @tracked opened = false;

  @action
  onHide() {
    this.opened = false;
  }

  @action
  onShow() {
    this.opened = true;
  }

  @action
  setCurrency(ticker: string) {
    const currency = Currencies.getCurrency(ticker);

    if (currency) {
      this.application.currency = currency;
    }
  }
}
