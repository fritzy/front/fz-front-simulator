import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

import Simulators from 'fz-front-simulator/utils/simulators';

import type IntlConfigService from 'fz-front-simulator/services/intl-config';
import type RouterConfigService from 'fz-front-simulator/services/router-config';

export default class AppMainSidebar extends Component {
  @service declare intlConfig: IntlConfigService;
  @service declare routerConfig: RouterConfigService;

  get loanSimulators() {
    return this.intlConfig.sortByLabelKey(Simulators.LOAN_SIMULATORS);
  }

  get portfolioSimulators() {
    return this.intlConfig.sortByLabelKey(Simulators.PORTFOLIO_SIMULATORS);
  }

  get savingSimulators() {
    return this.intlConfig.sortByLabelKey(Simulators.SAVING_SIMULATORS);
  }

  get taxationSimulators() {
    return this.intlConfig.sortByLabelKey(Simulators.TAXATAION_SIMULATORS);
  }
}
