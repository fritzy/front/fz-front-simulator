import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

import type RouterConfigService from 'fz-front-simulator/services/router-config';

export default class AppFooter extends Component {
  @service declare routerConfig: RouterConfigService;
}
