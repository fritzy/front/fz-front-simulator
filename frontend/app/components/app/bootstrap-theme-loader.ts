import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

import BsTheme from 'fz-front-simulator/objects/bs/theme';

import type ApplicationService from 'fz-front-simulator/services/application';

export default class AppBootstrapThemeLoader extends Component {
  @service declare application: ApplicationService;

  constructor(owner: unknown, args: Record<string, never>) {
    super(owner, args);

    this.loadBoostrapTheme();
  }

  private loadBoostrapTheme() {
    const body = getComputedStyle(document.body);

    this.application.darkTheme = new BsTheme.Builder()
      .primary(this.propertyValue(body, '--primary-alt'))
      .secondary(this.propertyValue(body, '--secondary-alt'))
      .success(this.propertyValue(body, '--success-alt'))
      .info(this.propertyValue(body, '--info-alt'))
      .warning(this.propertyValue(body, '--warning-alt'))
      .danger(this.propertyValue(body, '--danger-alt'))
      .light(this.propertyValue(body, '--light-alt'))
      .dark(this.propertyValue(body, '--dark-alt'))
      .build();

    this.application.lightTheme = new BsTheme.Builder()
      .primary(this.propertyValue(body, '--primary'))
      .secondary(this.propertyValue(body, '--secondary'))
      .success(this.propertyValue(body, '--success'))
      .info(this.propertyValue(body, '--info'))
      .warning(this.propertyValue(body, '--warning'))
      .danger(this.propertyValue(body, '--danger'))
      .light(this.propertyValue(body, '--light'))
      .dark(this.propertyValue(body, '--dark'))
      .build();
  }

  propertyValue(body: CSSStyleDeclaration, color: string) {
    return body.getPropertyValue(color).trim();
  }
}
