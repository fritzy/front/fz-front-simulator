import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import Money from 'bigint-money';
import { CryptoTaxEventType } from 'fz-front-simulator/enums/crypto-tax-event-type';
import CryptoTaxEventFunding from 'fz-front-simulator/models/crypto-tax-event-funding';
import CryptoCurrencyTicker from 'fz-front-simulator/objects/crypto-currency-ticker';

import type StoreService from '@ember-data/store';
import type { BsModal } from 'ember-bootstrap';
import type { BufferedChangeset } from 'ember-changeset/types';
import type IntlService from 'ember-intl/services/intl';
import type CryptoTaxEvent from 'fz-front-simulator/models/crypto-tax-event';
import type CoinGeckoCoin from 'fz-front-simulator/objects/cgc/coin-gecko-coin';
import type CryptoBalanceHistory from 'fz-front-simulator/pods/taxes/crypto/crypto-balance-history';
import type CryptoService from 'fz-front-simulator/services/crypto-service';

interface EventsAddArgs {
  adding: boolean;
  balanceHistories: CryptoBalanceHistory[];
  changeset?: BufferedChangeset;
  visible: boolean;
  onAdd: (event: CryptoTaxEvent) => Promise<void>;
  onHidden: () => void;
}

export default class EventsAdd extends Component<EventsAddArgs> {
  @service cryptoService!: CryptoService;
  @service intl!: IntlService;
  @service store!: StoreService;

  @tracked event?: CryptoTaxEvent;

  get tickers(): CryptoCurrencyTicker[] {
    if (this.event) {
      const currentDate = new Date(this.event.date);
      const previousHistories = this.args.balanceHistories.filter((h) => new Date(h.event.date) < currentDate);
      const mostPreviousHistory = previousHistories[previousHistories.length - 1];

      const tickers =
        mostPreviousHistory?.balances.map((b) => new CryptoCurrencyTicker(b.balance.currency, b.averagePrice)) || [];

      tickers.forEach(async (t) => {
        const crypto = this.cryptoService.getCrypto(t.cryptoCurrency);

        if (crypto) {
          t.price = await this.cryptoService.getPrice(crypto, currentDate);
        }
      });

      return tickers;
    }

    return [];
  }

  get cannotAdd(): boolean {
    return this.event === undefined;
  }

  get cryptoCurrency(): CoinGeckoCoin | undefined {
    if (this.event instanceof CryptoTaxEventFunding && this.event.quantity.currency) {
      return this.cryptoService.getCrypto(this.event.quantity.currency);
    } else {
      return undefined;
    }
  }

  get type(): CryptoTaxEventType | undefined {
    if (this.event) {
      return this.event.type;
    }

    return undefined;
  }

  set type(type: CryptoTaxEventType | undefined) {
    if (type === CryptoTaxEventType.BUY || type === CryptoTaxEventType.SELL) {
      this.event = this.store.createRecord('crypto-tax-event-funding');
    }

    if (this.event instanceof CryptoTaxEventFunding) {
      this.event.quantity = new Money(0, '');
      this.event.price = new Money(0, 'EUR');
    }

    if (this.event) {
      if (!this.event.date) {
        this.event.date = new Date().toISOString().substring(0, 10);
      }

      if (type) {
        this.event.type = type;
      }
    }
  }

  get types(): string[] {
    return Object.keys(CryptoTaxEventType);
  }

  @action
  dummyAction(): void {
    // Dummy action
  }

  @action
  onShow(): void {
    this.event = undefined;
  }

  @action
  onHidden(): void {
    this.args.onHidden();
  }

  @action
  async addEvent(modal: BsModal): Promise<void> {
    if (this.event) {
      if (this.event instanceof CryptoTaxEventFunding) {
        const currency = this.event.quantity.currency;

        if (this.tickers) {
          this.event.tickers = this.tickers.filter((a) => a.cryptoCurrency !== currency);
        }
      }

      this.event.tickers = this.event.tickers || [];

      await this.args.onAdd(this.event);
    }

    modal.close();
  }

  @action
  setAsset(value: CoinGeckoCoin): void {
    if (this.event instanceof CryptoTaxEventFunding) {
      this.event.quantity = new Money(this.event.quantity ? this.event.quantity.toFixed(2) : 0, value.id);
    }
  }

  @action
  setPrice(value: number): void {
    if (this.event instanceof CryptoTaxEventFunding) {
      this.event.price = new Money(value.toString(), 'EUR');
    }
  }

  @action
  setQuantity(value: number): void {
    if (this.event instanceof CryptoTaxEventFunding) {
      this.event.quantity = new Money(value.toString(), this.event.quantity.currency);
    }
  }
}
