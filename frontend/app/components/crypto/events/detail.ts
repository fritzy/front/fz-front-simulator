import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import type StoreService from '@ember-data/store';
import type CryptoTaxEvent from 'fz-front-simulator/models/crypto-tax-event';

interface EventsDetailArgs {
  event: CryptoTaxEvent;
  deleting: boolean;
  onDelete: () => void;
}

export default class EventsDetail extends Component<EventsDetailArgs> {
  @service store!: StoreService;

  @tracked deleteConfirmationModalVisible = false;

  @action
  toggleConfirmationModalVisibility(): void {
    this.deleteConfirmationModalVisible = !this.deleteConfirmationModalVisible;
  }

  @action
  delete(): void {
    this.args.onDelete();
  }
}
