import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import { task } from 'ember-concurrency';
import { perform } from 'ember-concurrency-ts';
import { useTask } from 'ember-resources';

import type { BsForm } from 'ember-bootstrap';
import type { TaskIsh } from 'ember-resources/-private/resources/ember-concurrency-task';
import type CoinGeckoCoin from 'fz-front-simulator/objects/cgc/coin-gecko-coin';
import type ApplicationService from 'fz-front-simulator/services/application';
import type CryptoService from 'fz-front-simulator/services/crypto-service';

interface CryptoFormCurrencySelectorArgs {
  assetId?: string;
  form: BsForm;
  onChange: (asset: CoinGeckoCoin) => void;
}

export default class CryptoFormCurrencySelector extends Component<CryptoFormCurrencySelectorArgs> {
  @service declare application: ApplicationService;
  @service declare cryptoService: CryptoService;

  @tracked asset?: CoinGeckoCoin;
  @tracked cryptoCurrencyFilter = '';
  @tracked retry = 0;
  task = useTask(this, this.fetchCryptosTask as unknown as TaskIsh, () => [this.retry]);

  @action
  fetchCryptos() {
    this.retry++;
  }

  get cryptoCurrencies(): CoinGeckoCoin[] {
    return this.cryptoService.getCryptosByCode('');
  }

  @action
  setAsset(value: CoinGeckoCoin) {
    this.asset = value;
    this.args.onChange(value);
  }

  @action
  searchCryptoCurrencies(value: string) {
    return this.cryptoService.getCryptosByCode(value);
  }

  @task
  async fetchCryptosTask() {
    await perform(this.cryptoService.fetchCryptosTask);

    if (this.args.assetId) {
      const asset = this.cryptoService.getCrypto(this.args.assetId);

      if (asset) {
        this.setAsset(asset);
      }
    }

    await perform(this.cryptoService.fetchTopCryptosByMarketCapTask);
  }
}
