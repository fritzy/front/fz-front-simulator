import Component from '@glimmer/component';

import { dropTask } from 'ember-concurrency';

import type CryptoTaxEvent from 'fz-front-simulator/models/crypto-tax-event';

interface CryptoEventsArgs {
  events: CryptoTaxEvent[];
  // onAdd: () => void;
}

export default class CryptoEvents extends Component<CryptoEventsArgs> {
  @dropTask
  async delete(event: CryptoTaxEvent): Promise<void> {
    await event.destroyRecord();
  }
}
