import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import Money from 'bigint-money';
import { Changeset } from 'ember-changeset';
import { CryptoTaxEventType } from 'fz-front-simulator/enums/crypto-tax-event-type';

import type StoreService from '@ember-data/store';
import type { BsModal } from 'ember-bootstrap';
import type { BufferedChangeset } from 'ember-changeset/types';
import type IntlService from 'ember-intl/services/intl';
import type CryptoHolding from 'fz-front-simulator/models/crypto-holding';
import type CoinGeckoCoin from 'fz-front-simulator/objects/cgc/coin-gecko-coin';
import type CryptoBalanceHistory from 'fz-front-simulator/pods/taxes/crypto/crypto-balance-history';
import type CryptoService from 'fz-front-simulator/services/crypto-service';

interface EventsAddArgs {
  adding: boolean;
  balanceHistories: CryptoBalanceHistory[];
  changeset?: BufferedChangeset;
  visible: boolean;
  onAdd: (holding: CryptoHolding) => Promise<void>;
  onHidden: () => void;
}

export default class EventsAdd extends Component<EventsAddArgs> {
  @service declare cryptoService: CryptoService;
  @service declare intl: IntlService;
  @service declare store: StoreService;

  @tracked declare holding?: CryptoHolding;
  @tracked declare changeset?: BufferedChangeset;

  get cannotAdd(): boolean {
    const quantity = this.changeset?.get('quantity');

    return this.holding === undefined || this.changeset === undefined || quantity === undefined;
  }

  get cryptoCurrency(): CoinGeckoCoin | undefined {
    if (this.holding?.quantity.currency) {
      return this.cryptoService.getCrypto(this.holding.quantity.currency);
    } else {
      return undefined;
    }
  }

  get types(): string[] {
    return Object.keys(CryptoTaxEventType);
  }

  @action
  dummyAction(): void {
    // Dummy action
  }

  @action
  onShow(): void {
    this.holding = this.store.createRecord('crypto-holding');
    this.changeset = Changeset(this.holding);
  }

  @action
  onHidden(): void {
    this.args.onHidden();

    if (this.holding?.isNew) {
      this.holding.rollbackAttributes();
    }
  }

  @action
  async add(changeset: BufferedChangeset, modal: BsModal): Promise<void> {
    changeset.execute();

    if (this.holding) {
      await this.args.onAdd(this.holding);
    }

    modal.close();
  }

  @action
  setAsset(value: CoinGeckoCoin): void {
    if (this.holding) {
      this.holding.quantity = new Money(this.holding.quantity ? this.holding.quantity.toFixed(8) : 0, value.id);
    }
  }

  @action
  setQuantity(value: number): void {
    if (this.changeset) {
      this.changeset.set('quantity', new Money(value.toString(), this.changeset.get('quantity').currency));
    }
  }
}
