import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

import { dropTask } from 'ember-concurrency';

import type CryptoTaxEvent from 'fz-front-simulator/models/crypto-tax-event';
import type CryptoAsset from 'fz-front-simulator/pods/portfolio/crypto/crypto-asset';

interface AggregatedArgs {
  event: CryptoTaxEvent;
  deleting: boolean;
  onDelete: () => void;
}

export default class AggregatedDetail extends Component<AggregatedArgs> {
  @tracked collapsed = false;

  @dropTask
  async delete(asset: CryptoAsset): Promise<void> {
    await asset.holding.destroyRecord();
  }

  @action
  toggleCollapsed(): void {
    this.collapsed = !this.collapsed;
  }
}
