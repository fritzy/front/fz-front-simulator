import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

import type IntlService from 'ember-intl/services/intl';

interface FormSelectArgs {
  optionKey: string;
  options: string[];
  value: unknown;
}

export default class FormEnumSelect extends Component<FormSelectArgs> {
  @service declare intl: IntlService;

  getOptionLabel(option: string) {
    return this.intl.t(this.args.optionKey + option);
  }

  get sortedOptions() {
    return this.args.options.sort((o1, o2) => this.getOptionLabel(o1).localeCompare(this.getOptionLabel(o2)));
  }
}
