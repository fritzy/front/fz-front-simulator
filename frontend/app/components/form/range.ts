import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';
import { inject as service } from '@ember/service';

import type { BsForm } from 'ember-bootstrap';
import type IntlService from 'ember-intl/services/intl';

interface FormRangeArgs {
  form: BsForm;
  hint?: string;
  label: string;
  onChange: (value: number) => void;
  value: number;
  valueLabelKey: string;
  valueLabelKeyParam: string;
  valueMax: number;
  valueMin: number;
}

export default class FormRange extends Component<FormRangeArgs> {
  @service declare intl: IntlService;

  elementId = guidFor(this);

  @tracked _currentValue = this.args.value;

  get currentValue() {
    return Math.min(this._currentValue, this.args.valueMax);
  }

  get valueLabel() {
    const params: Record<string, number> = {};

    params[this.args.valueLabelKeyParam] = this.currentValue;

    return this.intl.t(this.args.valueLabelKey, params);
  }

  @action
  handleRangeInputOnInput(inputEvent: Event): void {
    this._currentValue = this.toTargetNumber(inputEvent);
  }

  @action
  handleRangeInputOnChange(changeEvent: Event): void {
    this.args.onChange(this.toTargetNumber(changeEvent));
  }

  private toTargetNumber(event: Event) {
    return Number((event.target as HTMLInputElement).value);
  }
}
