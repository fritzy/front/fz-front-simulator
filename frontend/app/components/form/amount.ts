import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';
import { inject as service } from '@ember/service';

import type ApplicationService from 'fz-front-simulator/services/application';

interface FormAmountArgs {
  hint?: string;
  onChange: (value: number) => void;
  value: number;
  valueMax: number;
}

export default class FormAmount extends Component<FormAmountArgs> {
  @service declare application: ApplicationService;

  elementId = guidFor(this);

  @tracked currentValue = this.args.value;

  @action
  setValue(value: number | string): void {
    this.currentValue = Number(value);

    this.args.onChange(this.currentValue);
  }

  @action
  handleRangeInputOnInput(inputEvent: Event): void {
    this.currentValue = this.toTargetNumber(inputEvent);
  }

  @action
  handleRangeInputOnChange(changeEvent: Event): void {
    this.setValue(this.toTargetNumber(changeEvent));
  }

  private toTargetNumber(event: Event) {
    return Number((event.target as HTMLInputElement).value);
  }
}
