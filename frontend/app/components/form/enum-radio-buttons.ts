import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { inject as service } from '@ember/service';

import type IntlService from 'ember-intl/services/intl';

interface FormEnumRadioButtonsArgs {
  id: string;
  optionKey: string;
  options: string[];
  value: string;
}

export default class FormEnumRadioButtons extends Component<FormEnumRadioButtonsArgs> {
  @service declare intl: IntlService;

  get id() {
    return this.args.id ?? guidFor(this);
  }

  getOptionLabel(option: string) {
    return this.intl.t(this.args.optionKey + option);
  }

  get sortedOptions() {
    return this.args.options.sort((o1, o2) => this.getOptionLabel(o1).localeCompare(this.getOptionLabel(o2)));
  }
}
