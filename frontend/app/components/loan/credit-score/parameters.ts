import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import { ResidentialStatus } from 'fz-front-simulator/enums/residential-status';
import { SocioProfessionalCategory } from 'fz-front-simulator/enums/socio-professional-category';

import type IntlService from 'ember-intl/services/intl';
import type Borrower from 'fz-front-simulator/pods/-loan/credit-score/borrower';
import type LoanCreditScoreParameters from 'fz-front-simulator/pods/-loan/credit-score/parameters';
import type ApplicationService from 'fz-front-simulator/services/application';

interface SavingCryptoParametersArgs {
  onChange: () => void;
  parameters: LoanCreditScoreParameters;
}

export default class LoanCreditScoreParametersComponent extends Component<SavingCryptoParametersArgs> {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;

  get residentialStatuses() {
    return Object.keys(ResidentialStatus);
  }

  get socioProfessionalCategories() {
    return Object.keys(SocioProfessionalCategory);
  }

  @action
  setBankingIncidents(value: number): void {
    this.args.parameters.bankingIncidents = value;
    this.args.onChange();
  }

  @action
  setBorrowerAge(borrower: Borrower, age: number): void {
    borrower.age = age;
    this.args.onChange();
  }

  @action
  setBorrowerSocioProfessionalCategory(borrower: Borrower, socioProfessionalCategory: SocioProfessionalCategory) {
    borrower.socioProfessionalCategory = socioProfessionalCategory;
    this.args.onChange();
  }

  @action
  setBorrowerSocioProfessionalCategoryYears(borrower: Borrower, years: number) {
    borrower.socioProfessionalCategoryYears = years;
    this.args.onChange();
  }

  @action
  setChldren(value: number): void {
    this.args.parameters.children = value;
    this.args.onChange();
  }

  @action
  setContribution(value: number): void {
    this.args.parameters.contribution = value;
    this.args.onChange();
  }

  @action
  setDebtRate(value: number): void {
    this.args.parameters.debtRate = value;
    this.args.onChange();
  }

  @action
  setDuration(value: number): void {
    this.args.parameters.duration = value;
    this.args.onChange();
  }

  @action
  setIncome(value: number): void {
    this.args.parameters.income = value;
    this.args.onChange();
  }

  @action
  setLiveRemainder(value: number): void {
    this.args.parameters.liveRemainder = value;
    this.args.onChange();
  }

  @action
  setLoanAmount(value: number): void {
    this.args.parameters.loanAmount = value;
    this.args.onChange();
  }

  @action
  setNumberBorrowers(value: number): void {
    this.args.parameters.numberBorrowers = value;
    this.args.onChange();
  }

  @action
  setResidentialStatus(value: ResidentialStatus) {
    this.args.parameters.residentialStatus = value;
    this.args.onChange();
  }

  @action
  setSavings(value: number) {
    this.args.parameters.savings = value;
    this.args.onChange();
  }
}
