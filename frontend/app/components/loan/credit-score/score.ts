import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { inject as service } from '@ember/service';

import GaugeData from 'fz-front-simulator/components/ac4/gauge/gauge-data';
import GaugeRange from 'fz-front-simulator/components/ac4/gauge/gauge-range';

import type IntlService from 'ember-intl/services/intl';
import type SimulationRunner from 'fz-front-simulator/objects/simulation-runner';
import type LoanCreditScoreParameters from 'fz-front-simulator/pods/-loan/credit-score/parameters';
import type LoanCreditScoreResult from 'fz-front-simulator/pods/-loan/credit-score/result';
import type ApplicationService from 'fz-front-simulator/services/application';

interface LoanCreditScoreScoreArgs {
  simulationRunner: SimulationRunner<LoanCreditScoreParameters, LoanCreditScoreResult>;
}

export default class LoanCreditScoreScore extends Component<LoanCreditScoreScoreArgs> {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;

  elementId = guidFor(this);

  get data() {
    return new GaugeData(this.args.simulationRunner.result?.score ?? 0, [
      new GaugeRange(
        this.intl.t('components.loan.credit-score.score.grade.unlikely'),
        this.application.bsTheme.danger,
        0,
        400,
      ),
      new GaugeRange(
        this.intl.t('components.loan.credit-score.score.grade.to-defend'),
        this.application.bsTheme.warning,
        400,
        600,
      ),
      new GaugeRange(this.intl.t('components.loan.credit-score.score.grade.with-conditions'), '#f3eb0c', 600, 750),
      new GaugeRange(
        this.intl.t('components.loan.credit-score.score.grade.likely'),
        this.application.bsTheme.success,
        750,
        1000,
      ),
    ]);
  }
}
