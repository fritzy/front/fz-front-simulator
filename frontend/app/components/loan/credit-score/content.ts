import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import SimulationRunner from 'fz-front-simulator/objects/simulation-runner';
import LoanCreditScoreSimulation from 'fz-front-simulator/pods/-loan/credit-score/simulation';
import Simulators from 'fz-front-simulator/utils/simulators';

import type IntlService from 'ember-intl/services/intl';
import type LoanCreditScoreParameters from 'fz-front-simulator/pods/-loan/credit-score/parameters';
import type ApplicationService from 'fz-front-simulator/services/application';
import type RouterConfigService from 'fz-front-simulator/services/router-config';

interface LoanCreditScoreArgs {
  parameters: LoanCreditScoreParameters;
  title: string;
}

export default class LoanCreditScoreContent extends Component<LoanCreditScoreArgs> {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;
  @service declare routerConfig: RouterConfigService;

  simulation = new LoanCreditScoreSimulation();
  simulationRunner = new SimulationRunner(this.simulation, this.args.parameters);

  get route() {
    return this.routerConfig.getSimulatorRoute(Simulators.LOAN_CREDIT_SCORE);
  }

  @action
  onParametersChange(): void {
    this.simulationRunner.simulation = this.simulation;
  }
}
