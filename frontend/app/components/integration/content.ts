import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import { generateOptions } from '@fritzy/simulator-embedded';
import { restartableTask, timeout } from 'ember-concurrency';
import { perform } from 'ember-concurrency-ts';
import Simulators from 'fz-front-simulator/utils/simulators';

import type RouterService from '@ember/routing/router-service';
import type IntlService from 'ember-intl/services/intl';
import type Simulator from 'fz-front-simulator/objects/app/simulator';
import type ApplicationService from 'fz-front-simulator/services/application';
import type IntlConfigService from 'fz-front-simulator/services/intl-config';
import type RouterConfigService from 'fz-front-simulator/services/router-config';

export default class IntegrationContent extends Component {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;
  @service declare intlConfig: IntlConfigService;
  @service declare router: RouterService;
  @service declare routerConfig: RouterConfigService;

  @tracked
  backgroundColor: string;

  @tracked
  simulator = Simulators.SAVING_FIXED_RATE;

  @tracked
  title = false;

  @tracked
  titleValue = '';

  constructor(owner: unknown, args: Record<string, unknown>) {
    super(owner, args);

    this.backgroundColor = this.application.embeddedBackgroundColor;
  }

  get hrefWithoutPathname() {
    return window.location.href.substring(0, window.location.href.indexOf(window.location.pathname));
  }

  get loanSimulators() {
    return this.intlConfig.sortByLabelKey(Simulators.LOAN_SIMULATORS).filter((s) => s.embeddable === true);
  }

  get savingSimulators() {
    return this.intlConfig.sortByLabelKey(Simulators.SAVING_SIMULATORS).filter((s) => s.embeddable === true);
  }

  get taxationSimulators() {
    return this.intlConfig.sortByLabelKey(Simulators.TAXATAION_SIMULATORS).filter((s) => s.embeddable === true);
  }

  get script() {
    let script = '<script \n' + '  id="fritzy-simulator"\n' + `  data-simulator="${this.simulator.id}"\n`;

    script = script + `  data-simulator-background-color="${this.backgroundColor}"\n`;

    if (this.title) {
      script = script + `  data-simulator-title="${this.titleValue}"\n`;
    }

    script += `  src="${this.hrefWithoutPathname}/iframe-integration.js"\n></script>`;

    return script;
  }

  get src() {
    const options = generateOptions(this.simulator.id, this.backgroundColor, this.title ? this.titleValue : undefined);

    return `${this.hrefWithoutPathname}${options.simulator.path}?${options.simulator.queryParams}`;
  }

  get simulators() {
    return [
      { groupName: this.intl.t('components.index.content.loan'), options: this.loanSimulators },
      { groupName: this.intl.t('components.index.content.saving'), options: this.savingSimulators },
      { groupName: this.intl.t('components.index.content.taxation'), options: this.taxationSimulators },
    ];
  }

  @action
  setBackgroundColor(value: string) {
    this.backgroundColor = value;
    this.reloadIframe();
  }

  @action
  setSimulator(value: Simulator) {
    this.simulator = value;
    this.reloadIframe();
  }

  @action
  setTitle(inputEvent: Event) {
    this.title = (inputEvent.target as HTMLInputElement).checked;
    this.reloadIframe();
  }

  @action
  setTitleValue(inputEvent: Event) {
    this.titleValue = (inputEvent.target as HTMLInputElement).value;
    this.reloadIframe();
  }

  @action
  reloadIframe() {
    perform(this.reloadIframeTask);
  }

  @restartableTask
  async reloadIframeTask() {
    await timeout(1000);

    const iframes = document.getElementsByTagName('iframe');

    if (iframes) {
      iframes[0].src = this.src;
    }
  }
}
