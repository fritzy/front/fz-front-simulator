import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import SimulationRunner from 'fz-front-simulator/objects/simulation-runner';
import SixAccountsStrategySimulation from 'fz-front-simulator/pods/saving/six-accounts-strategy/simulation';

import type IntlService from 'ember-intl/services/intl';
import type SixAccountsStrategySavingParameters from 'fz-front-simulator/pods/saving/six-accounts-strategy/parameters';
import type ApplicationService from 'fz-front-simulator/services/application';
import type RouterConfigService from 'fz-front-simulator/services/router-config';

interface SavingSixAccountsStrategyParametersArgs {
  onLanguageChange: () => void;
  parameters: SixAccountsStrategySavingParameters;
  title: string;
}

export default class SavingSixAccountsStrategyContent extends Component<SavingSixAccountsStrategyParametersArgs> {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;
  @service declare routerConfig: RouterConfigService;

  simulation = new SixAccountsStrategySimulation();
  simulationRunner = new SimulationRunner(this.simulation, this.args.parameters);

  @action
  onParametersChange(): void {
    this.simulationRunner.simulation = this.simulation;
  }
}
