import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import type SixAccountsStrategySavingParameters from 'fz-front-simulator/pods/saving/six-accounts-strategy/parameters';
import type ApplicationService from 'fz-front-simulator/services/application';

interface SavingSixAccountsStrategyParametersArgs {
  onChange: () => void;
  parameters: SixAccountsStrategySavingParameters;
}

export default class SavingSixAccountsStrategyParametersComponent extends Component<SavingSixAccountsStrategyParametersArgs> {
  @service declare application: ApplicationService;

  @action
  setIncome(value: number | string): void {
    this.args.parameters.income = Number(value);
    this.args.onChange();
  }

  @action
  handleIncomeRangeInputOnInput(inputEvent: Event): void {
    this.args.parameters.income = Number((inputEvent.target as HTMLInputElement).value);
  }

  @action
  handleIncomeRangeInputOnChange(changeEvent: Event): void {
    this.setIncome((changeEvent.target as HTMLInputElement).value);
  }
}
