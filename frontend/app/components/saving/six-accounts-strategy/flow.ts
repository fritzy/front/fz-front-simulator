import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { inject as service } from '@ember/service';

import SankeyElement from 'fz-front-simulator/components/ac4/sankey-diagram/sankey-element';
import SankeyElements from 'fz-front-simulator/components/ac4/sankey-diagram/sankey-elements';

import type IntlService from 'ember-intl/services/intl';
import type SimulationRunner from 'fz-front-simulator/objects/simulation-runner';
import type SixAccountsStrategySavingParameters from 'fz-front-simulator/pods/saving/six-accounts-strategy/parameters';
import type SixAccountsStrategySavingResult from 'fz-front-simulator/pods/saving/six-accounts-strategy/result';
import type ApplicationService from 'fz-front-simulator/services/application';

interface SavingSixAccountsStrategyFlowArgs {
  darkMode: boolean;
  simulationRunner: SimulationRunner<SixAccountsStrategySavingParameters, SixAccountsStrategySavingResult>;
}

export default class SavingSixAccountsStrategyFlow extends Component<SavingSixAccountsStrategyFlowArgs> {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;

  elementId = guidFor(this);

  get data() {
    const result = this.args.simulationRunner.result;

    return this.args.simulationRunner.parameters && result
      ? new SankeyElements(
          [
            new SankeyElement(
              this.intl.t('components.saving.six-accounts-strategy.parameters.income'),
              this.args.darkMode ? this.application.bsTheme.light : this.application.bsTheme.dark,
            ),
            new SankeyElement(
              this.intl.t('components.saving.six-accounts-strategy.parameters.income'),
              this.application.bsTheme.success,
              this.intl.t('components.saving.six-accounts-strategy.accounts.financial-freedom'),
              result.financialFreedom.toFixed(2),
            ),
            new SankeyElement(
              this.intl.t('components.saving.six-accounts-strategy.parameters.income'),
              this.application.bsTheme.warning,
              this.intl.t('components.saving.six-accounts-strategy.accounts.long-term'),
              result.longTerm.toFixed(2),
            ),
            new SankeyElement(
              this.intl.t('components.saving.six-accounts-strategy.parameters.income'),
              this.application.bsTheme.primary,
              this.intl.t('components.saving.six-accounts-strategy.accounts.training'),
              result.training.toFixed(2),
            ),
            new SankeyElement(
              this.intl.t('components.saving.six-accounts-strategy.parameters.income'),
              this.application.bsTheme.danger,
              this.intl.t('components.saving.six-accounts-strategy.accounts.living-expenses'),
              result.livingExpenses.toFixed(2),
            ),
            new SankeyElement(
              this.intl.t('components.saving.six-accounts-strategy.parameters.income'),
              this.application.bsTheme.info,
              this.intl.t('components.saving.six-accounts-strategy.accounts.leisure'),
              result.leisure.toFixed(2),
            ),
            new SankeyElement(
              this.intl.t('components.saving.six-accounts-strategy.parameters.income'),
              this.application.bsTheme.secondary,
              this.intl.t('components.saving.six-accounts-strategy.accounts.donations'),
              result.donations.toFixed(2),
            ),
          ],
          this.args.simulationRunner.parameters?.currency,
        )
      : undefined;
  }
}
