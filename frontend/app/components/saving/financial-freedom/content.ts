import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import SimulationRunner from 'fz-front-simulator/objects/simulation-runner';
import SavingFinancialFreedomSimulation from 'fz-front-simulator/pods/-saving/financial-freedom/simulation';
import Simulators from 'fz-front-simulator/utils/simulators';

import type IntlService from 'ember-intl/services/intl';
import type SavingFinancialFreedomParameters from 'fz-front-simulator/pods/-saving/financial-freedom/parameters';
import type ApplicationService from 'fz-front-simulator/services/application';
import type RouterConfigService from 'fz-front-simulator/services/router-config';

interface SavingFinancialFreedomContentArgs {
  parameters: SavingFinancialFreedomParameters;
  title: string;
}

export default class SavingFinancialFreedomContent extends Component<SavingFinancialFreedomContentArgs> {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;
  @service declare routerConfig: RouterConfigService;

  simulation = new SavingFinancialFreedomSimulation();
  simulationRunner = new SimulationRunner(this.simulation, this.args.parameters);

  get route() {
    return this.routerConfig.getSimulatorRoute(Simulators.SAVING_FINANCIAL_FREEDOM);
  }

  @action
  onParametersChange(): void {
    this.simulationRunner.simulation = this.simulation;
  }
}
