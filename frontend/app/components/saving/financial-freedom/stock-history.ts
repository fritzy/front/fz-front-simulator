import Component from '@glimmer/component';
import { debug } from '@ember/debug';
import { inject as service } from '@ember/service';

import Am4ChartsStackedHistoryData from 'fz-front-simulator/objects/ac4/stacked-history-data';
import Am4ChartsStackedHistoryElementData from 'fz-front-simulator/objects/ac4/stacked-history-element-data';
import Am4ChartsStackedHistorySerie from 'fz-front-simulator/objects/ac4/stacked-history-serie';

import type IntlService from 'ember-intl/services/intl';
import type SimulationRunner from 'fz-front-simulator/objects/simulation-runner';
import type SavingFinancialFreedomResult from 'fz-front-simulator/pods/-saving/financial-freedom/result';
import type SavingFinancialFreedomResultEvent from 'fz-front-simulator/pods/-saving/financial-freedom/result-event';
import type SavingFinancialFreedomSimulation from 'fz-front-simulator/pods/-saving/financial-freedom/simulation';

interface SavingFinancialFreedomStockHistoryArgs {
  simulationRunner: SimulationRunner<SavingFinancialFreedomSimulation, SavingFinancialFreedomResult>;
}

export default class SavingFinancialFreedomStockHistory extends Component<SavingFinancialFreedomStockHistoryArgs> {
  @service declare intl: IntlService;

  get data(): Am4ChartsStackedHistoryData {
    debug('charts data computation');

    const elements =
      this.args.simulationRunner.result?.events.reduce(
        (values: Am4ChartsStackedHistoryElementData[], value: SavingFinancialFreedomResultEvent) => {
          const current = new Am4ChartsStackedHistoryElementData(value.date);

          values.push(current);

          current[1] = parseFloat(value.capital.toFixed(2));
          current[2] = parseFloat(value.compoundInterests.toFixed(2));
          current[3] = parseFloat(value.balance.toFixed(2));
          current[4] = parseFloat(value.fireThreshold.toFixed(2));

          return values;
        },
        new Array<Am4ChartsStackedHistoryElementData>(),
      ) || [];

    const series = [
      new Am4ChartsStackedHistorySerie(this.intl.t('components.saving.financial-freedom.stock-history.capital'), true),
      new Am4ChartsStackedHistorySerie(
        this.intl.t('components.saving.financial-freedom.stock-history.interests'),
        true,
      ),
      new Am4ChartsStackedHistorySerie(this.intl.t('components.saving.financial-freedom.stock-history.balance'), false),
      new Am4ChartsStackedHistorySerie(
        this.intl.t('components.saving.financial-freedom.stock-history.threshold'),
        false,
      ),
    ];

    return new Am4ChartsStackedHistoryData('€', series, elements);
  }
}
