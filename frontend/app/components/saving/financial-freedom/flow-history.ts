import Component from '@glimmer/component';
import { debug } from '@ember/debug';
import { inject as service } from '@ember/service';

import Am4ChartsStackedHistoryData from 'fz-front-simulator/objects/ac4/stacked-history-data';
import Am4ChartsStackedHistoryElementData from 'fz-front-simulator/objects/ac4/stacked-history-element-data';
import Am4ChartsStackedHistorySerie from 'fz-front-simulator/objects/ac4/stacked-history-serie';

import type IntlService from 'ember-intl/services/intl';
import type SimulationRunner from 'fz-front-simulator/objects/simulation-runner';
import type SavingFinancialFreedomResult from 'fz-front-simulator/pods/-saving/financial-freedom/result';
import type SavingFinancialFreedomResultEvent from 'fz-front-simulator/pods/-saving/financial-freedom/result-event';
import type SavingFinancialFreedomSimulation from 'fz-front-simulator/pods/-saving/financial-freedom/simulation';

interface SavingFinancialFreedomFlowHistoryArgs {
  simulationRunner: SimulationRunner<SavingFinancialFreedomSimulation, SavingFinancialFreedomResult>;
}

export default class SavingFinancialFreedomFlowHistory extends Component<SavingFinancialFreedomFlowHistoryArgs> {
  @service declare intl: IntlService;

  get data(): Am4ChartsStackedHistoryData {
    debug('charts data computation');

    const elements =
      this.args.simulationRunner.result?.events.reduce(
        (values: Am4ChartsStackedHistoryElementData[], value: SavingFinancialFreedomResultEvent) => {
          const current = new Am4ChartsStackedHistoryElementData(value.date);

          values.push(current);

          current[1] = parseFloat(value.expenses.toFixed(2));
          current[2] = parseFloat(value.saving.toFixed(2));
          current[3] = parseFloat(value.income.toFixed(2));
          current[4] = parseFloat(value.withdrawal.toFixed(2));

          return values;
        },
        new Array<Am4ChartsStackedHistoryElementData>(),
      ) || [];

    const series = [
      new Am4ChartsStackedHistorySerie(this.intl.t('components.saving.financial-freedom.flow-history.expenses'), true),
      new Am4ChartsStackedHistorySerie(this.intl.t('components.saving.financial-freedom.flow-history.saving'), true),
      new Am4ChartsStackedHistorySerie(this.intl.t('components.saving.financial-freedom.flow-history.income'), false),
      new Am4ChartsStackedHistorySerie(
        this.intl.t('components.saving.financial-freedom.flow-history.withdrawals'),
        false,
      ),
    ];

    return new Am4ChartsStackedHistoryData('€', series, elements);
  }
}
