import Component from '@glimmer/component';
import { action } from '@ember/object';

import type SavingFinancialFreedomParameters from 'fz-front-simulator/pods/-saving/financial-freedom/parameters';

interface SavingFinancialFreedomParametersArgs {
  parameters: SavingFinancialFreedomParameters;
  onChange: (parameters: SavingFinancialFreedomParameters) => void;
}

export default class SavingFinancialFreedomParametersComponent extends Component<SavingFinancialFreedomParametersArgs> {
  @action
  setCurrentWealth(value: number): void {
    this.args.parameters.currentWealth = value;
    this.args.onChange(this.args.parameters);
  }

  @action
  setExpenses(value: number): void {
    this.args.parameters.expenses = value;
    this.args.onChange(this.args.parameters);
  }

  @action
  setIncome(value: number): void {
    this.args.parameters.income = value;
    this.args.onChange(this.args.parameters);
  }

  @action
  setIncomeGrowth(value: number): void {
    this.args.parameters.incomeGrowth = value;
    this.args.onChange(this.args.parameters);
  }

  @action
  setInflation(value: number): void {
    this.args.parameters.inflation = value;
    this.args.onChange(this.args.parameters);
  }

  @action
  setYield(value: number): void {
    this.args.parameters.yield = value;
    this.args.onChange(this.args.parameters);
  }
}
