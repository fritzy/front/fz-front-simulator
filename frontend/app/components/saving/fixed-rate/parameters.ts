import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import { TransferType } from 'fz-front-simulator/enums/transfer-type';
import SavingParameters from 'fz-front-simulator/pods/savings/saving/saving-parameters';

import type IntlService from 'ember-intl/services/intl';
import type SavingParametersBuilder from 'fz-front-simulator/pods/savings/saving/saving-parameters-builder';

interface SavingFixedRateParametersArgs {
  onUpdated: (parameters: SavingParameters) => void;
  builder: SavingParametersBuilder;
}

export default class SavingFixedRateParameters extends Component<SavingFixedRateParametersArgs> {
  @service declare intl: IntlService;

  parameters = this.args.builder ? this.args.builder.build() : new SavingParameters();

  @action
  doNothing(): void {
    // Dummy action
  }

  @action
  setContribution(value: number): void {
    this.parameters.contribution = value;
    this.args.onUpdated(this.parameters);
  }

  @action
  setFees(value: number): void {
    this.parameters.fees = value;
    this.args.onUpdated(this.parameters);
  }

  @action
  setSaving(value: number): void {
    this.parameters.saving = value;
    this.args.onUpdated(this.parameters);
  }

  @action
  setTransferType(value: TransferType): void {
    this.parameters.transferType = value;
    this.args.onUpdated(this.parameters);
  }

  @action
  setYield(value: number): void {
    this.parameters.yield = value;
    this.args.onUpdated(this.parameters);
  }

  get transferTypes(): string[] {
    return Object.keys(TransferType);
  }
}
