import Component from '@glimmer/component';
import { debug } from '@ember/debug';
import { inject as service } from '@ember/service';

import Am4ChartsStackedHistoryData from 'fz-front-simulator/objects/ac4/stacked-history-data';
import Am4ChartsStackedHistoryElementData from 'fz-front-simulator/objects/ac4/stacked-history-element-data';
import Am4ChartsStackedHistorySerie from 'fz-front-simulator/objects/ac4/stacked-history-serie';

import type IntlService from 'ember-intl/services/intl';
import type SimulationRunner from 'fz-front-simulator/objects/simulation-runner';
import type FixedRateSavingResult from 'fz-front-simulator/pods/savings/saving/result';
import type FixedRateSavingResultEvent from 'fz-front-simulator/pods/savings/saving/result-event';
import type SavingParameters from 'fz-front-simulator/pods/savings/saving/saving-parameters';

interface SavingFixedRateHistoryArgs {
  simulationRunner: SimulationRunner<SavingParameters, FixedRateSavingResult>;
}

export default class SavingFixedRateHistory extends Component<SavingFixedRateHistoryArgs> {
  @service declare intl: IntlService;

  get data(): Am4ChartsStackedHistoryData {
    debug('charts data computation');

    const elements =
      this.args.simulationRunner.result?.events.reduce(
        (values: Am4ChartsStackedHistoryElementData[], value: FixedRateSavingResultEvent) => {
          const current = new Am4ChartsStackedHistoryElementData(value.date);

          values.push(current);

          current[1] = parseFloat(value.capital.toFixed(2));
          current[2] = parseFloat(value.interests.toFixed(2));
          current[3] = parseFloat(value.fees.toFixed(2));
          current[4] = parseFloat(value.balance.toFixed(2));
          current[5] = parseFloat(value.grossBalance.toFixed(2));

          return values;
        },
        new Array<Am4ChartsStackedHistoryElementData>(),
      ) || [];

    const series = [
      new Am4ChartsStackedHistorySerie(this.intl.t('components.saving.fixed-rate.history.capital'), true),
      new Am4ChartsStackedHistorySerie(this.intl.t('components.saving.fixed-rate.history.interests'), true),
      new Am4ChartsStackedHistorySerie(this.intl.t('components.saving.fixed-rate.history.fees'), true, true),
      new Am4ChartsStackedHistorySerie(this.intl.t('components.saving.fixed-rate.history.balance'), false),
      new Am4ChartsStackedHistorySerie(this.intl.t('components.saving.fixed-rate.history.gross-balance'), false),
    ];

    return new Am4ChartsStackedHistoryData('€', series, elements);
  }
}
