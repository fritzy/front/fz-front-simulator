import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { inject as service } from '@ember/service';

import type IntlService from 'ember-intl/services/intl';
import type SimulationRunner from 'fz-front-simulator/objects/simulation-runner';
import type FixedRateSavingResult from 'fz-front-simulator/pods/savings/saving/result';
import type SavingParameters from 'fz-front-simulator/pods/savings/saving/saving-parameters';

interface SavingFixedRateDistributionArgs {
  simulationRunner: SimulationRunner<SavingParameters, FixedRateSavingResult>;
}

export default class SavingFixedRateDistribution extends Component<SavingFixedRateDistributionArgs> {
  @service declare intl: IntlService;

  elementId = guidFor(this);

  get data() {
    return [
      {
        label: this.intl.t('components.saving.fixed-rate.distribution.deposits'),
        value: this.args.simulationRunner.result?.lastEvent.deposits.toFixed(0),
      },
      {
        label: this.intl.t('components.saving.fixed-rate.distribution.interests'),
        value: this.args.simulationRunner.result?.lastEvent.interests.toFixed(0),
      },
    ];
  }
}
