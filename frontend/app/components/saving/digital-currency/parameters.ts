import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import { CryptoSavingFrequency } from 'fz-front-simulator/enums/crypto-saving-frequency';

import type CoinGeckoCoin from 'fz-front-simulator/objects/cgc/coin-gecko-coin';
import type CryptoSavingParameters from 'fz-front-simulator/pods/savings/crypto/parameters';
import type ApplicationService from 'fz-front-simulator/services/application';
import type CryptoService from 'fz-front-simulator/services/crypto-service';

interface SavingCryptoParametersArgs {
  onChange: () => void;
  parameters: CryptoSavingParameters;
}

export default class SavingCryptoParametersComponent extends Component<SavingCryptoParametersArgs> {
  @service declare application: ApplicationService;
  @service declare cryptoService: CryptoService;

  get frequencies(): string[] {
    return Object.keys(CryptoSavingFrequency);
  }

  @action
  setAmount(value: number): void {
    this.args.parameters.amount = value;
    this.args.onChange();
  }

  @action
  setAsset(value: CoinGeckoCoin): void {
    this.args.parameters.asset = value;
    this.args.onChange();
  }

  @action
  setFrequency(value: CryptoSavingFrequency): void {
    this.args.parameters.frequency = value;
    this.args.onChange();
  }

  @action
  setFrom(changeEvent: Event): void {
    const target = changeEvent.target as HTMLInputElement;

    this.args.parameters.from = target.value;
    this.args.onChange();
  }

  @action
  setTo(changeEvent: Event): void {
    const target = changeEvent.target as HTMLInputElement;

    this.args.parameters.to = target.value;
    this.args.onChange();
  }

  @action
  updateCurrency(): void {
    this.args.parameters.currency = this.application.currency;
    this.args.onChange();
  }
}
