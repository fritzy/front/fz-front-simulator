import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import SimulationRunner from 'fz-front-simulator/objects/simulation-runner';
import SavingCryptoSimulation from 'fz-front-simulator/pods/savings/crypto/simulation';

import type IntlService from 'ember-intl/services/intl';
import type CryptoSavingParameters from 'fz-front-simulator/pods/savings/crypto/parameters';
import type ApplicationService from 'fz-front-simulator/services/application';

interface SavingDigitalCurrencyContentArgs {
  onLanguageChange: () => void;
  parameters: CryptoSavingParameters;
  title: string;
}

export default class SavingDigitalCurrencyContent extends Component<SavingDigitalCurrencyContentArgs> {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;

  simulation = new SavingCryptoSimulation();
  simulationRunner = new SimulationRunner(this.simulation, this.args.parameters);

  @action
  onParametersChange(): void {
    this.simulationRunner.simulation = this.simulation;
  }
}
