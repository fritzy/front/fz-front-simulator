import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

import type IntlService from 'ember-intl/services/intl';
import type ApplicationService from 'fz-front-simulator/services/application';

interface LegalNoticesContentArgs {
  onLanguageChange: () => void;
  title: string;
}

export default class LegalNoticesContent extends Component<LegalNoticesContentArgs> {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;

  applicationDate = new Date(2021, 9, 3);
  lastUpdatedDate = new Date(2021, 9, 4);
}
