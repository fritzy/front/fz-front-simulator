import { action } from '@ember/object';

import { CategoryAxis, ColumnSeries, LabelBullet, ValueAxis, XYChart } from '@amcharts/amcharts4/charts';
import { color } from '@amcharts/amcharts4/core';

import { Ac4chart } from '../chart';

import type { Ac4ChartArgs } from '../chart';
import type Ac4StackedColumn from './stacked-colmun';
import type Ac4StackedColumnsData from './stacked-columns-data';
import type { Column, ColumnSeriesDataItem } from '@amcharts/amcharts4/charts';
import type { Color } from '@amcharts/amcharts4/core';

interface Ac4StackedColumnsArgs extends Ac4ChartArgs {
  data: Ac4StackedColumnsData;
}

export default class Ac4StackedColumns extends Ac4chart<XYChart, Ac4StackedColumnsArgs> {
  @action
  setup(element: HTMLElement): void {
    super.setupChart(element, XYChart);
  }

  protected configure(chart: XYChart): void {
    chart.numberFormatter.numberFormat = {
      style: 'decimal',
      maximumFractionDigits: 2,
      notation: 'compact',
    };

    // Create axes
    let categoryAxis = chart.xAxes.push(new CategoryAxis());

    categoryAxis.dataFields.category = 'year';
    categoryAxis.renderer.grid.template.opacity = 0;

    let valueAxis = chart.yAxes.push(new ValueAxis());

    valueAxis.min = 0;

    if (valueAxis.tooltip) {
      valueAxis.tooltip.disabled = true;
    }

    this.createSeries(chart, '0');
    this.createSeries(chart, '1');
    this.createSeries(chart, '2');
    this.createSeries(chart, '3');
    this.createSeries(chart, '4');
    this.createSeries(chart, '5');
  }

  private createSeries(chart: XYChart, field: string): void {
    let series = chart.series.push(new ColumnSeries());

    series.dataFields.valueY = field;
    series.dataFields.categoryX = 'year';
    series.stacked = true;

    if (series.tooltip) {
      series.tooltip.getFillFromObject = false;
      series.tooltip.background.fill = color('#fff');
      series.tooltip.label.fill = color('#00');
    }

    series.columns.template.tooltipText = 'label';
    series.columns.template.adapter.add('tooltipText', this.tooltipText);

    let labelBullet = series.bullets.push(new LabelBullet());

    labelBullet.locationY = 0.5;
    labelBullet.label.adapter.add('text', (_v, target) => {
      const dataItem = target.dataItem as ColumnSeriesDataItem;
      const year = dataItem.categoryX;
      const valueY = dataItem.component?.dataFields.valueY;

      if (valueY) {
        return this.args.data.columns.find((c) => c.year === year)?.elements[Number(valueY)]?.label;
      }

      return undefined;
    });
    labelBullet.label.rotation = 270;
    labelBullet.label.fill = color('#fff');
    labelBullet.label.truncate = false;

    series.columns.template.adapter.add('hidden', (_v, target) => {
      return (target.tooltipDataItem as ColumnSeriesDataItem).valueY === 0;
    });

    series.columns.template.events.on('sizechanged', function (ev) {
      const dataItem = ev.target.dataItem as ColumnSeriesDataItem;

      if (dataItem?.bullets) {
        const height = ev.target.pixelHeight;

        dataItem.bullets.each(function (_id, bullet) {
          if (height > 50) {
            bullet.show();
          } else {
            bullet.hide();
          }
        });
      }
    });
  }

  toData(columns: Ac4StackedColumn[]) {
    return columns.map((c) => {
      const data = {
        year: c.year,
      };

      Object.assign(
        data,
        c.elements.map((e) => e.value),
      );

      return data;
    });
  }

  private tooltipText = (_defaultText: unknown, target: Column) => {
    const elementDataItem = target.tooltipDataItem as ColumnSeriesDataItem;

    if (elementDataItem.valueY === 0) {
      return '';
    }

    const dataContext = elementDataItem.dataContext as { year: string; [key: string]: number | string };
    const total = this.args.data.columns.find((c) => c.year === dataContext.year)?.total;
    let text = total
      ? `${dataContext.year} : ${this.intl.formatNumber(total, {
          currency: 'EUR',
          style: 'currency',
        })}\n`
      : '';

    this.chart?.series.values.reverse().forEach((item) => {
      if (item.dataFields.valueY && dataContext[item.dataFields.valueY] > 0) {
        text += `[${(item.stroke as Color).hex}]●[/] ${
          this.args.data.columns.find((c) => c.year === dataContext.year)?.elements[Number(item.dataFields.valueY)]
            ?.label
        } : ${this.intl.formatNumber(dataContext[item.dataFields.valueY] as number, {
          currency: 'EUR',
          style: 'currency',
        })}\n`;
      }
    });

    return text;
  };

  @action
  update(): void {
    if (this.chart) {
      this.chart.data = this.toData(this.args.data.columns);
      this.chart.invalidateRawData();
    }
  }
}
