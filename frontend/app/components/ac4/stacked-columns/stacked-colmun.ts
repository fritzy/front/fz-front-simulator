import type Ac4StackedColumnElement from './stacked-colmun-element';

export default class Ac4StackedColumn {
  constructor(readonly year: string, readonly total: number, readonly elements: Ac4StackedColumnElement[]) {}
}
