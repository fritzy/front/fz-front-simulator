import type Ac4StackedColumnsElementData from './stacked-colmun';

export default class Ac4StackedColumnsData {
  constructor(readonly columns: Ac4StackedColumnsElementData[]) {}
}
