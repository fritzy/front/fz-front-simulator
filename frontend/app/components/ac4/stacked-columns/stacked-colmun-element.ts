export default class Ac4StackedColumnElement {
  constructor(readonly value: string, readonly label: string) {}
}
