import type GaugeRange from './gauge-range';

export default class GaugeData {
  constructor(readonly score: number, readonly ranges: GaugeRange[]) {}
}
