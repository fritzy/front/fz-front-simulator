import { action } from '@ember/object';

import { ClockHand, GaugeChart, ValueAxis } from '@amcharts/amcharts4/charts';
import { color, Label, percent } from '@amcharts/amcharts4/core';

import { Ac4chart } from '../chart';

import type { Ac4ChartArgs } from '../chart';
import type GaugeData from './gauge-data';
import type GaugeRange from './gauge-range';
import type { AxisLabelCircular, AxisRendererCircular } from '@amcharts/amcharts4/charts';

interface Ac4GaugeArgs extends Ac4ChartArgs {
  data: GaugeData;
}

export default class Ac4Gauge extends Ac4chart<GaugeChart, Ac4GaugeArgs> {
  hand?: ClockHand;
  label?: Label;

  @action
  setup(element: HTMLElement) {
    super.setupChart(element, GaugeChart);
  }

  protected hasData() {
    return true;
  }

  protected configure(chart: GaugeChart) {
    chart.data = [];

    const chartMin = 0;
    const chartMax = 1000;

    chart.hiddenState.properties.opacity = 0;
    chart.fontSize = 11;
    chart.innerRadius = percent(80);
    chart.resizable = true;

    const axis = chart.xAxes.push(new ValueAxis<AxisRendererCircular>());

    axis.min = chartMin;
    axis.max = chartMax;
    axis.strictMinMax = true;
    axis.renderer.radius = percent(80);
    axis.renderer.inside = true;
    axis.renderer.line.strokeOpacity = 0.1;
    axis.renderer.ticks.template.disabled = false;
    axis.renderer.ticks.template.strokeOpacity = 1;
    axis.renderer.ticks.template.strokeWidth = 0.5;
    axis.renderer.ticks.template.length = 5;
    axis.renderer.grid.template.disabled = true;
    axis.renderer.labels.template.radius = percent(15);
    axis.renderer.labels.template.fontSize = '0.9em';

    const axis2 = chart.xAxes.push(new ValueAxis<AxisRendererCircular>());

    axis2.min = chartMin;
    axis2.max = chartMax;
    axis2.strictMinMax = true;
    axis2.renderer.labels.template.disabled = true;
    axis2.renderer.ticks.template.disabled = true;
    axis2.renderer.grid.template.disabled = false;
    axis2.renderer.grid.template.opacity = 0.5;
    axis2.renderer.labels.template.bent = true;
    axis2.renderer.labels.template.fill = color('#000');
    axis2.renderer.labels.template.fontWeight = 'bold';
    axis2.renderer.labels.template.fillOpacity = 0.3;

    this.args.data.ranges.forEach((range) => this.createGradingRange(axis2, range, chartMin, chartMax));

    let matchingGrade = this.lookUpGrade(this.args.data.score, this.args.data.ranges);

    this.label = chart.radarContainer.createChild(Label);

    this.label.isMeasured = false;
    this.label.fontSize = '4em';
    this.label.x = percent(50);
    this.label.horizontalCenter = 'middle';
    this.label.verticalCenter = 'bottom';
    this.label.text = this.args.data.score.toFixed(2);

    if (matchingGrade) {
      this.label.fill = color(matchingGrade.color);
    }

    let label2 = chart.radarContainer.createChild(Label);

    label2.isMeasured = false;
    label2.fontSize = '2em';
    label2.horizontalCenter = 'middle';

    if (matchingGrade) {
      label2.text = matchingGrade.title.toUpperCase();
      label2.fill = color(matchingGrade.color);
    }

    this.hand = chart.hands.push(new ClockHand());

    this.hand.axis = axis2;
    this.hand.innerRadius = percent(55);
    this.hand.startWidth = 8;
    this.hand.pin.disabled = true;
    this.hand.value = this.args.data.score;
    this.hand.fill = color('#444');
    this.hand.stroke = color('#000');

    this.hand.events.on('positionchanged', () => {
      if (this.label && this.hand) {
        this.label.text = axis2.positionToValue(this.hand.currentPosition).toFixed(1);

        matchingGrade = this.lookUpGrade(axis.positionToValue(this.hand.currentPosition), this.args.data.ranges);

        if (matchingGrade) {
          label2.text = matchingGrade.title.toUpperCase();
          label2.fill = color(matchingGrade.color);
          label2.stroke = color(matchingGrade.color);

          if (this.label) {
            this.label.fill = color(matchingGrade.color);
          }
        }
      }
    });
  }

  private createGradingRange(
    axis: ValueAxis<AxisRendererCircular>,
    grading: GaugeRange,
    chartMin: number,
    chartMax: number,
  ) {
    let range = axis.axisRanges.create();

    range.axisFill.fill = color(grading.color);
    range.axisFill.fillOpacity = 0.8;
    range.axisFill.zIndex = -1;
    range.value = grading.lowScore > chartMin ? grading.lowScore : chartMin;
    range.endValue = grading.highScore < chartMax ? grading.highScore : chartMax;
    range.grid.strokeOpacity = 0;
    range.label.inside = true;
    range.label.text = grading.title.toUpperCase();
    range.label.inside = true;
    range.label.location = 0.5;
    range.label.inside = true;
    (range.label as AxisLabelCircular).radius = percent(10);
    range.label.paddingBottom = -5; // ~half font size
    range.label.fontSize = '0.9em';
  }

  lookUpGrade(lookupScore: number, grades: GaugeRange[]) {
    // Only change code below this line
    for (let i = 0; i < grades.length; i++) {
      if (grades[i].lowScore < lookupScore && grades[i].highScore >= lookupScore) {
        return grades[i];
      }
    }

    return null;
  }

  @action
  update() {
    if (this.chart) {
      if (this.hand) {
        this.hand.value = this.args.data.score;
      }

      this.chart.invalidateRawData();
    }
  }
}
