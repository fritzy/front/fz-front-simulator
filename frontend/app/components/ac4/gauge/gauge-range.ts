export default class GaugeRange {
  constructor(readonly title: string, readonly color: string, readonly lowScore: number, readonly highScore: number) {}
}
