import { action } from '@ember/object';

import { Legend, PieChart, PieSeries } from '@amcharts/amcharts4/charts';
import { color, DropShadowFilter, percent } from '@amcharts/amcharts4/core';
import { SliceGrouper } from '@amcharts/amcharts4/plugins/sliceGrouper';

import { Ac4chart } from '../chart';

import type { Ac4ChartArgs } from '../chart';
import type { PercentSeriesDataItem } from '@amcharts/amcharts4/.internal/charts/series/PercentSeries';
import type { AxisLabelCircular } from '@amcharts/amcharts4/charts';
import type { Slice } from '@amcharts/amcharts4/core';

interface Ac4PiechartArgs extends Ac4ChartArgs {
  categoryField: string;
  categoryFieldMapper: (category: string) => string;
  data: { [x: string]: number | string }[];
  valueField: string;
}

export default class Ac4Piechart extends Ac4chart<PieChart, Ac4PiechartArgs> {
  @action
  setup(element: HTMLElement) {
    super.setupChart(element, PieChart);
  }

  protected configure(chart: PieChart) {
    // Add and configure Series
    const pieSeries = chart.series.push(new PieSeries());
    const grouper = pieSeries.plugins.push(new SliceGrouper());

    grouper.limit = 8;
    grouper.groupName = 'Autres';
    grouper.clickBehavior = 'zoom';

    pieSeries.dataFields.category = this.args.categoryField;
    pieSeries.dataFields.value = this.args.valueField;

    if (this.args.categoryFieldMapper instanceof Function) {
      pieSeries.adapter.add('dataContextValue', (value) => {
        if (this.args.categoryFieldMapper && value.field === 'category') {
          value.value = this.args.categoryFieldMapper(value.value);
        }

        return value;
      });
    }

    // Let's cut a hole in our Pie chart the size of 30% the radius
    chart.innerRadius = percent(30);

    // Put a thick white border around each Slice
    pieSeries.slices.template.stroke = color('#fff');
    pieSeries.slices.template.strokeWidth = 2;
    pieSeries.slices.template.strokeOpacity = 1;
    // change the cursor on hover to make it apparent the object can be interacted with
    pieSeries.slices.template.cursorOverStyle = [
      {
        property: 'cursor',
        value: 'pointer',
      },
    ];

    pieSeries.alignLabels = false;
    pieSeries.slices.template.adapter.add('tooltipText', this.formatTooltip);
    pieSeries.labels.template.bent = true;
    pieSeries.labels.template.radius = 3;
    pieSeries.labels.template.padding(0, 0, 0, 0);

    pieSeries.labels.template.events.on('ready', this.hideSmall);
    pieSeries.ticks.template.disabled = true;

    // Create a base filter effect (as if it's not there) for the hover to return to
    const shadow = pieSeries.slices.template.filters.push(new DropShadowFilter());

    shadow.opacity = 0;

    // Create hover state
    // normally we have to create the hover state, in this case it already exists
    const hoverState = pieSeries.slices.template.states.getKey('hover');

    // Slightly shift the shadow and make it more prominent on hover
    if (hoverState != null) {
      const hoverShadow = hoverState.filters.push(new DropShadowFilter());

      hoverShadow.opacity = 0.7;
      hoverShadow.blur = 5;
    }

    // Add a legend
    chart.legend = new Legend();
  }

  private formatTooltip = (text: string | undefined, target: Slice) => {
    const data = target.dataItem as PercentSeriesDataItem;

    if (data) {
      return `${data.category}: ${this.intl.formatNumber(data.values.value.percent, {
        minimumFractionDigits: 1,
        maximumFractionDigits: 1,
      })}% (${this.intl.formatNumber(data.value, { currency: 'EUR', style: 'currency', maximumFractionDigits: 0 })})`;
    } else {
      return text;
    }
  };

  private hideSmall(ev: { type: string; target: AxisLabelCircular }): void {
    if (ev.target.dataItem && ev.target.dataItem.values.value.percent < 10) {
      ev.target.disabled = true;
    }
  }

  @action
  update() {
    if (this.chart) {
      this.chart.data = this.args.data?.filter((slice) => slice[this.args.valueField] > 0);
      this.chart.invalidateRawData();
    }
  }
}
