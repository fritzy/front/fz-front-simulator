import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { registerDestructor } from '@ember/destroyable';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import { color, Container, create, Label, percent, unuseTheme, useTheme } from '@amcharts/amcharts4/core';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import am4themes_dark from '@amcharts/amcharts4/themes/dark';
import am4themes_kelly from '@amcharts/amcharts4/themes/kelly';
import { restartableTask, timeout } from 'ember-concurrency';
import { perform } from 'ember-concurrency-ts';

import ChartDataModifier from './chart-data';

import type { Chart } from '@amcharts/amcharts4/charts';
import type { AMEvent } from '@amcharts/amcharts4/core';
import type IntlService from 'ember-intl/services/intl';
import type ApplicationService from 'fz-front-simulator/services/application';

export interface Ac4ChartArgs {
  setBuilding: (building: boolean) => void;
}

export abstract class Ac4chart<C extends Chart, T extends Ac4ChartArgs> extends Component<T> {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;

  @tracked building = false;
  @tracked protected chart?: C;
  private indicator?: Container;

  dataModifier = ChartDataModifier;

  @restartableTask
  async setupChartTask(createChart: () => void) {
    await timeout(250);

    createChart();
    registerDestructor(this, () => this.destroyChart());
  }

  protected abstract configure(chart: C): void;

  protected hasData(ev: AMEvent<C, C['_events']>['beforedatavalidated']) {
    return ev.target.data.length !== 0;
  }

  protected abstract setup(element: HTMLElement): void;

  protected abstract update(): void;

  protected setupChart(element: HTMLElement, classType: new () => C) {
    this.setupTheme();
    perform(this.setupChartTask, () => {
      this.building = true;

      if (this.args.setBuilding) {
        this.args.setBuilding(true);
      }

      const c = create(element, classType);

      this.configure(c);
      this.chart = c;
      this.chart.events.on('ready', () => {
        this.building = false;

        if (this.args.setBuilding) {
          this.args.setBuilding(false);
        }
      });

      this.chart.events.on('beforedatavalidated', (ev) => {
        if (!this.hasData(ev)) {
          this.showIndicator();
        } else if (this.indicator) {
          this.hideIndicator();
        }
      });
    });
  }

  private destroyChart() {
    this.indicator?.dispose();
    this.indicator = undefined;
    this.chart?.dispose();
  }

  protected setupTheme(): void {
    useTheme(am4themes_animated);
    this.setTheme(this.application.darkMode);
  }

  protected setTheme(darkMode: boolean) {
    if (darkMode) {
      unuseTheme(am4themes_kelly);
      useTheme(am4themes_dark);
    } else {
      unuseTheme(am4themes_dark);
      useTheme(am4themes_kelly);
    }
  }

  @action
  regenerate(element: HTMLElement): void {
    if (this.chart) {
      this.destroyChart();
      this.setup(element);
    }
  }

  hideIndicator() {
    this.indicator?.hide();
  }

  showIndicator() {
    if (this.indicator) {
      this.indicator.show();
    } else {
      this.indicator = this.chart?.tooltipContainer?.createChild(Container);

      if (this.indicator) {
        this.indicator.background.fill = this.application.darkMode
          ? color(this.application.darkTheme.dark)
          : color(this.application.darkTheme.light);
        this.indicator.background.fillOpacity = 0.8;
        this.indicator.width = percent(100);
        this.indicator.height = percent(100);

        const indicatorLabel = this.indicator.createChild(Label);

        indicatorLabel.text = this.intl.t('components.app.ac4.chart.no-data');
        indicatorLabel.align = 'center';
        indicatorLabel.valign = 'middle';
        indicatorLabel.fontSize = 20;
      }
    }
  }
}
