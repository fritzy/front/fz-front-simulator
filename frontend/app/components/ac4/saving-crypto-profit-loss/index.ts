import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import { DateAxis, Legend, LineSeries, ValueAxis, XYChart, XYCursor } from '@amcharts/amcharts4/charts';
import { Scrollbar } from '@amcharts/amcharts4/core';
import Money from 'bigint-money';

import { Ac4chart } from '../chart';

import type { Ac4ChartArgs } from '../chart';
import type { LineSeriesDataItem, XYSeriesDataItem } from '@amcharts/amcharts4/charts';
import type CryptoSavingResultEvent from 'fz-front-simulator/pods/savings/crypto/result-event';
import type MoneyService from 'fz-front-simulator/services/money';

interface Ac4SavingCryptoProfitLossArgs extends Ac4ChartArgs {
  data: CryptoSavingResultEvent[];
}

export default class Ac4SavingCryptoProfitLoss extends Ac4chart<XYChart, Ac4SavingCryptoProfitLossArgs> {
  @service declare money: MoneyService;

  investedCurrencyTicker = '';

  @action
  setup(element: HTMLElement): void {
    super.setupChart(element, XYChart);
  }
  protected configure(chart: XYChart): void {
    chart.cursor = new XYCursor();
    chart.legend = new Legend();
    chart.scrollbarX = new Scrollbar();

    chart.numberFormatter.numberFormat = {
      style: 'decimal',
      maximumFractionDigits: 2,
      notation: 'compact',
    };

    // Date axis
    let dateAxis = chart.xAxes.push(new DateAxis());

    dateAxis.baseInterval = {
      timeUnit: 'day',
      count: 1,
    };
    dateAxis.tooltipDateFormat = 'dd-MM-yyyy';

    // Euro axis
    let eurosAxis = chart.yAxes.push(new ValueAxis());

    // Value
    let valueSeries = chart.series.push(new LineSeries());

    valueSeries.adapter.add('tooltipText', this.investedTooltip);
    valueSeries.dataFields.valueY = 'valueAmount';
    valueSeries.dataFields.dateX = 'date';
    valueSeries.name = 'Valeur';
    valueSeries.smoothing = 'monotoneX';
    valueSeries.strokeWidth = 2;
    valueSeries.yAxis = eurosAxis;

    // Invested
    let investedSeries = chart.series.push(new LineSeries());

    investedSeries.adapter.add('tooltipText', this.investedTooltip);
    investedSeries.dataFields.dateX = 'date';
    investedSeries.dataFields.valueY = 'investedAmount';
    investedSeries.name = 'Investi';
    investedSeries.smoothing = 'monotoneX';
    investedSeries.strokeWidth = 2;
    investedSeries.yAxis = eurosAxis;

    // price
    const priceValueAxis = chart.yAxes.push(new ValueAxis());

    if (priceValueAxis.tooltip) {
      priceValueAxis.tooltip.disabled = true;
    }

    priceValueAxis.renderer.disabled = true;
    priceValueAxis.renderer.grid.template.disabled = true;

    let priceSeries = chart.series.push(new LineSeries());

    priceSeries.adapter.add('tooltipText', this.investedTooltip);
    priceSeries.dataFields.dateX = 'date';
    priceSeries.dataFields.valueY = 'priceAmount';
    priceSeries.hidden = true;
    priceSeries.name = 'Prix';
    priceSeries.smoothing = 'monotoneX';
    priceSeries.strokeWidth = 2;
    priceSeries.yAxis = priceValueAxis;

    // Profit / loss series
    let profitLossSeries = chart.series.push(new LineSeries());

    profitLossSeries.adapter.add('tooltipText', this.investedTooltip);
    profitLossSeries.dataFields.dateX = 'date';
    profitLossSeries.dataFields.valueY = 'plusMinusValueAmount';
    profitLossSeries.fillOpacity = 0.1;
    profitLossSeries.name = 'Gains / Pertes';
    profitLossSeries.smoothing = 'monotoneX';
    profitLossSeries.stroke = chart.colors.getIndex(7);
    profitLossSeries.fill = profitLossSeries.stroke;
    profitLossSeries.strokeWidth = 2;
    profitLossSeries.yAxis = eurosAxis;

    if (profitLossSeries.tooltip) {
      profitLossSeries.tooltip.getFillFromObject = false;
      profitLossSeries.tooltip.adapter.add('x', (x) => {
        if (profitLossSeries.tooltip && this.chart) {
          const valueY = (profitLossSeries.tooltip.tooltipDataItem as XYSeriesDataItem).valueY;

          if (valueY < 0) {
            profitLossSeries.tooltip.background.fill = this.chart.colors.getIndex(4);
          } else {
            profitLossSeries.tooltip.background.fill = this.chart.colors.getIndex(7);
          }
        }

        return x;
      });
    }

    let range = eurosAxis.createSeriesRange(profitLossSeries);

    range.value = 0;
    range.endValue = -10000000000;
    range.contents.stroke = chart.colors.getIndex(4);
    range.contents.fill = range.contents.stroke;
    range.contents.strokeOpacity = 0.7;
    range.contents.fillOpacity = 0.1;
  }

  private investedTooltip = (text: string | undefined, target: LineSeries) => {
    const data = target.tooltipDataItem as LineSeriesDataItem;

    if (data?.valueY) {
      return `{name}\n[bold font-size: 20]${this.money.format(
        new Money(data.valueY.toFixed(8), this.investedCurrencyTicker),
        data?.valueY > -0.01 && data?.valueY < 0.01 ? 8 : undefined,
      )}[/]`;
    } else {
      return text;
    }
  };

  @action
  update(): void {
    if (this.chart) {
      this.chart.data = this.args.data;

      if (this.args.data?.length) {
        this.investedCurrencyTicker = this.args.data[this.args.data.length - 1].invested.currency;
      }

      this.chart.invalidateRawData();
    }
  }
}
