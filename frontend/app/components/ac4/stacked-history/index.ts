import { action } from '@ember/object';

import { DateAxis, Legend, LineSeries, ValueAxis, XYChart, XYCursor } from '@amcharts/amcharts4/charts';
import { color, LinePattern, Scrollbar } from '@amcharts/amcharts4/core';

import { Ac4chart } from '../chart';

import type { Ac4ChartArgs } from '../chart';
import type { LineSeriesDataItem } from '@amcharts/amcharts4/charts';
import type { Color } from '@amcharts/amcharts4/core';
import type Am4ChartsStackedHistoryData from 'fz-front-simulator/objects/ac4/stacked-history-data';

interface Ac4StackedHistoryArgs extends Ac4ChartArgs {
  data: Am4ChartsStackedHistoryData;
}

export default class Ac4StackedHistory extends Ac4chart<XYChart, Ac4StackedHistoryArgs> {
  @action
  setup(element: HTMLElement): void {
    super.setupChart(element, XYChart);
  }

  protected configure(chart: XYChart): void {
    const keys = this.args.data.elements[0] ? Object.keys(this.args.data.elements[0]) : undefined;

    let ids;

    if (keys) {
      ids = keys.filter((e: string) => parseInt(e, 10) > 0);
    } else {
      ids = this.args.data.series ? this.args.data.series.map((_s, i) => (i + 1).toString()) : [];
    }

    chart.dateFormatter.inputDateFormat = 'yyyy-MM-dd';

    const dateAxis = chart.xAxes.push(new DateAxis());

    dateAxis.renderer.minGridDistance = 60;
    dateAxis.startLocation = 0.5;
    dateAxis.endLocation = 0.5;
    dateAxis.baseInterval = {
      timeUnit: 'day',
      count: 1,
    };
    dateAxis.tooltipDateFormat = 'dd-MM-yyyy';

    const valueAxis = chart.yAxes.push(new ValueAxis());

    if (valueAxis.tooltip) {
      valueAxis.tooltip.disabled = true;
    }

    for (const [i, id] of ids.entries()) {
      const series = chart.series.push(new LineSeries());

      series.dataFields.dateX = 'date';
      series.name = this.args.data.series[i].label;
      series.dataFields.valueY = id;

      const stacked = this.args.data.series[i].stacked;

      if (series.tooltip && stacked) {
        series.adapter.add('tooltipText', this.tooltipText);

        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = color('#fff');
        series.tooltip.label.fill = color('#00');

        // Prevent cross-fading of tooltips
        series.tooltip.defaultState.transitionDuration = 0;
        series.tooltip.hiddenState.transitionDuration = 0;
      }

      if (stacked) {
        series.fillOpacity = 0.6;
      }

      const striped = this.args.data.series[i].striped;

      if (striped) {
        let pattern = new LinePattern();

        pattern.rotation = -45;
        pattern.width = 1000;
        pattern.height = 1000;
        pattern.gap = 10;
        series.fill = pattern;
        series.fillOpacity = 0.2;
      }

      series.strokeWidth = 2;
      series.stacked = this.args.data.series[i].stacked;
    }

    chart.cursor = new XYCursor();
    chart.cursor.xAxis = dateAxis;
    chart.cursor.maxTooltipDistance = 0;
    chart.scrollbarX = new Scrollbar();

    // Add a legend
    chart.legend = new Legend();
  }

  private tooltipText = (_defaultText: unknown, target: LineSeries) => {
    const elementDataItem = target.tooltipDataItem as LineSeriesDataItem;

    if (elementDataItem.valueY === 0) {
      return '';
    }

    let text = '';

    this.chart?.series.each((item) => {
      const tooltipDataItem = item.tooltipDataItem as LineSeriesDataItem;

      text += `[${(item.stroke as Color).hex}]●[/] ${item.name} : ${this.intl.formatNumber(tooltipDataItem.valueY, {
        currency: 'EUR',
        style: 'currency',
        maximumFractionDigits: 0,
      })}\n`;
    });

    return text;
  };

  @action
  update(): void {
    if (this.chart) {
      this.chart.data = this.args.data.elements;
      this.chart.invalidateRawData();
    }
  }
}
