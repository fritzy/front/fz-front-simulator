import Modifier from 'ember-modifier';

import type { Chart } from '@amcharts/amcharts4/charts';

interface ChartDataModifierArgs {
  positional: [data: unknown];
  named: {
    setup(element: HTMLElement): Chart;
    update: () => void;
  };
}

export default class ChartDataModifier extends Modifier<ChartDataModifierArgs> {
  constructor(owner: unknown, args: ChartDataModifierArgs) {
    super(owner, args);

    if (!this.args.named.setup) {
      throw new Error('setup must be defined');
    }

    if (typeof this.args.named.setup !== 'function') {
      throw new Error('setup must be a function');
    }

    if (!this.args.named.update) {
      throw new Error('update must be defined');
    }

    if (typeof this.args.named.update !== 'function') {
      throw new Error('update must be a function');
    }
  }

  didInstall() {
    this.args.named.setup(this.element as HTMLElement);
    this.didUpdateArguments();
  }

  didUpdateArguments() {
    this.args.named.update();
  }
}
