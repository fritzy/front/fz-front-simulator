import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import { DateAxis, Legend, LineSeries, ValueAxis, XYChart, XYCursor } from '@amcharts/amcharts4/charts';
import { Scrollbar } from '@amcharts/amcharts4/core';
import Money from 'bigint-money';

import { Ac4chart } from '../chart';

import type { Ac4ChartArgs } from '../chart';
import type { LineSeriesDataItem } from '@amcharts/amcharts4/charts';
import type CryptoSavingResultEvent from 'fz-front-simulator/pods/savings/crypto/result-event';
import type MoneyService from 'fz-front-simulator/services/money';

interface Ac4SavingCryptoHistoryArgs extends Ac4ChartArgs {
  data: CryptoSavingResultEvent[];
}

export default class Ac4SavingCryptoHistory extends Ac4chart<XYChart, Ac4SavingCryptoHistoryArgs> {
  @service declare money: MoneyService;

  investedCurrencyTicker = '';
  accumulatedCurrencyTicker = '';

  @action
  setup(element: HTMLElement): void {
    super.setupChart(element, XYChart);
  }

  protected configure(chart: XYChart): void {
    chart.cursor = new XYCursor();
    chart.legend = new Legend();
    chart.scrollbarX = new Scrollbar();

    chart.numberFormatter.numberFormat = {
      style: 'decimal',
      maximumFractionDigits: 2,
      notation: 'compact',
    };

    // Date axis
    let dateAxis = chart.xAxes.push(new DateAxis());

    dateAxis.baseInterval = {
      timeUnit: 'day',
      count: 1,
    };
    dateAxis.tooltipDateFormat = 'dd-MM-yyyy';

    let coinAxis = chart.yAxes.push(new ValueAxis());

    // Accumulated
    let accumulatedSeries = chart.series.push(new LineSeries());

    accumulatedSeries.dataFields.valueY = 'accumulatedAmount';
    accumulatedSeries.dataFields.dateX = 'date';
    accumulatedSeries.yAxis = coinAxis;
    accumulatedSeries.name = 'Acquis';
    accumulatedSeries.smoothing = 'monotoneX';
    accumulatedSeries.strokeWidth = 2;
    accumulatedSeries.adapter.add('tooltipText', this.accumulatedTooltip);

    // invested
    let eurosAxis = chart.yAxes.push(new ValueAxis());

    eurosAxis.renderer.opposite = true;
    eurosAxis.renderer.grid.template.disabled = true;

    let investedSeries = chart.series.push(new LineSeries());

    investedSeries.adapter.add('tooltipText', this.investedTooltip);
    investedSeries.dataFields.valueY = 'investedAmount';
    investedSeries.dataFields.dateX = 'date';
    investedSeries.name = 'Investi';
    investedSeries.smoothing = 'monotoneX';
    investedSeries.strokeWidth = 2;
    investedSeries.yAxis = eurosAxis;

    // price
    const priceValueAxis = chart.yAxes.push(new ValueAxis());

    if (priceValueAxis.tooltip) {
      priceValueAxis.tooltip.disabled = true;
    }

    priceValueAxis.renderer.disabled = true;
    priceValueAxis.renderer.grid.template.disabled = true;

    let priceSeries = chart.series.push(new LineSeries());

    priceSeries.adapter.add('tooltipText', this.investedTooltip);
    priceSeries.dataFields.dateX = 'date';
    priceSeries.dataFields.valueY = 'priceAmount';
    priceSeries.hidden = true;
    priceSeries.name = 'Prix';
    priceSeries.smoothing = 'monotoneX';
    priceSeries.strokeWidth = 2;
    priceSeries.yAxis = priceValueAxis;

    // Value
    let valueSeries = chart.series.push(new LineSeries());

    valueSeries.adapter.add('tooltipText', this.investedTooltip);
    valueSeries.dataFields.dateX = 'date';
    valueSeries.dataFields.valueY = 'valueAmount';
    valueSeries.fill = valueSeries.stroke;
    valueSeries.fillOpacity = 0.1;
    valueSeries.name = 'Valeur';
    valueSeries.smoothing = 'monotoneX';
    valueSeries.strokeWidth = 2;
    valueSeries.yAxis = eurosAxis;
  }

  private accumulatedTooltip = (text: string | undefined, target: LineSeries) => {
    const data = target.tooltipDataItem as LineSeriesDataItem;

    if (data?.valueY) {
      return `{name}\n[bold font-size: 20]${this.money.format(
        new Money(data.valueY.toFixed(8), this.accumulatedCurrencyTicker),
        data?.valueY > -0.01 && data?.valueY < 0.01 ? 8 : undefined,
      )}[/]`;
    } else {
      return text;
    }
  };

  private investedTooltip = (text: string | undefined, target: LineSeries) => {
    const data = target.tooltipDataItem as LineSeriesDataItem;

    if (data?.valueY) {
      return `{name}\n[bold font-size: 20]${this.money.format(
        new Money(data.valueY.toFixed(8), this.investedCurrencyTicker),
        data?.valueY > -0.01 && data?.valueY < 0.01 ? 8 : undefined,
      )}[/]`;
    } else {
      return text;
    }
  };

  @action
  update(): void {
    if (this.chart) {
      this.chart.data = this.args.data;

      if (this.args.data?.length) {
        this.investedCurrencyTicker = this.args.data[this.args.data.length - 1].invested.currency;
        this.accumulatedCurrencyTicker = this.args.data[this.args.data.length - 1].accumulated.currency;
      }

      this.chart.invalidateRawData();
    }
  }
}
