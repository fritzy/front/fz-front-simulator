import type SankeyElement from './sankey-element';
import type Currency from 'fz-front-simulator/objects/currency';

export default class SankeyElements {
  constructor(readonly elements: SankeyElement[], readonly currency: Currency) {}
}
