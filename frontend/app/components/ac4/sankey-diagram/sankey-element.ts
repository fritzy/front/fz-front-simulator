export default class SankeyElement {
  color?: string;
  to?: string;
  value?: string;

  constructor(readonly from: string, color?: string, to?: string, value?: string) {
    this.color = color;
    this.to = to;
    this.value = value;
  }
}
