import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import { LabelBullet, SankeyDiagram, SankeyNode } from '@amcharts/amcharts4/charts';

import { Ac4chart } from '../chart';

import type { Ac4ChartArgs } from '../chart';
import type SankeyElements from './sankey-elements';
import type { SankeyDiagramDataItem } from '@amcharts/amcharts4/charts';
import type MoneyService from 'fz-front-simulator/services/money';

interface Ac4SankeyDiagramArgs extends Ac4ChartArgs {
  darkMode: boolean;
  data: SankeyElements | undefined;
  loading: boolean;
}

export default class Ac4SankeyDiagram extends Ac4chart<SankeyDiagram, Ac4SankeyDiagramArgs> {
  @service declare money: MoneyService;

  currencyTicker = '';
  accumulatedCurrencyTicker = '';

  @action
  setup(element: HTMLElement): void {
    super.setupChart(element, SankeyDiagram);
  }

  protected configure(chart: SankeyDiagram): void {
    chart.hiddenState.properties.opacity = 0;

    const hoverState = chart.links.template.states.create('hover');

    hoverState.properties.fillOpacity = 0.6;

    chart.dataFields.fromName = 'from';
    chart.dataFields.toName = 'to';
    chart.dataFields.value = 'value';
    chart.dataFields.color = 'color';

    const linkTemplate = chart.links.template;

    linkTemplate.colorMode = 'toNode';
    linkTemplate.fillOpacity = 1;

    chart.padding(10, 10, 10, 10);

    chart.nodes.template.width = 10;
    chart.nodes.template.nameLabel.width = 200;
    chart.nodes.template.nameLabel.adapter.add('dx', (value, target: LabelBullet) => {
      if ((target.dataItem as SankeyDiagramDataItem)?.toNode) {
        return -200;
      }

      return value;
    });

    chart.responsive.enabled = true;
    chart.responsive.rules.push({
      relevant: function (target) {
        if (target.pixelWidth < 768) {
          return true;
        }

        return false;
      },

      state: function (target, stateId) {
        if (target instanceof SankeyNode) {
          let state = target.states.create(stateId);

          state.properties.visible = false;

          return state;
        }

        if (target instanceof LabelBullet) {
          let state = target.states.create(stateId);

          state.properties.disabled = true;

          return state;
        }

        return undefined;
      },
    });
  }

  @action
  update(): void {
    if (this.chart && this.args.data) {
      this.chart.data = this.args.data.elements;
      this.currencyTicker = this.args.data.currency.ticker;
      this.chart.invalidateRawData();
    }
  }
}
