import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

import Simulators from 'fz-front-simulator/utils/simulators';

import type IntlService from 'ember-intl/services/intl';
import type ApplicationService from 'fz-front-simulator/services/application';
import type IntlConfigService from 'fz-front-simulator/services/intl-config';
import type RouterConfigService from 'fz-front-simulator/services/router-config';

interface IndexContentArgs {
  onLanguageChange: () => void;
}

export default class IndexContent extends Component<IndexContentArgs> {
  @service declare application: ApplicationService;
  @service declare intlConfig: IntlConfigService;
  @service declare intl: IntlService;
  @service declare routerConfig: RouterConfigService;

  get loanSimulators() {
    return this.intlConfig.sortByLabelKey(Simulators.LOAN_SIMULATORS);
  }

  get portfolioSimulators() {
    return this.intlConfig.sortByLabelKey(Simulators.PORTFOLIO_SIMULATORS);
  }

  get savingSimulators() {
    return this.intlConfig.sortByLabelKey(Simulators.SAVING_SIMULATORS);
  }

  get taxationSimulators() {
    return this.intlConfig.sortByLabelKey(Simulators.TAXATAION_SIMULATORS);
  }
}
