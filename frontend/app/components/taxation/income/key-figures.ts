import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

import type IntlService from 'ember-intl/services/intl';
import type SimulationRunner from 'fz-front-simulator/objects/simulation-runner';
import type IncomeTaxationParameters from 'fz-front-simulator/pods/taxation/income/parameters';
import type IncomeTaxationResult from 'fz-front-simulator/pods/taxation/income/result';
import type ApplicationService from 'fz-front-simulator/services/application';

interface TaxationIncomeKeyFiguresArgs {
  simulationRunner: SimulationRunner<IncomeTaxationParameters, IncomeTaxationResult>;
}

export default class TaxationIncomeKeyFigures extends Component<TaxationIncomeKeyFiguresArgs> {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;

  get result2022() {
    return this.args.simulationRunner.result?.events.find((e) => e.year === 2022)?.total;
  }
}
