import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import type IncomeTaxationParameters from 'fz-front-simulator/pods/taxation/income/parameters';
import type ApplicationService from 'fz-front-simulator/services/application';

interface TaxationIncomeParametersArgs {
  onChange: () => void;
  parameters: IncomeTaxationParameters;
}

export default class TaxationIncomeParametersComponent extends Component<TaxationIncomeParametersArgs> {
  @service declare application: ApplicationService;

  @action
  setIncome(value: number | string): void {
    this.args.parameters.income = Number(value);
    this.args.onChange();
  }

  @action
  handleIncomeRangeInputOnInput(inputEvent: Event): void {
    this.args.parameters.income = Number((inputEvent.target as HTMLInputElement).value);
  }

  @action
  handleIncomeRangeInputOnChange(changeEvent: Event): void {
    this.setIncome((changeEvent.target as HTMLInputElement).value);
  }
}
