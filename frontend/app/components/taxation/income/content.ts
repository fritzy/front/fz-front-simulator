import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import SimulationRunner from 'fz-front-simulator/objects/simulation-runner';
import IncomeTaxationSimulation from 'fz-front-simulator/pods/taxation/income/simulation';

import type IntlService from 'ember-intl/services/intl';
import type IncomeTaxationParameters from 'fz-front-simulator/pods/taxation/income/parameters';
import type ApplicationService from 'fz-front-simulator/services/application';

interface TaxationIncomeContentArgs {
  onLanguageChange: () => void;
  parameters: IncomeTaxationParameters;
  title: string;
}

export default class TaxationIncomeContent extends Component<TaxationIncomeContentArgs> {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;

  simulation = new IncomeTaxationSimulation();
  simulationRunner = new SimulationRunner(this.simulation, this.args.parameters);

  @action
  onParametersChange(): void {
    this.simulationRunner.simulation = this.simulation;
  }
}
