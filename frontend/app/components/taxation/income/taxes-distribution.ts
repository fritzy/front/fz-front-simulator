import Component from '@glimmer/component';
import { debug } from '@ember/debug';
import { inject as service } from '@ember/service';

import Ac4StackedColumn from 'fz-front-simulator/components/ac4/stacked-columns/stacked-colmun';
import Ac4StackedColumnElement from 'fz-front-simulator/components/ac4/stacked-columns/stacked-colmun-element';
import Ac4StackedColumnsData from 'fz-front-simulator/components/ac4/stacked-columns/stacked-columns-data';

import type IntlService from 'ember-intl/services/intl';
import type SimulationRunner from 'fz-front-simulator/objects/simulation-runner';
import type IncomeTaxationParameters from 'fz-front-simulator/pods/taxation/income/parameters';
import type IncomeTaxationResult from 'fz-front-simulator/pods/taxation/income/result';
import type ApplicationService from 'fz-front-simulator/services/application';

interface TaxationIncomeTaxesDistributionArgs {
  simulationRunner: SimulationRunner<IncomeTaxationParameters, IncomeTaxationResult>;
}

export default class TaxationIncomeTaxesDistribution extends Component<TaxationIncomeTaxesDistributionArgs> {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;

  get columnsData() {
    debug('stacked bars data computation');

    if (!this.args.simulationRunner.result) {
      return new Ac4StackedColumnsData([]);
    } else {
      const events = this.args.simulationRunner.result.events;

      return new Ac4StackedColumnsData(
        events.map(
          (e) =>
            new Ac4StackedColumn(
              e.year.toString(),
              Number(e.total.toFixed(2)),
              e.slices.map(
                (s) =>
                  new Ac4StackedColumnElement(
                    s.amount.toFixed(2),
                    this.intl.formatNumber(s.slice.taxYield / 100, {
                      style: 'percent',
                      minimumSignificantDigits: 1,
                    }),
                  ),
              ),
            ),
        ),
      );
    }
  }
}
