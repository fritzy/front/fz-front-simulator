import Component from '@glimmer/component';
import { action } from '@ember/object';

import { FamilyStatus } from 'fz-front-simulator/enums/family-status';

import type { TaxSharesSpecialStatus } from 'fz-front-simulator/enums/tax-shares-special-status';
import type TaxationTaxSharesParameters from 'fz-front-simulator/pods/-taxation/tax-shares/parameters';

interface TaxationTaxSharesParametersArgs {
  parameters: TaxationTaxSharesParameters;
  onChange: (parameters: TaxationTaxSharesParameters) => void;
}

export default class TaxationTaxSharesParametersComponent extends Component<TaxationTaxSharesParametersArgs> {
  get familyStatuses() {
    return Object.keys(FamilyStatus);
  }

  @action
  setAlternatingCustodyChildren(value: number): void {
    this.args.parameters.alternatingCustodyChildren = value;
    this.args.onChange(this.args.parameters);
  }

  @action
  setDependentChildren(value: number): void {
    this.args.parameters.dependentChildren = value;
    this.args.onChange(this.args.parameters);
  }

  @action
  setDisabledAlternatingCustodyChildren(value: number): void {
    this.args.parameters.disabledAlternatingCustodyChildren = value;
    this.args.onChange(this.args.parameters);
  }

  @action
  setDisabledDependentPeople(value: number): void {
    this.args.parameters.disabledDependentPeople = value;
    this.args.onChange(this.args.parameters);
  }

  @action
  setFamilyStatus(value: FamilyStatus): void {
    this.args.parameters.familyStatus = value;
    this.args.onChange(this.args.parameters);
  }

  @action
  setSpecialStatus(value: TaxSharesSpecialStatus): void {
    this.args.parameters.specialStatus = value;
    this.args.onChange(this.args.parameters);
  }
}
