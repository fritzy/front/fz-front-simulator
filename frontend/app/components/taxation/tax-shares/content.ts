import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import SimulationRunner from 'fz-front-simulator/objects/simulation-runner';
import TaxationTaxSharesSimulation from 'fz-front-simulator/pods/-taxation/tax-shares/simulation';
import Simulators from 'fz-front-simulator/utils/simulators';

import type IntlService from 'ember-intl/services/intl';
import type TaxationTaxSharesParameters from 'fz-front-simulator/pods/-taxation/tax-shares/parameters';
import type ApplicationService from 'fz-front-simulator/services/application';
import type RouterConfigService from 'fz-front-simulator/services/router-config';

interface TaxationTaxSharesContentArgs {
  parameters: TaxationTaxSharesParameters;
  title: string;
}

export default class TaxationTaxSharesContent extends Component<TaxationTaxSharesContentArgs> {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;
  @service declare routerConfig: RouterConfigService;

  simulation = new TaxationTaxSharesSimulation();
  simulationRunner = new SimulationRunner(this.simulation, this.args.parameters);

  get route() {
    return this.routerConfig.getSimulatorRoute(Simulators.TAXATION_TAX_SHARES);
  }

  @action
  onParametersChange(): void {
    this.simulationRunner.simulation = this.simulation;
  }
}
