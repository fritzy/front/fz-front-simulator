import { setOwner } from '@ember/application';
import { inject as service } from '@ember/service';

import type ApplicationService from 'fz-front-simulator/services/application';

export default class CurrencyParameters {
  @service declare application: ApplicationService;

  constructor(owner: unknown) {
    setOwner(this, owner);
  }

  get currency() {
    return this.application.currency;
  }
}
