import type Money from 'bigint-money';

export default class HistoricalPrice {
  constructor(readonly date: Date, readonly price: Money) {}
}
