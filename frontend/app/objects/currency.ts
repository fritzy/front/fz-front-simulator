export default class Currency {
  constructor(
    readonly id: string,
    readonly ticker: string,
    readonly symbol: string,
    readonly fractionDigits: number,
    readonly iso4217: boolean,
  ) {}

  toString() {
    return this.id;
  }
}
