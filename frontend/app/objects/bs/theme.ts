export default class BsTheme {
  public static readonly EMPTY_THEME = new BsTheme();

  #primary!: string;
  #secondary!: string;
  #success!: string;
  #info!: string;
  #warning!: string;
  #danger!: string;
  #light!: string;
  #dark!: string;

  private constructor() {
    // private constructor
  }

  get primary() {
    return this.#primary;
  }
  get secondary() {
    return this.#secondary;
  }
  get success() {
    return this.#success;
  }
  get info() {
    return this.#info;
  }
  get warning() {
    return this.#warning;
  }
  get danger() {
    return this.#danger;
  }
  get light() {
    return this.#light;
  }
  get dark() {
    return this.#dark;
  }

  static Builder = class {
    private _primary?: string;
    private _secondary?: string;
    private _success?: string;
    private _info?: string;
    private _warning?: string;
    private _danger?: string;
    private _light?: string;
    private _dark?: string;

    primary(value: string) {
      this._primary = value;

      return this;
    }
    secondary(value: string) {
      this._secondary = value;

      return this;
    }
    success(value: string) {
      this._success = value;

      return this;
    }
    info(value: string) {
      this._info = value;

      return this;
    }
    warning(value: string) {
      this._warning = value;

      return this;
    }
    danger(value: string) {
      this._danger = value;

      return this;
    }
    light(value: string) {
      this._light = value;

      return this;
    }
    dark(value: string) {
      this._dark = value;

      return this;
    }

    build() {
      const theme = new BsTheme();

      theme.#primary = this._primary ?? '';
      theme.#secondary = this._secondary ?? '';
      theme.#success = this._success ?? '';
      theme.#info = this._info ?? '';
      theme.#warning = this._warning ?? '';
      theme.#danger = this._danger ?? '';
      theme.#light = this._light ?? '';
      theme.#dark = this._dark ?? '';

      return theme;
    }
  };
}
