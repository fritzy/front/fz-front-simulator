export default class CoinGeckoCoin {
  constructor(readonly id: string, readonly symbol: string, readonly name: string) {}
}
