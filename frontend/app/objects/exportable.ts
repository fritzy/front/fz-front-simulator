export default interface Exportable<Q> {
  fromQueryParams(params: Q): void;
  toQueryParams(): Q;
}
