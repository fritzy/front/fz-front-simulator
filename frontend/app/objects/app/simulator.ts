import type { SimulatorCategory } from 'fz-front-simulator/enums/simulator-category';

export default class Simulator {
  constructor(
    readonly category: SimulatorCategory,
    readonly name: string,
    readonly icon: string,
    readonly embeddable: boolean,
  ) {}

  get iconPrefix() {
    if (this.icon === 'btc') {
      return 'fab';
    }

    return 'fal';
  }

  get id() {
    return `${this.category.toLowerCase()}-${this.name}`;
  }

  get labelKey() {
    return `objects.app.simulator.${this.category.toLowerCase()}.${this.name}`;
  }
}
