import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

import Money from 'bigint-money';

export default class CryptoCurrencyTicker {
  @tracked price: Money;

  constructor(readonly cryptoCurrency: string, price: Money) {
    this.price = price;
  }

  @action
  setPrice(value: number): void {
    this.price = new Money(value.toString(), this.price.currency);
  }
}
