import { tracked } from '@glimmer/tracking';
import { waitFor } from '@ember/test-waiters';

import { restartableTask, timeout } from 'ember-concurrency';
import { useTask } from 'ember-resources';

import type { TaskIsh } from 'ember-resources/-private/resources/ember-concurrency-task';
import type Simulation from 'fz-front-simulator/objects/simulation';

export default class SimulationRunner<P, R> {
  @tracked parameters?: P;
  @tracked result?: R;
  @tracked simulation?: Simulation<P, R>;

  task = useTask(this, this.run as unknown as TaskIsh, () => [this.simulation, this.parameters]);

  constructor(simulation: Simulation<P, R>, parameters?: P) {
    this.parameters = parameters;
    this.simulation = simulation;
  }

  get isRunning() {
    return this.task.isRunning;
  }

  @restartableTask
  @waitFor
  private async run(simulation: Simulation<P, R>, parameters: P): Promise<void> {
    await timeout(500);

    this.result = this.parameters ? await simulation.simulate(parameters) : undefined;
  }
}
