export default interface Simulation<T, R> {
  simulate(parameters: T): Promise<R>;
}
