import type Am4ChartsHistoryElementData from './history-element-data';

export default class Am4ChartsHistoryData {
  constructor(
    readonly currency: string,
    readonly currentBalance: number,
    readonly elements: Am4ChartsHistoryElementData[],
  ) {}
}
