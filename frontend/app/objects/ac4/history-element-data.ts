export default class Am4ChartsHistoryElementData {
  constructor(readonly date: Date, readonly value: number) {}
}
