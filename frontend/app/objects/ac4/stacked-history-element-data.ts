export default class Am4ChartsStackedHistoryElementData {
  [property: string]: number | Date;

  constructor(readonly date: Date) {}
}
