export default class Am4ChartsStackedHistorySerie {
  constructor(readonly label: string, readonly stacked: boolean, readonly striped?: boolean) {}
}
