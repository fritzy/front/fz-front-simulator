import type Am4ChartsStackedHistoryElementData from './stacked-history-element-data';
import type Am4ChartsStackedHistorySerie from './stacked-history-serie';

export default class Am4ChartsStackedHistoryData {
  constructor(
    readonly currency: string,
    readonly series: Am4ChartsStackedHistorySerie[],
    readonly elements: Am4ChartsStackedHistoryElementData[],
  ) {}
}
