import type { Money } from 'bigint-money';

export default class LoanHistory {
  constructor(
    readonly date: Date,
    readonly payment: Money,
    readonly capital: Money,
    readonly interests: Money,
    readonly balance: Money,
  ) {}
}
