import type Dexie from 'dexie';
import type { Task } from 'ember-concurrency';

export default class IndexedDbService {
  setup(): Promise<void>;
  setupTask: Task<Dexie, void[]>;
}
