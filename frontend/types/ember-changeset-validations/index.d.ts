import type { ValidatorAction } from 'ember-changeset/types';

export default function lookupValidator(validationMap: {
  [s: string]: ValidatorAction | ValidatorAction[];
}): ValidatorAction;
