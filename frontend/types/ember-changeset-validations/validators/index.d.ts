import type { ValidatorAction } from 'ember-changeset/types';

export function validateConfirmation(options: { allowBlank?: boolean; on: string }): ValidatorAction;

export function validateFormat(options: {
  allowBlank?: boolean;
  allowNonTld?: boolean;
  minTldLength?: number;
  on?: string;
  regex?: string;
  type?: 'email' | 'phone' | 'url';
}): ValidatorAction;

export function validateLength(options: {
  allowBlank?: boolean;
  allowNone?: boolean;
  is?: number;
  min?: number;
  max?: number;
  useBetweenMessage?: boolean;
}): ValidatorAction;

export function validatePresence(options: { ignoreBlank?: boolean; presence: boolean }): ValidatorAction;
