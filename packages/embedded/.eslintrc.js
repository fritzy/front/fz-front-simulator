'use strict';

const { configs } = require('@nullvoxpopuli/eslint-configs');
const { tsBase, baseRulesAppliedLast, moduleImports } = require('@nullvoxpopuli/eslint-configs/configs/base');

const config = configs.node();

module.exports = {
  ...config,
  ignorePatterns: 'dist',
  overrides: [
    ...config.overrides,
    {
      files: ['./**/*.ts'],
      ...tsBase,
      plugins: [tsBase.plugins, moduleImports.plugins].flat(),
      extends: [
        'eslint:recommended',
        'plugin:decorator-position/ember',
        'plugin:@typescript-eslint/recommended',
        'prettier',
      ],
      rules: {
        ...tsBase.rules,
        ...moduleImports.rules,
        ...baseRulesAppliedLast,
      },
    },
    {
      files: ['./**/*.js', './**/*.ts'],
      rules: {
        'prettier/prettier': ['error', { singleQuote: true, printWidth: 120, trailingComma: 'all' }],
      },
    },
  ],
};
