'use strict';

const path = require('path');
const webpack = require('webpack');
const Dotenv = require('dotenv-webpack');

module.exports = {
  devtool: process.env.PRODUCTION != null ? false : 'source-map',
  entry: './src/index.ts',
  mode: process.env.PRODUCTION != null ? 'production' : 'development',
  module: {
    rules: [
      {
        test: /\.ts$/i,
        loader: 'esbuild-loader',
        options: {
          loader: 'ts',
        },
      },
    ],
  },
  plugins: [new Dotenv()],
  output: {
    filename: 'iframe-integration.js',
  },
  target: 'web',
  resolve: {
    extensions: ['.js', '.ts'],
  },
};
