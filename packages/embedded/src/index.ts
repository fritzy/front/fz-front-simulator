import { iframeResizer } from 'iframe-resizer';

import { generateOptions } from './options';

const script = document.getElementById('fritzy-simulator');
const simulatorName = script?.dataset.simulator;

if (script && script.parentNode && simulatorName) {
  const options = generateOptions(
    simulatorName,
    script.dataset.simulatorBackgroundColor,
    script.dataset.simulatorTitle,
  );

  if (options.simulator.path) {
    let src = `${process.env.URL}${options.simulator.path}?${options.simulator.queryParams}`;

    const iframe = document.createElement('iframe');
    const iframeAttributes = options.iframeAttributes;

    iframeAttributes.src = src;

    Object.entries(iframeAttributes).forEach(([name, value]) => iframe.setAttribute(name, value));

    iframeResizer(options.iframeResizerOptions, iframe);
    script.parentNode.insertBefore(iframe, script);
  }

  script.remove();
}
