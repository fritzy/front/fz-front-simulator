import type { IFrameOptions } from 'iframe-resizer';

enum simulators {
  'loan-credit-score' = '/fr/credit/score-credit',
  'loan-loan' = '/loans/loan',
  'saving-digital-asset' = '/savings/crypto',
  'saving-financial-freedom' = '/fr/epargne/liberte-financiere',
  'saving-fixed-rate' = '/fr/epargne/taux-fixe',
  'saving-six-accounts-strategy' = '/fr/epargne/strategie-six-comptes',
  'taxation-income' = '/fr/fiscalite/revenu',
  'taxation-tax-shares' = '/fr/fiscalite/parts-fiscales',
}

interface Attributes {
  [qualifiedName: string]: string;
}

export interface Options {
  iframeAttributes: Attributes;
  iframeResizerOptions: IFrameOptions;
  simulator: Simulator;
}

export interface Simulator {
  path: string | undefined;
  queryParams: string;
}

export function generateOptions(name: string, backgroundColor: string | undefined, title: string | undefined): Options {
  let simulatorPath;

  if (Object.keys(simulators).includes(name)) {
    simulatorPath = simulators[name as keyof typeof simulators];
  }

  let simulatorQueryParams = 'embedded=true';

  if (backgroundColor !== undefined) {
    simulatorQueryParams = simulatorQueryParams + '&backgroundColor=' + encodeURIComponent(backgroundColor);
  }

  if (title !== undefined) {
    simulatorQueryParams = simulatorQueryParams + '&title=' + encodeURIComponent(title);
  }

  return {
    iframeAttributes: {
      style: 'border: none; width: 1px; min-width: 100%;',
    },
    iframeResizerOptions: {
      autoResize: true,
      heightCalculationMethod: 'taggedElement',
    },
    simulator: {
      path: simulatorPath,
      queryParams: simulatorQueryParams,
    },
  };
}
