# CHANGELOG

<!--- next entry here -->

## 0.49.0
2024-07-29

### Features

- fixed rate saving - linear interest (b1b6781bfebf988e71b572e00436e9e0aea62115)

## 0.48.0
2024-07-29

### Features

- add commercial model message (a732791939cd6ce4b074a2b0c32883a4fe6fb2e1)

## 0.47.0
2024-07-28

### Features

- add fees on fixed rate simulator (29cf2983880b8eea92046fdc6e7071af81cd0f21)

## 0.46.2
2024-07-20

### Fixes

- unit test saving simulator (5020e41d3a5196a1007dfcf2038b6db9fb341cbf)
- use fritzy back for digital assets price (93b279c5f5d96cd941a195d948dad59ff0c51210)

## 0.46.1
2023-08-26

### Fixes

- change private class based method to private method (c44c86b7e67053b4cb39dcac16f7c2b11c611137)

## 0.46.0
2023-08-26

### Features

- update publication responsible (9825e4c36713170e74b04a0bed23d2a8ce097ac5)
- update copyright year (d556ee7ac32205e4a6a7e9a39e0f2f74f8c3363e)

## 0.45.3
2022-06-10

### Fixes

- savings computation (af29b9d49ab551074707512a527a839983feffd8)

## 0.45.2
2022-01-08

### Fixes

- remove useless 404 file (77672e8cc0f30e7aa6e07196cccb3e1a948b693c)

## 0.45.1
2021-12-08

### Fixes

- remove _redirects file (80e53d45859af4d70de94f75b4fc060372059598)

## 0.45.0
2021-12-07

### Features

- **financial-freedom:** add inflation (7ebcd6310798fab1bab696a8dab7307b0e69e122)

### Fixes

- use bootstrap invisible css def (c642bd78ed6347d34e5d1bd13a5039ba123901e1)

## 0.44.0
2021-12-06

### Features

- **financial-freedom:** transform withdrawals evolution to cashflow evolution (9464c569f38e03c1de8d67090bb9ad478d664ad8)
- **financial-freedom:** add income growth (054480458146ec813324d6e77dd90dd2324f2698)

### Fixes

- **financial-freedom:** change fire threshold icon (b47288e21c2ef58c3bf0df001d4efa111ef35f8b)
- amcharts wrong positions (e065706edd2c1c8573fb3a07661b38d771b6c83f)
- **financial-freedom:** manual new parameter income growth (f48f29b3f912720ae9c5f2a63d0b9c54516e7720)

## 0.43.0
2021-12-05

### Features

- **financial-freedom:** add manual (df2f27ec86abc8af35031b6e144b0d13941b49f6)
- **financial-freedom:** add english (5e477a01cbbb50cbe5056aa2f1aa13e3e92cfb12)

### Fixes

- **financial-freedom:** remove fire yield field (f8098f11bf66cc7f8119c7bdc39b3e9cb5900dad)
- **financial-freedom:** translations (23856e29456bf12dfb615b0ec5b99be956137117)

## 0.42.0
2021-12-04

### Features

- **financial-freedom:** new simulator (5ab4c31287815f3841497ddaabcb2cc7dc18f9ee)

### Fixes

- **taxation-tax-shares:** remove useless code (91da87e0f72d42876688f243b8e79a92a85eb454)

## 0.41.1
2021-12-01

### Fixes

- fix new prettier rules (b121e04c81d2edbbb4647306fb78589183fe11af)

## 0.41.0
2021-11-30

### Features

- **tax-shares:** new simulator (3b49cc82f09dbe94462578ba8a44588cd0648dc4)

### Fixes

- disclaimer text (c0d787ca3b1854504e90239afa9e8398307d02e1)

## 0.40.1
2021-11-16

### Fixes

- cryptocurrency selector by market cap (d20fb1429d17289e0eb640f839dc6712553b2dc6)
- duplicates in options of crypto currencies search (1d95166a86e91c4ee10b7afd6120939f97ac138a)

## 0.40.0
2021-11-15

### Features

- **integration:** add links (46664210f074f4230e45ba5566f1d91564972a4d)

### Fixes

- saving icon in main sidebar (3bd68aa3fb181e5aab5417497b7ff7016ba140ed)
- change layout to layout-fixed (d107d15140fb4756eb197f9d7f1851f1a5d56af7)

## 0.39.0
2021-11-14

### Features

- **integration:** customizable background color (14ed32daa0d9b3b32b308c3097f538909405635f)

## 0.38.0
2021-11-14

### Features

- **integration:** add title parameter (5c8bfdbe01812204e11f20bb179e57964a379690)
- **integration:** add information notice (961a72f32ee428c678498b54e0943e093cf44193)

### Fixes

- **integration:** simulator list (c0ba9a16cfb9600860ff95822d6ce6f9af7d0640)

## 0.37.0
2021-11-14

### Features

- integration route (901d51d02f1655cc42552c11ad084e6285a96332)

## 0.36.0
2021-11-12

### Features

- add fritzy logo on embedded page (d33416e458d06d28966e379c172cfed5d6c3f8f5)

## 0.35.2
2021-11-12

### Fixes

- disable sitemap generation (c9b42be691d6413b4a139c193a45a16f91791252)

## 0.35.1
2021-11-11

### Fixes

- inject simulator url into iframe integration script (0c61ebd83a5e76a62709cfdeeecd5d92f8fe42f4)

## 0.35.0

2021-11-11

### Features

- iframe integration script (3966854d592d880afdd458a15e0d5494ca0cedfa)

## 0.34.1

2021-11-01

### Fixes

- linter errors (963627806f7e75d295970c00d78174fbcfb09325)

## 0.34.0

2021-10-28

### Features

- **credit-score:** public (49f4a288af132400b1fc847aabf866fd7134d0d5)

### Fixes

- **credit-score:** set contribution in euros (16c2acc595685eae3eaf900991ee61dda9336081)
- **credit-score:** add translations (98b6b2c87d2a58ef8e448460d538468453a837ee)

## 0.33.1

2021-10-27

### Fixes

- remove unused ember modifier types (e3b752d074a2ec6c9d1ea5d7cbc775bcd53b55a4)
- move volta documentation (a9f86cff6ddade3edae6b6298e53e27040522f90)
- **credit-score:** add loan amount (816d90af0de76610104ae38ec624b4cc7bc59f77)
- update on change on forms element (43b9af902c132a225294388de8a6f20b8495f3f8)
- **credit-score:** update on socio professional category change (50fd0ae52846e490673b1b2d21f68f732caa887f)

## 0.33.0

2021-10-27

### Features

- **credit-score:** new simulator (d34b6652b57515d02e01268282ebd2f6315e5e60)

### Fixes

- remove unused translations (a10d020651fc41e72d06be5800be9c33bf9c1e73)
- set user locale at initialization (ff3b7f746f856fb3182091d79ae994de4c04060a)

## 0.32.0

2021-10-22

### Features

- disptach user on index based on language (be8cdfb02517c1a99918feb1928235153c0042cb)
- read language from localstorage (c8e3e7361281d29ab60a7ddb8c59a8bfef9fdb72)
- index remake (647a2df91c7d62c6384e9251c81d1c04b6993e48)

### Fixes

- local storage error (aab04c984ab5e279226f1038f7fea0776e78a7fe)

## 0.31.0

2021-10-19

### Features

- not found route (e0ee8c188053941388933274af0827b299c1bf35)

### Fixes

- remove useless session redirect (5e0b8c403446cc44971e004459cfa064342d7f1c)

## 0.30.0

2021-10-18

### Features

- **income-taxation:** add income chart taxation (84d8e7e9c674ef281f7e5842c9cc9f3a6cebfd2a)

## 0.29.3

2021-10-16

### Fixes

- **digital-currency-saving:** move elements to components (95420ab6acfd93511f85df1d4d610d4d1c912688)
- print no data on charts (fd8349d616c30da24284f8916d3a3873b2a1215f)
- **digital-currency-saving:** add content loader on graph (25bf6e74a845206ba835405dbcf1347dc27b5029)

## 0.29.2

2021-10-14

### Fixes

- **six-accounts-strategy:** typos (be00efd4a4a6978bdda073aac7b2252680d63ea8)

## 0.29.1

2021-10-12

### Fixes

- **crypto-saving:** fetching prices on very small amount (4358ec9058fe3317837aaee76c69ff3017345c24)

## 0.29.0

2021-10-10

### Features

- **tax-income:** 10 years evolution (37dcf93d805cbf9d281c72486194e5e2fc2e9b9b)

### Fixes

- use a result container for a simulation (b7f88ccf468a609b0266d7d6af2ff37e07bd23ea)
- **income-taxation:** move pods to content component (fb713937714e8e71df6aee8b1118ec588f7cb99f)

## 0.28.2

2021-10-05

### Fixes

- embedded query param ignored (c3dab5a7d6a5c2ff15164cedee08777f77d879f7)

## 0.28.1

2021-10-04

### Fixes

- **legal-notices:** small fixes (8b6a5eec9247ad3912f8d66a13a45a936446c9bc)

## 0.28.0

2021-10-04

### Features

- legal notices route (723d9c5eae35b61f0ac36748ce1bffe4eeb97134)

## 0.27.0

2021-10-03

### Features

- add disclaimer footer (468a7aecb705008a83269059cc8c9b86b4415f25)

### Fixes

- move sentry config to addons config (e835fd5a5d82d2cdad5bdb0686c25f664362bcc0)

## 0.26.3

2021-10-02

### Fixes

- sentry error in production (82fbd61f3e07baaf51af9766199a96d9af94bc85)

## 0.26.2

2021-10-01

### Fixes

- **six-accounts-strategy:** remove external link (7c907b344f8bda72baded9869055ee5f3e811caa)
- alphabetize translations (3dd2e836d755a2e15419f7f4ec12a5b1eee44f8c)

## 0.26.1

2021-09-27

### Fixes

- replace check browser by window.onerror (a2fdec6aec61c21bad37596a8b9d93a473b905f9)

## 0.26.0

2021-09-26

### Features

- **six-accounts-strategy:** add sankey diagram (6f9817674034317ce18d59859abe8bbe9475302b)

### Fixes

- **six-accounts-strategy:** description (9cacd647431289c2e660a36970f4d72b8c6032d8)

## 0.25.1

2021-09-25

### Fixes

- **six-accounts-strategy:** types (124e506c17ebc3141a4699680a61a62bd49aa5b6)

## 0.25.0

2021-09-25

### Features

- **six-accounts-strategy:** new simulator (f85517dab8149af5be1d37da8ba1a3026823024b)

### Fixes

- router config (c1c1be70b414f0ab79af27e08992383d0bd05ca9)
- footer link to fritzy (6319c1ad4d6cc2a613f95595d6ea4bcbe5cdfcde)

## 0.24.0

2021-09-23

### Features

- add language selector (d9de88139eef06e2a32c77394f19b7e7198e5b7e)

### Fixes

- remove --random for test args concatenation (b9ad29d8946e42d605b4d6adbf77bee94c8aa2a3)
- rename to setLocale (e028a82e5990974648138eddc8de9af3da584e66)

## 0.23.0

2021-09-23

### Features

- **crypto-saving:** add currency update (42fdb78a490b935ca8d922c56a10061e443b20f4)

### Fixes

- **crypto-saving:** recomputet on currency update (1a048e2ab0e629dd669bf60ea5953566b0c866a0)

## 0.22.1

2021-09-21

### Fixes

- use task in crypto form currency selector (9d35bed7edab21484cee10c0248bbf4d80d0e825)

## 0.22.0

2021-09-21

### Features

- **crypto-saving:** reactive route (f5eaeafcba6d758ba5528f690a05aecb65c288ee)

## 0.21.1

2021-09-18

### Fixes

- simpler loader (212b55267adc430acd4849fbcdbbde0647d65ea3)
- import admin-lte from sass (623aa447e15236126ff86a4e20212ee4a57e72c7)

## 0.21.0

2021-09-18

### Features

- add currency tab (65386a9a659148bdcd38e8a3f06b37d2334b6a3e)

### Fixes

- content wrapper component (b84abbffa18233e1881cb500d6a528482f529a64)

## 0.20.18

2021-09-15

### Fixes

- **fixed-rate-savings:** skeleton chart pie on firefox (8885e8347b339a9718e782422c7c502648f7a735)

## 0.20.17

2021-09-13

### Fixes

- select trigger hidden on firefox (233d5fa65d914a7e038e1ef68ec4f119d891a716)

## 0.20.16

2021-09-09

### Fixes

- add cloudflare web analytics (ab104ad9a9f9b7e665823fb9daec4dd80782c783)

## 0.20.15

2021-09-06

### Fixes

- no fontawesome pro license (a7dc482bc5906e3504a9aa147fbeaff9afa41350)

## 0.20.14

2021-09-05

### Fixes

- **loan:** allow zero rate (1ce1f8dac79997f25f911cc9ae7693f6142cf076)

## 0.20.13

2021-09-04

### Fixes

- **fixed-rate-savings:** show withdrawals on key figures (1d98956e3b8efd3ca5c7b82b57a66573f6400672)

## 0.20.12

2021-09-04

### Fixes

- **fixed-rate-saving:** render only visible table row (41d9cbcf7058868027490bbe20d10d0633fa84b8)
- **fixed-rate-saving:** align amount to right (5ac4169bbdbbd2b9b25b773812484841f74964de)
- format money helper does not remove fraction digits (259dfb1934177802664f9ff4c818a3d36274fe43)

## 0.20.11

2021-09-01

### Fixes

- **fixed-rate-saving:** unit test computation (d572543090136be6794f905622b28dd056b80b8b)

## 0.20.10

2021-08-31

### Fixes

- move to ember exam (861e31b4488f37bc26f1c81ccadd5854c840a292)

## 0.20.9

2021-08-30

### Fixes

- **fixed-rate-saving:** results update on slider change (bc5dcaf1e35d18f3cff6dcd1e0962460e1869395)

## 0.20.8

2021-08-30

### Fixes

- **fixed-rated-saving:** use resources for reactive components (dc47631477b83db211c13a0e0c8caf3d329dd014)

## 0.20.7

2021-08-28

### Fixes

- defer charts generation (2c59a7604920d806519f26ff7aaf3dd88c42c99c)
- daisy-chaining multiple charts (66d185f7c9c3c1c4f225988ce5d6f0b4e465149e)
- skeleton for pie chart (ccf5dc818df5ff3c323596cdabeb955483f66dcf)

## 0.20.6

2021-08-27

### Fixes

- wrong behaviour of imask after update (cd53d8a8a68f5e47580f6648c8c070bc7e1eeb39)

## 0.20.5

2021-08-26

### Fixes

- add test on imask currency modifier (091b8e42c9343abb55f20b1af695524480674ec2)

## 0.20.4

2021-08-26

### Fixes

- add test on imask currency modifier (d99ea6cfa22fc6f9eb54a10f407bc95439f87376)

## 0.20.3

2021-08-25

### Fixes

- remove assetMap generation because not cached by service worker (0e305f88311eea3bdc4d58ac9c3df9f41354db19)

## 0.20.2

2021-08-24

### Fixes

- log no indexeddb available (208e81a41a650f0265aedb412784348b7613ebcb)
- put application parameters to a service (d90d3186c930193f3b980a0950c0363cf0abec5c)

## 0.20.1

2021-08-23

### Fixes

- **fixed-rate-savings:** skeleton on results cards (86c9fd4af60b286cd7cabf5b6f29ef4725683cbb)

## 0.20.0

2021-08-21

### Features

- add sitemap (bfdce2cbc53cdf65ec6d24aeb88238fa746075bb)

## 0.19.0

2021-08-19

### Features

- title on each route (2d42424ef2babd1f2ea547ca3ca0bc7034a219f2)
- add description on routes (0d86b23f3c3789a159e8dae128cda89af663df4e)

## 0.18.2

2021-08-16

### Fixes

- **fixed-rate-saving:** en translation (1384b7dc16aac847ce6d2a5aa4fecaa57a8620ce)

## 0.18.1

2021-08-16

### Fixes

- inject intl into intlConfig service (159a614d914e0e1bc4f5c10df1f0d496d8f59cc3)
- some files to typescript (168292afc3d4bfcd07915767fe9bc697320e4c5e)

## 0.18.0

2021-08-15

### Features

- add en langage (8f7dcb564ffb9938c351a2c872c5bc7513b8521d)

## 0.17.0

2021-08-14

### Features

- dark mode (00db8df2c4603876c3ca1f38b3e7668f73083904)

## 0.16.0

2021-08-14

### Features

- **fixed-rate-saving:** add fr route (4b9dfb43a2f95a9aedb41c25e21801b0555772c8)

### Fixes

- **fixed-rate-saving:** extract parameters to a component (4b006772d14aaf7b91f7fdd877b2bc205868f1c3)
- configure prettier for hbs template (69e12e4f616a0c34f42271e628d60550329eb2fa)
- **fixed-rate-saving:** factorize key figures component (ebccb5638f1d8d2dd8e09056de0da5610968ae06)
- **fixed-rate-saving:** extract manual to a component (48e9b999f103c2e9cd76f925a05dc8ae49cc661f)
- **fixed-rate-saving:** i18n manual component (2e54378583367d421d9ea79527bf0b2fae909065)

## 0.15.0

2021-08-10

### Features

- **constant-yield-saving:** add withdrawal (cb0d540a64359056b12b69d7dc490df53d1fa47c)

## 0.14.22

2021-08-03

### Fixes

- move assetlinks.json to .well-known folder (598a13773f47a1f7c33c4b845ede29cb4a3834b7)
- add simulation service (f2f69a9ad4d7b7fe8ff1cdf68ba839db7a79e12b)

## 0.14.21

2021-07-26

### Fixes

- **contant-yield-saving:** add query parameters (4e7dae7fb8a7c062852f940672c57e9e2f76c1ec)

## 0.14.20

2021-07-12

### Fixes

- **constant-yield-saving:** add helper card (2456aff167a03fcc44cfa0b79d5432411fa94f96)
- **constant-yield-saving:** tooltip on label (c3e556909e7158123b12e3467577ef9245ff9704)

## 0.14.19

2021-07-11

### Fixes

- **constant-yield-saving:** update contribution to 1 000 000 (be425630a65bc5a48facd05af07b0461742a3c43)
- typescript error (0e188788c8283cbc2f938625c40d9e04cb39f633)

## 0.14.18

2021-07-09

### Fixes

- **crypto-saving:** all in (9014d07a3f27d87f439ee3b485d71ce501007771)
- **saving-crypto:** add coin query param (4b7504fc1dcaaacb4d7b74b19978432462c1722e)

## 0.14.17

2021-07-08

### Fixes

- import nullvoxpopuli/eslint-configs (eaa0e8aedd907c26b3b28ecee24744b5f5c4c193)

## 0.14.16

2021-07-06

### Fixes

- stacked history improvement (f304965ad6fa91075b91a92a183af4e3d56dddf2)

## 0.14.15

2021-07-05

### Fixes

- allowed to work without indexed db (435667599125a96c8c006d5d63447a4790f2b64a)

## 0.14.14

2021-07-05

### Fixes

- embedded on savings (29571d1c8e782968d29e819d21861f1725e4c89a)

## 0.14.13

2021-07-04

### Fixes

- update uglify with terser (a772ff31f279019c4253811420b5cb459d3b1701)

## 0.14.12

2021-07-04

### Fixes

- savings computation (e5ed0a363c248c98144490d6327c1deff93d6705)
- unresponsive simulator on 0 amount inputs (afc1ace920c0d39840dc15c911314737185bdf57)

## 0.14.11

2021-07-04

### Fixes

- update footer date to 2012 (9f8d7ec6b587c2c650a610f968d2aa6a9902512f)

## 0.14.10

2021-02-27

### Fixes

- **savings/crypto:** add upto field (be83ae4a240f761dd38133ddb084e19e56e26e7a)

## 0.14.9

2021-02-26

### Fixes

- **crypto-currencies:** crypto currencies with exact code in first (ef49bdfaf5cc484b7d8499dab4b29566240d3eb0)

## 0.14.8

2020-12-26

### Fixes

- cp public/index.html to all routes (e75676082be2c35bf1d1e5eb6e7642a1921f1f22)

## 0.14.7

2020-12-26

### Fixes

- cp index.html to all routes (a70134bac9d1b9e4198d599ccbd68cd89ae1b627)

## 0.14.6

2020-12-26

### Fixes

- copy index.html to all routes (1699129c0d821151ed2ae8c8144f34b95c744434)

## 0.14.5

2020-12-22

### Fixes

- manage 404 errors (171d8aa51442baadc4515cb0670da78bb796c5c3)

## 0.14.4

2020-12-20

### Fixes

- redirect 404 to 404.html file (b626a47cbf9c303d8e982fc15724d5adbb51846d)

## 0.14.3

2020-12-20

### Fixes

- rename stage to pages (69139c883f70327cc056eaf813b4f3877b5bbf2a)

## 0.14.2

2020-12-20

### Fixes

- deploy pages image (b98eb99235b70bf313a55c5d4e084579cffe1342)

## 0.14.1

2020-12-20

### Fixes

- build artifacts dependencies (c15942375b70bb71b650bd038ccb8ede353f18f1)

## 0.14.0

2020-12-20

### Features

- deploy to gitlab pages (cf54572b497c925b5cbc52236526c65a369b76a4)

## 0.13.0

2020-10-11

### Features

- add savings crypto simulator (659c5135b9f229a7980e36177d046a8a30260b62)

## 0.12.2

2020-08-15

### Fixes

- add unsupported browser page (28e1b793619947632f8a63da04e6c40b054a8e04)

## 0.12.1

2020-08-15

### Fixes

- add loader (5cb7db7c67a32e92b065b770e567f88e5082f5b9)

## 0.12.0

2020-08-10

### Features

- crypto portfolio (e6687ab1c3a43e91a3b7ce475a87b8f6cc373537)

## 0.11.0

2020-08-09

### Features

- income taxes (e75b026d69d2a1c88d775b48567861a1225cb001)

### Fixes

- content-wrapper (6c0c524e1a02b95cca94d63aba6191cc0958af24)
- typescript warnings (c860cd81dda99f71e7acdcbd05920fbfa449c637)

## 0.10.0

2020-08-07

### Features

- fetch coin gecko prices (8c0582bb3a548b2a0f16031748ddb863d26fbc89)

## 0.9.2

2020-08-07

### Fixes

- format unknow money currency (b4b9a10949017aa8268af3479a8399e5a509005a)
- **crypto-taxes:** division by 0 (078c79c014010d640c1d3c9aac20dcbd84cb9108)
- multi currency support (b7dbf3e806ffe2839ac0ab661c36f58ed12edf97)

## 0.9.1

2020-08-06

### Fixes

- **crypto-taxe:** error when empty (fe377c2120f9a253d4a7fcaef5aac5a1ab02c77d)

## 0.9.0

2020-08-06

### Features

- crypto taxes (c0ddad37372dd07e99636ea45b489314e57afcc1)

## 0.8.1

2020-08-03

### Fixes

- use bigint-money for pow (2f47dcad31188b625a45db358192510fdccac332)
- **loan:** set amount (9bc7496ed3a91b8e9f50474001cf7374b1bd1641)

## 0.8.0

2020-08-01

### Features

- loan (64545fb4682146dbda1f640ab1c936bf96e776d9)

### Fixes

- remove some alte components (30b71fbae0ce64d543e4a2769f102ba2e06e6138)

## 0.7.1

2020-08-01

### Fixes

- removes types provided by ember-modifier (e07b112d0e06959b1596b256f04b87e2934ba56c)

## 0.7.0

2020-07-31

### Features

- add landing choice (05fcf70c3e3cc1902cf119b5269667680a2bb307)

### Fixes

- typescript error (44b12b2609002c8e3c2531b468ce341fef19d6d2)

## 0.6.1

2020-07-31

### Fixes

- translations in update checker (64f290d2edacee43f6cb40def0ae8de7d6da5c17)

## 0.6.0

2020-07-31

### Features

- add navbar / footer / sidebar (635dd09e354fe246eee7f8f044c2633c2013c63b)

## 0.5.6

2020-07-27

### Fixes

- editable amount (88b79056e2f4549a0e2f6b21e439274935f555de)
- **saving:** card position (30e9e900713c9e6444c196a0ac7a5fb683522e3d)

## 0.5.5

2020-07-25

### Fixes

- kaniko context dir (9bf626229a94741b5a10cbac053983fc7a4392b3)

## 0.5.4

2020-07-25

### Fixes

- build image jobs dependencies (2f65b4122a254d2563487f3284e0374bd82357c0)

## 0.5.3

2020-07-25

### Fixes

- split docker image build into configure > build (7762f0bef5c23ef4efd8b7b57ed5c80af047cad4)

## 0.5.2

2020-07-25

### Fixes

- add dind for build image step (6d9f47cdc0849a033bb7666c93c4f227ec401f56)

## 0.5.1

2020-07-25

### Fixes

- font-awesome package (a595351f336ca3b8fb667514ba0187dc96f56f7a)

## 0.5.0

2020-07-25

### Features

- **saving:** add overview card (c5c76d75a55d7c16436bc3fb52536f55459b09c7)
- **saving:** added pie chart (d39f9c24ccac12130da17f75b27dd019ff7ee347)

### Fixes

- rename am4charts to ac4 (18f56d287cb82e44115634ade699ca7b7da7d8f3)
- icons (49c30b9ca52d7a921ea826de9896b3677a9b7a33)

## 0.4.2

2020-07-24

### Fixes

- implemented a debounce on histories computation (c00c889661403e9800513d1dead954028b93b9f3)

## 0.4.1

2020-07-23

### Fixes

- variabalize url (58a2a5fd63a695d33548f9fbd82e991630a134c5)
- move saving computation to @computed (86478b4ccdce3032ee6039d09ec0223724fb3dee)

## 0.4.0

2020-07-22

### Features

- add a privacy route (ad8932f4f932a04089edddb909fa91104009393f)

## 0.3.0

2020-07-20

### Features

- redirect index to savings (783f9282c3af441d5b4f089909952edcfb8e3235)
- pwa (ba5b7c7f21e70a12174374c61db04278c1279f1f)

## 0.2.0

2020-07-20

### Features

- build static compressed files (394bdde22884035fe2af944e592eae9a245601d5)

## 0.1.3

2020-07-19

### Fixes

- gitlab-ci lint (98b2f7b94d0d1af8d6905e746609752dcc433b46)

## 0.1.2

2020-07-19

### Fixes

- push docker image (6d86dd221dd180bfa4c62a0835e410a0ff09363d)

## 0.1.1

2020-07-19

### Fixes

- build artifacts (0cb9bba8fc63ce677ce6100642d7c071109f6bf3)

## 0.1.0

2020-07-19

### Features

- add savings card (38ff6e145a72eaca06b7f7eb5a98542020b0824f)
- add saving page (2917696d0ddce7e7551f787fa1a45a3991457907)
- compute savings (58f3775b9d28aa4d8baaf3266b56b7ad0eb0cf3d)
- add charts (9db2ead928551a12f71fdee5408d12d895b30fee)

### Fixes

- interets computation (561fa1ffad2e5722732bc7a5f622643805eb74d1)
